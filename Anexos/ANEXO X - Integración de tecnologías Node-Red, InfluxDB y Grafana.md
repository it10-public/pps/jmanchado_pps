# Anexo X

## Integración Node-RED/ InfluxDB/ Grafana

Se procede a hacer una prueba integrando las tres tecnologías para luego poder construir un escenario real. Por el momento los datos que se trabajaran son aleatorios para tener una carga simulada al guardar y mostrar a través de gráficos.

### Fase 1: Node-RED

El flujo completo es el siguiente:

![prueba mqtt_influxDB_grafana.png](Imagenes_anexos/Node-RED/prueba_mqtt_influxDB_grafana.png)

Describiremos nodo por nodo:

- **Trigger:** Envía un "1" cada cierto período de tiempo, que es configurable (en este caso cada 10 segundos), para que dispare la acción del bloque siguiente (**Numero aleatorio**).![trigger.png](Imagenes_anexos/Node-RED/trigger.png)
  Podría enviarse cualquier otro dato, no es de importancia, ya que cuando la función recibe una entrada, se ejecuta independientemente de lo que tenga la entrada, o si se usara como argumento propio de la función.

- **Numero aleatorio:** En este bloque se genera el flujo de datos que luego se parsea para colocarlo como corresponde en la base de datos y posteriormente en Grafana. El código es el siguiente:

  ```Javascript
  //############################ FUNCION ########################################
  // Retorna un número aleatorio entre min (incluido) y max (excluido)
  function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
  }
  //############################################################################
  var min = 0;
  var max = 40;
  var str1= '20';
  var str2= '02';
  var name= 'sensor';
  var result1 = getRandomArbitrary(min, max);
  var result2 = getRandomArbitrary(min, max);
  var str_datos = str1 + ';' + str2 + ';'+ name + ';'+ 'valor1=' + result1.toString() + ';'+ 'valor2=' +result2.toString()+ ';';
  msg.payload = str_datos;
  return msg;
  ```

  ![numero aleatorio.png](Imagenes_anexos/Node-RED/numero_aleatorio.png)
  Se crea una función para que genere números aleatorios entre limites inferiores y superiores. Luego se llama a esa función poniéndole como limites las variables `min` y `max`, respectivamente.
  Luego se crean variables adicionales para agregar mas carga al mensaje que se enviará al broker. Todas las variables y resultados se concatenan en forma de string en `str_datos`, separados cada uno por el caracter `';'` (Esto posteriormente servirá para poder parsear el mensaje).
  Por ultimo se pone la variable string como carga de `msg` y se devuelve el mismo como salida.

- **Mosca MQTT Broker:** Es un broker MQTT que se crea localmente. Muy útil para automatización de hogares o flujo de datos pequeño. Por defecto el broker escucha en el puerto 1883 y el puerto 8080 para el Websocket. Por el momento no le asignamos ningun usuario y contraseña ya que es una prueba. ![broker local.png](Imagenes_anexos/Node-RED/broker_local.png)

- **test:** Son dos bloques similares que tienen que ver con la publicación/suscripción respecto del broker local. Los datos aleatorios generados se publican al topic `test`.![topic a donde publica.png](Imagenes_anexos/Node-RED/topic_a_donde_publica.png)

  Luego el bloque suscriptor toma los datos que se enviaron para procesarlos.
  ![topic a donde publica_broker.png](Imagenes_anexos/Node-RED/topic_a_donde_publica_broker.png)

- **Parse message:** Es un bloque tipo funcion en donde se parsea el mensaje tomado del broker en forma de suscriptor al topic `test`. ![parse messsage.png](Imagenes_anexos/Node-RED/parse_messsage.png)
  El código es el siguiente:

  ```Javascript
  var msg433 = {};
  msg.payload = msg.payload.replace(/(\r\n|\n|\r)/gm,"");
  
  var parts433 = msg.payload.split(";");
  
  msg433.p1 = parts433[0];
  msg433.p2 = parts433[1];
  msg433.name = parts433[2];
  for (var i=3; i < parts433.length; i++) {
  	var keyvalue = parts433[i].split("=");
  	if (keyvalue.length===2) {
  		msg433[keyvalue[0]] = keyvalue[1];
  	}
  }
  
  msg.msg433 = msg433;
  
  msg.topic="test";
  return msg;
  ```

  Primero se crea la variable `msg433` donde se ira parseando el mensaje que se recibe. Luego eliminamos todos los saltos de línea posibles con la instrucción `msg.payload.replace(/(\r\n|\n|\r)/gm,"")`. Entendiendo esto:

  - **\n**: Nueva línea
  - **\r**: Retorno de carro
  - **gm**: Flags de patrones globales. 
    - **g**: Modificador global.  Todos los matcheos (No regresa después del primer encuentro)
    - **m**: Multilínea. Hace que ^ y $ coincidan con el inicio / final de cada línea (no solo el inicio / final de la cadena)
      Después se separan los datos teniendo en cuenta el caracter `';'`. De las primeras tres partes, la tercera es el nombre del sensor, por una cuestión de identificar el flujo de datos. A partir de ahí, con un bucle se recorre todas las partes separadas y se queda con el valor que fue enviado, propiamente dicho.
      Se agrega una propiedad nueva al mensaje que sigue en flujo, que es el topic. Servirá para setearlo mas adelante.
      Finalmente se devuelve el mensaje formateado y parseado.

- **valor1 conversion:** Bloque de función. ![valor1.png](Imagenes_anexos/Node-RED/valor1.png)
  El código es el siguiente:

  ```Javascript
  if (msg.msg433.valor1 !== undefined) {
  	msg.msg433.valor1  = parseInt(msg.msg433.valor1 , 10);
  }
  else msg.msg433.valor1 =-999.0;
  node.status({fill:"blue",shape:"ring",text: msg.msg433.valor1  });
  return msg;
  ```

  Se indica que si `valor1` no esta definido como parte del objeto `msg`, lo toma y lo incluye en el mismo, como un tipo de dato entero, y decimal. Esto viene de la instrucción `msg.msg433.valor1  = parseInt(msg.msg433.valor1 , 10)`. El "10" indica base decimal. Si estuviera definido, se setea con un **-999.0**. Además para poder observar como esta funcionando el nodo y ver que valor esta pasando por ahí se incluye la línea `	node.status({fill:"blue",shape:"ring",text: msg.msg433.valor1  })`. Sigue el mensaje con las respectivas partes siguientes.

- **valor2 conversion:** Bloque de función. Funciona exactamente igual que el anterior, nada más que trabaja con la variable `valor2`.

![valor2.png](Imagenes_anexos/Node-RED/valor2.png)

- **Prueba formato mensaje:**  Nodo de función. Asigna como propiedad los valores previos del mensaje para formatearlos de la manera que lo necesita la base de datos InfluxDB. ![prueba formato payload.png](Imagenes_anexos/Node-RED/prueba_formato_payload.png)

  El código es el siguiente:

  ```Javascript
  msg.payload = {
    name: msg.msg433.name,
    valor1: msg.msg433.valor1,
    valor2: msg.msg433.valor2,
  }
  return msg;
  ```

- **prueba influxdb:** Nodo de influxDB. ![influxdb.png](Imagenes_anexos/Node-RED/influxdb.png)
  Se debe indicar la dirección del servidor (Dirección IP local) y el nombre de la base de datos (*test*). Además el *Measurement* donde se insertarán los datos. En nuestro caso se llama **prueba**. Además se le puede indicar la precisión del tiempo y alguna política de retención. No será necesario modificar nada en ese sentido para este ejemplo.
  En cuanto a la configuración del nodo, se indica el nombre de la base de datos, el puerto por el que esta escuchando, usuario y contraseña con la que debe acceder e insertar los datos. Para el caso se usa `pi:it10`

![influxdb_2.png](Imagenes_anexos/Node-RED/influxdb_2.png)

### Fase 2: InfluxDB

Con InfluxDB ya instalado solo hace falta crear la base de datos de prueba, ya que Node-RED envía los datos, pero si no hay una base de datos donde insertarse, estos nunca se envían. Se procede de la siguiente manera:

1. Abrir el cliente de influxDB en la terminal de Raspbian:

   ```Bash
   influx
   ```

   ![cliente-terminal influx.png](Imagenes_anexos/influxdb/cliente-terminal_influx.png)

2. Creamos la base de datos `test`:

   ```Bash
   CREATE DATABASE test
   ```

3. Mostramos para ver que se creo correctamente:

   ```Bash
   show databases
   ```

   ![show databases.png](Imagenes_anexos/influxdb/show_databases.png)

4. Una vez hecho lo anterior, Hacemos **Deploy** nuevamente en Node-RED, para que cree el *Measurement* **prueba**, con los respectivos *Field Keys* comenzar a recibir los primeros valores. Luego lo observarnos con el cliente influx.

   ```Bash
   use test
   show measurements
   ```

   ![show databases.png](Imagenes_anexos/influxdb/show_measurements.png)

   ```Bash
   show FIELD KEYS
   ```

   ![show field keys.png](Imagenes_anexos/influxdb/show_field_keys.png)

   ```Bash
   SELECT * FROM prueba LIMIT 10
   ```

   ![primeros datos guardados.png](Imagenes_anexos/influxdb/primeros_datos_guardados.png)

### Fase 3 - Grafana

Grafana toma los datos insertados en la base de datos temporal y los pone a disposición en una grafica de nuestro gusto.

1. Ingresar a la aplicación web con nuestra IP local o a la que estamos accediendo en la misma red, en este caso `http://192.168.20.129:3000` e ingresar con el *usuario:contraseña* correspondiente.

2. Asignar una fuente de datos. Ir a configuración, sobre el panel izquierdo y seleccionar `Data sources`. Luego presionar sobre `Add data source`. ![agregar nueva fuente de datos.png](Imagenes_anexos/grafana/agregar_nueva_fuente_de_datos.png) En la imagen aparece ya agregada, pero el procedimiento es el mismo para agregar cualquier *data source*.

3. Configurar la fuente de datos. Aquí se pone la información necesaria para acceder a la base de datos Influx. ![config grafana influx.png](Imagenes_anexos/grafana/config_grafana_influx.png)

   ![config grafana influx_2.png](Imagenes_anexos/grafana/config_grafana_influx_2.png)Se le pone un nombre (en este caso *prueba_grafana*). En las opciones HTTP se pone la dirección IP local o de la maquina a la que se accede en la red, con el puerto 8086, que es el puerto por defecto en el que la base de datos escucha las peticiones HTTP. Dejamos la opción `Access: Server(Default)`. En cuanto a la autenticación, solo ponemos la básica, que son nuestro *usuario:contraseña* de Raspbian (**pi:it10**). Por ultimo en los detalles de InfluxDB, se indica la base de datos a la que se quiere acceder. En este caso a `test`, con el *usuario:contraseña* **pi:it10**. Esta ultima parte de autenticación no es necesaria ya que se accede de manera local con el mismo usuario pi. Se guarda y se procede a crear un dashboard.

4. Crear un dashboard. Aquí se decide que tipo de panel se va a utilizar. Por el momento solo usaremos gráficos.![nuevo dashboard.png](Imagenes_anexos/grafana/nuevo_dashboard.png) ![panel nuevo.png](Imagenes_anexos/grafana/panel_nuevo.png) Sobre el `Panel Title` se hace click, luego `Edit`. ![metrics.png](Imagenes_anexos/grafana/metrics.png) En la pestaña metrics esta la parte esencial para mostrar los datos. Es donde se encuentra la peticion a la base de datos. Por el momento solo realizaremos una sola para este panel.
   - En la línea `FROM`: *Data source* *Measurement* `WHERE` *Field* `=` *valor*. Esta línea debemos indicar la fuente como **default** o **prueba_grafana**, **prueba** como *Measurement*. `WHERE` nos sirve para identificar datos particulares dentro de la misma base de datos, como por ejemplo todo lo referido a un sensor, entonces se indica **name** como *Field* y **sensor** valor de ese field. En el caso que tuviéramos mas sensores de esta forma se puede discriminar uno de otro.
   - En la línea `SELECT`: *field()* *mean()*. En field, indicamos que para **valor1** realizaremos la grafica con sus datos, y realizando la media de los datos aportados en el intervalo de tiempo que se ha consultado (había quedado por defecto 10 segundos).![metrics_select.png](Imagenes_anexos/grafana/metrics_select.png) Se puede agregar o modificar la caracteristica de como se procesan los datos, no es solamente **mean()**, Para el caso es el mas conveniente para visualizar.
   - En la línea `GROUP BY`: `time($_interval)` `fill(null)`. Aquí se indica el tiempo de acceso a los datos y como mostrarlos. Dejamos por defecto `time` y en fill podemos dejarlo como **null**, que se visualiza en forma de puntos, o con **none**, en forma de linea continua.
   - En la línea `FORMAT AS`: `Time Series` lo dejamos por defecto.

5. Para ver mas cuestiones títulos, leyendas, formas de grafico, etc, están las demás pestañas. No lo analizaremos por el momento.

El resultado de toda esta configuración es la siguiente:

![prueba dashboard.png](Imagenes_anexos/grafana/prueba_dashboard.png)

