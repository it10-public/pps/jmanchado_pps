

# ANEXO V 

## Raspbian 

## Formateo de SD
Primero hay que formatear la SD que se va a usar para instalar el sistema operativo. Para hacer esto vamos a usar Gparted, una utilidad de Linux.

Se debe primero seleccionar el dispositivo montado, y desmontar cada partición que hay en el. Luego borrar esas particiones y crear una nueva. Por simplicidad para un nuevo uso lo formateamos con un formato de `fat32`.

Aquí se ve como borramos las particiones:
![borrado_particiones.png](Imagenes_anexos/Raspbian/borrado_particiones.png)

Aquí las operaciones se han completado y tenemos la SD con su tamaño total lista para usarse nuevamente.
![borrado_particiones_2.png](Imagenes_anexos/Raspbian/borrado_particiones_2.png)

## Instalación de Raspbian

Se usa la herramienta Etcher. Se puede descargar desde https://www.balena.io/etcher/![logo balena etcher](Imagenes_anexos/Raspbian/logo balena etcher.png)

Luego se selecciona el archivo comprimido que contiene al sistema operativo, el dispositivo flash donde se va a cargar y se inicia.

![etcher](Imagenes_anexos/Raspbian/etcher.png)

![file:///D:/Teleco/PPS/INFORME%20FINAL/Imagenes/imagenes%20informe%20y%20anexos/Raspbian/etcher_starting.png](Imagenes_anexos/Raspbian/etcher_starting.png)

Una vez terminado el proceso ya se puede insertar la tarjeta micro SD en la Raspberry Pi 3B+.

## Configuración inicial Raspbian

Se crea una nueva password que es `it10` para el usuario `pi`.

![2019-02-19-112702_1440x900_scrot](Imagenes_anexos/Raspbian/2019-02-19-112702_1440x900_scrot.png)

Luego se debe seleccionar una red WiFi, para poder realizar actualizaciones.

![2019-02-19-112718_1440x900_scrot](Imagenes_anexos/Raspbian/2019-02-19-112718_1440x900_scrot.png)

Y por ultimo, se reinicia para aplicar los cambios.

![2019-02-19-115604_1440x900_scrot](Imagenes_anexos/Raspbian/2019-02-19-115604_1440x900_scrot.png)

### Habilitar SSH en Raspbian

Por motivos de seguridad, las nuevas imágenes de Raspbian vienen con el server ssh deshabilitado. Esto es porque el usuario y el password por defecto son de dominio público y una RPi (Raspberry Pi) accesible por ssh podría ser fácilmente atacada por gente indeseable si su contraseña no ha sido cambiada. Evidentemente la RPi en un entorno seguro no correría en principio riesgos, pero si la exponemos directamente a Internet (abriendo el puerto 22) o la conectamos en un lugar público, los riesgos son más que evidentes.

En primer lugar se recomienda cambiar la contraseña con este comando:

`passwd`

En nuestro caso la cambiamos inicialmente al momento de instalar Raspbian.
Existen varios métodos para activar el server ssh. Teniendo acceso local a la RPi, lo más fácil es utilizar el comando `sudo raspi-config` -> `5 Interfacing Options` -> P2 SSH -> `Sí.`

Tras el reinicio de la RPi, se puede comprobar que el servicio está activo con este comando:

`service ssh status`

### Crear copia de seguridad de sd con Raspbian
Usando Windows, se debe instalar el software **Win32 Disk Imager**, que se puede descargar desde https://sourceforge.net/projects/win32diskimager/. 

Una vez instalado el programa, se conecta la unidad sd, se indica la que corresponde para la que se hara la copia de seguridad. En este caso es la unidad **E:\ **. Luego se da una ubicacion y nombre de la copia de seguridad, con extension **.img**. Por ultimo se da click en el boton **Read**.

![back up sd progreso](Imagenes_anexos/Raspbian/back_up_sd_progreso.png)

Para restaurar con la copia hecha previamente se conecta la unidad y con el mismo software, indicando la unidad y la ubicación de la copia, se presiona sobre **Write**.


