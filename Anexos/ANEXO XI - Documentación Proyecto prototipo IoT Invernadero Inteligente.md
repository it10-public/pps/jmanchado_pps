# ANEXO XI

## Proyecto "Invernadero Inteligente IoT" usando WeMos D1 Mini, Node-RED, InfluxDB y Grafana

## Introducción

Gracias al módulo IoT de la Edukit10 se puede montar un invernadero inteligente. Además, usando otras herramientas y plataformas open-source se puede potenciar. La idea general es tomar datos del ambiente a través de sensores con la Edukit, vía WiFi con una placa WeMos D1 mini se envían los datos usando protocolo MQTT a un broker instalado en una Raspberry Pi, que luego serán leídos y guardados en una base de datos temporal, InfluxDB. Por último, se busca obtener graficas en tiempo real, con Grafana, de lo que esta sucediendo en nuestro invernadero.

## ¿Que necesitamos?
- Edukit10 y su respectivo cable USB
- WeMos D1 mini
- Raspberry Pi 3+
- Cable USB-microUSB
- Sensor de monóxido de carbono MQ-9
- Sensor de humedad para tierra YL-69
- Protoboard
- Cables macho-macho y macho-hembra

## Conexión de elementos

Por una cuestión de orden, se creo una placa PCB, pero con una protoboard se puede lograr los mismos resultados.

### Sensor de humedad
![YL-69](https://www.electronicoscaldas.com/2621-thickbox_default/sensor-de-humedad-en-suelo-yl-69.jpg)
Este sensor puede medir la cantidad de humedad presente en el suelo que lo rodea empleando dos electrodos que pasan corriente a través del suelo, y lee la resistencia. Mayor presencia de agua hace que la tierra conduzca electricidad más fácil (Menor resistencia), mientras que un suelo seco es un conductor pobre de la electricidad (Mayor resistencia).

Para aplicación como sensor de humedad en suelo por largos periodos de tiempo, se recomienda alimentar el módulo electrónico automáticamente para encenderlo únicamente al momento de tomar las mediciones, apagándolo inmediatamente al terminar y así minimizar la corrosión electrolítica. También se puede intercambiar las conexiones del elemento sensor periódicamente para que los dos electrodos roten de polaridad.

Características:

- Medida análoga de la humedad con salida de variación de voltaje (AO)
- Señal digital de superación de umbral con salida para el usuario (DO) y LED indicador. La sensibilidad de disparo se puede ajustar mediante trimmer. Esta función es provista por un comparador con LM393
- Pines de conexión de la tarjeta: VCC: alimentación, GND: Tierra, DO: Salida digital indicadora de superación de umbral, AO: Salida análoga de la medición de humedad
- LED indicador de encendido
- Voltaje de alimentación: 2 V a 6 V
- Dos agujeros de sujeción en el sensor de diámetro 3 mm aprox. y un agujero de sujeción en el módulo electrónico de 2 mm aprox.
- Dimensiones aprox: Sensor 6 cm x 2 cm. Módulo electrónico 4 cm x 1.5 cm

### Esquema de montaje

Las conexiones de este sensor son bastante simples, la sonda no posee polaridad y por lo tanto es igual  como la conectemos al modulo de medición, el resto de conexiones son de alimentación y la salida del modulo que será análoga o digital dependiendo de cual queramos usar (en este caso usaremos la análoga).

Las conexiones que usaremos para el prototipo son:

| **Modulo medición** | **Edukit** |
| ------------------- | ---------- |
| VCC                 | 5V         |
| GND                 | GND        |
| A0                  | A4         |

### Sensor de gases MQ-9
![MQ-9](https://www.geekfactory.mx/wp-content/uploads/2015/12/sensor_de_gas_mq9_1.jpg)
Este sensor de gas es sensible al monóxido de carbono (CO), pero también es sensible a gases inflamables (Gas Natural y Butano). Tiene alta sensibilidad (ajustable mediante potenciómetro) y un tiempo de respuesta rápido. Dispone de una capa sensible de Dióxido de Estaño  (SnO2). El monóxido de carbono se detecta a temperatura de calentamiento baja (1.4V) mediante ciclos de temperatura elevada y baja. La conductividad eléctrica incrementa con la concentración de monóxido de carbono en el aire. El sensor tiene 6 pines, 4 para la medición de la señal y 2 para el calentador, sin embargo el módulo dispone directamente de un pin analógico, otro digital, además de alimentación y masa.

Características:

- Tensión de alimentación: 5V
- Concentración: 20-2000ppm CO, 500ppm-10000ppm CH4,  500ppm-10000ppm LPG
- Temperatura de calentamiento: Alta (5V), Baja (1.4V).
- Sensibilidad ajustable con potenciómetro.
- Resistencia de calentamiento: 31Ω±3Ω(Temperatura ambiente)
- Potencia de calentamiento: ≤350mW
- Temperatura y Humedad: -20℃ 50℃
- RL: 1K

Propiedades eléctricas:

- Voltaje de entrada: DC5V consumo de energía (actual): 150mA
- Salida do: TTL digitales 0 y 1 (0.1 y 5 v)
- Sa: cerca de 0.1-0.3 V (relativamente limpio), la mayor concentración de tensión 4 V
- Nota especial: después de que el sensor se activa, necesita calentarse alrededor de 20 segundos, los datos medidos se estabilizan.

### Esquema de montaje

El esquema eléctrico es sencillo. Alimentamos el módulo conectando GND y 5V a los pines correspondientes de la Edukit.

En este caso la el pin digital no se conecta a nada, solo el analógico, que es por donde se leerán los datos obtenidos por el sensor.

Las conexiones que usaremos para el prototipo son:

| **Modulo medición** | **Edukit** |
| ------------------- | ---------- |
| VCC                 | 5V         |
| GND                 | GND        |
| A0                  | A5         |

## Edukit10
Con la placa Edukit conectada a la PC, ingresamos al Arduino IDE. Luego deberemos descargar la librería Arduino Json(https://arduinojson.org/). Para esto, vamos a `Programa` -> `Inlcuir Librería` -> `Administrar Bibliotecas`. Una vez allí, deberemos usar el buscador.

![arduinojson libreria.png](Imagenes_anexos/Definitivo/Edukit/arduinojson_libreria.png)

Seleccionamos la librería desarrollada por Benoit Blanchon. Tener en cuenta que se esta utilizando la version 5.13.2

Luego procedemos a configurar Arduino IDE para cargar nuestro codigo. En el menu `Herramientas` seleccionamos lo siguiente:

- Placa: "Arduino Nano"
![seleccionar arduino nano.png](Imagenes_anexos/Definitivo/Edukit/seleccionar_arduino_nano.png)

- Procesador: "ATmega328p (Old Bootloader)
![seleccionar oldbootloader.png](Imagenes_anexos/Definitivo/Edukit/seleccionar_oldbootloader.png)

- Puerto: El que sea necesario. El sistema operativo debería detectarlo cuando se conecta al equipo. (En este caso COM9, puede ser otro)
![seleccionar puerto.png](Imagenes_anexos/Definitivo/Edukit/seleccionar_puerto.png)

Una vez configurado como se cargará el programa a la Edukit, se ingresa el siguiente código:

```c++
#include <SoftwareSerial.h>
#include <ArduinoJson.h>

SoftwareSerial mySerial(10, 9); // RX, TX

//############################## Sensor CO - MQ-9 #######################################
const int MQ9_PIN = A5; //PIN ANALOGICO DE MQ-9
//#######################################################################################

//############################## Humedad - YL69 #########################################
const int HUM_PIN = A4; //PIN ANALOGICO DE YL69
int Humedad=0;//Variable utilizada para calcular la humedad
int Lectura_Analogica=0;//variable para leer el valor del pin analogico
//#######################################################################################

//############################## Sensor de Luz - LDR ####################################
const int LDR_PIN = A7;
int val_LDR = 0;
//#######################################################################################

//############################ Temperatura - LM35 #######################################
const int TEMP_PIN = A6;
int lecturaADC = 0;
double voltajeLM35 = 0.0;
double TemperaturaLM35 = 0.0;
//#######################################################################################

//############################## Variables Generales ####################################
unsigned long tiempo1 = 0;
unsigned long tiempo2 = 0;

unsigned long tiempo_pub1;
unsigned long tiempo_pub2;
int size_char = 12;
const int DELAY = 2000;
const int seg = 1000;

String topic = "testpps/";
//Esto es lo que quiero mandar para interactuar con InfluxDB:
//String mensaje = 'nodo' + ',' + MQ9_valor + ',' + hum + ',' + ldr + ',' + temp;
String topic_redundante = "testpps/control/";
//#######################################################################################

void setup()
{
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.println("Inicio!");
  mySerial.begin(9600);

  tiempo1=millis();
}
 
void loop()
{
  tiempo2=millis();
  if(tiempo2 > (tiempo1+(30*seg))){
    tiempo1=millis();

//############################## Sensor CO - MQ-9 #######################################
    int raw_adc_MQ9 = analogRead(MQ9_PIN);
    float value_adc_MQ9 = raw_adc_MQ9 * (5.0 / 1023.0);
    char MQ9_valor[size_char];
    dtostrf(value_adc_MQ9, 5, 2, MQ9_valor);
    
//############################## Humedad - YL69 #########################################
    Lectura_Analogica = analogRead(HUM_PIN);//Leer el valor del potenciometro 
    Humedad = map(Lectura_Analogica, 0, 1023, 100, 0);//Escala para utilizarlo con el servo 
    char hum[size_char];
    dtostrf(Humedad, 5, 2, hum);
    //(variable float, ancho, cantidad de decimales, variable char)
    
//############################ Temperatura - LM35 #######################################
    lecturaADC = analogRead(TEMP_PIN);
    voltajeLM35 = ((double)lecturaADC/1023)*5;
    TemperaturaLM35 = voltajeLM35/0.01;
    //char temp[5];
    char temp[size_char];
    dtostrf(TemperaturaLM35, 5, 2, temp);    
    
//############################## Sensor de Luz - LDR ####################################
    val_LDR = analogRead(LDR_PIN);
    char ldr[size_char];
    dtostrf(val_LDR, 5, 2, ldr); 

//############################## Publicación al broker ####################################
    String mensaje;
    mensaje.concat("edukit");
    mensaje.concat(",");
    mensaje.concat("mq9=");
    mensaje.concat(MQ9_valor);
    mensaje.concat(",");
    mensaje.concat("hum=");
    mensaje.concat(hum);
    mensaje.concat(",");
    mensaje.concat("ldr=");
    mensaje.concat(ldr);
    mensaje.concat(",");
    mensaje.concat("temp=");
    mensaje.concat(temp);
    
    pub_mqtt(topic, mensaje);

// ESPERANDO FINALIZAR TRANSMISION SERIAL
    tiempo_pub1 = millis();
    tiempo_pub2 = millis();
    while (tiempo_pub2 < (5*seg)+tiempo_pub1) {
      tiempo_pub2 = millis();
    }
    /*Hay que darle algo de tiempo entre publicacion y publicacion al broker porque arduino no procesa todo al instante. Se le da un tiempo de 3segundos entre envio y envio.*/

    StaticJsonBuffer<80> jsonBuffer;
    char json[80];
    JsonObject& root = jsonBuffer.createObject();

    root["nodo"] = "edukit";
    //Se formatea el char del sensor MQ-9 a string y luego a Float, porque asi ya viene redondeado a 2 decimales
    String inString = "";
    inString += MQ9_valor;
    root["mq9"] = inString.toFloat();
    inString = "";

    root["hum"] = Humedad;
    root["ldr"] = val_LDR;

    inString += temp;
    root["temp"] = inString.toFloat();
    
    root.printTo(json, sizeof(json));
    pub_mqtt(topic_redundante,json);
  }
}
//#########################----------------FUNCIONES--------------------########################
void pub_mqtt(String topic, String payload) //que sea recurrente
{
  String a;
  a = topic + ';' + payload;
  Serial.println(a);
  mySerial.println(a);
}
```

Como vimos antes, se necesitan las librerías `ArduinoJson` y `SoftwareSerial` (esta última ya incluida en la IDE de Arduino). Se hace comunicación serial por software con la WeMos D1 Mini a través de los pines 10 y 9, como Rx y TX, respectivamente.

La primer parte del código esta compuesta por declaración de variables y pines. Si bien se toma una decisión de conexión en cuanto al sensor MQ-9 y el YL-69, se podrían usar otros, que sean analógicos. Para más información revisar la documentación y el detalle del pinout de la placa.

La segunda parte del código, ya mas orientada a la ejecución, se encarga de la toma de datos de los sensores, conversión analógica/digital, y publicación al broker MQTT. Para todos los sensores, excepto el LDR, la conversión analógica/digital se hace teniendo en cuenta 10 bits, cuantizados en 5 volt, la tensión continua que toma cada uno de ellos. Por ejemplo:
```Java
float_value_adc_MQ = raw_adc_MQ9 * (5.0 / 1023.0);
```
Luego se convierten a una cadena de caracteres que tiene como máximo dos decimales con `dtostrf`.

En la parte de publicación al broker se van concatenando las cadenas de caracteres con el dato de cada sensor, separado en nombre y valor. Esto más tarde lo va a leer Node-RED para darle el formato necesario para guardarlo en la base de datos temporal InfluxDB. Se puede ver que hay dos publicaciones, es solo por una cuestión de redundancia de datos, en otro topic, que no estará leyendo particularmente Node-RED, y que además se escribe como JSON, para un posterior tratamiento si se considera necesario.

Una cuestión no menor es que hay dos temporizadores. Uno que se provee con la finalidad setear el tiempo entre transmisiones de datos a la base de datos, y el otro separar en tiempo las transmisiones a los dos topics MQTT. El segundo existe porque al usar un enlace serial entre la WeMos D1 mini y la Edukit tiene una transmisión bastante lenta, y termina siendo un cuello de botella para el flujo de datos. Para el volumen de sensores que se están manejando se toma como prudente un tiempo de 5 segundos entre transmisión y transmisión. Puede variar de acuerdo a la tasa de transmisión elegida para la conexión serial por software.

## WeMos D1 Mini
El WeMos D1 mini ESP8266 es una tarjeta de desarrollo similar a Arduino, especialmente orientada al Internet de las cosas (IoT). Está basado en el SoC (System on Chip) ESP8266, un chip altamente integrado, diseñado para las necesidades de un mundo conectado. Integra un potente procesador con Arquitectura de 32 bits (más potente que el Arduino Due) y conectividad Wifi.

WeMos está diseñado especialmente para trabajar en protoboard o soldado sobre una placa. Posee un regulador de voltaje en placa que le permite alimentarse directamente del puerto USB. Los pines de entradas/salidas trabajan a 3.3V. El chip CH340G se encarga de la comunicación USB-Serial.

De forma muy resumida estas son algunas de las principales características:

- Velocidad: 80MHz/160MHz
- Flash: 4M bytes
- Tensión funcionamiento: 3.3V
- Entradas y salidas digitales: 11, todos (salvo el D0) con PWM, interrupciones, e I2C
- Entradas analógicas: 1 (Max. 3.2V)
- Conector Micro-USB

Una de las características que hacen especial al microcontrolador ESP8266 es el hecho de poder usar firmware alternativos para poder hacer usos del mismo sin necesidad de tener que programar. Entre esas alternativas nos inclinamos por Tasmota, firmware muy completo que permite el uso de numerosos dispositivos de una forma más genérica.

### Tasmota

Firmware alternativo para dispositivos basados en ESP8266 con web, temporizadores, actualizaciones de firmware 'Over The Air' (OTA) y compatibilidad con sensores, lo que permite el control bajo Serial, HTTP, MQTT y KNX, para su uso en Smart Home Systems. Escrito para Arduino IDE y PlatformIO. Desarrollado principalmente para Sonoff, pero ahora compatible con muchos mas dispositivos, entre ellos la WeMos D1 mini.

Puede haber versiones nuevas, pero la que usaremos es la **v6.4.1**. Se puede descargar desde github, haciendo una clonación del repositorio o simplemente descargándolo. Desde aquí es posible obtener el firmware https://github.com/jnmanchado/Tasmota.

No es igual a la versión original del desarollador, ya que se hicieron algunas modificaciones al código para obtener lo que necesitamos. Queremos usar el firmware para publicar a un broker MQTT a través de la Edukit, que esta conectada a la WeMos D1 mini vía Software Serial.

### Puesta a punto de WeMos D1 mini
#### Software necesario

1. [PlatformIO](https://platformio.org/) (todas las bibliotecas y configuraciones necesarias están preconfiguradas en [platformio.ini])

2. [Visual Studio Code](https://code.visualstudio.com/)

En este proyecto usaremos PlatformIO.
#### Otros requerimientos

- El código fuente del firmware (Sonoff - Tasmota)
- Un broker MQTT
- Un cliente MQTT para interactuar con el módulo

#### Herramientas para la carga del firmware
##### Visual Studio Code
Cómo preparar y configurar Visual Studio Code con PlatformIO para la compilación y carga de Tasmota.

###### Descargar e instalar Visual Studio Code
Descargar [Visual Studio Code] (VSC) desde su pagina web
###### Instalar la extensión PlatformIO

Instalar la extensión *PlatformIO IDE* en VSC.

Seleccionar `Ver` - `Extensiones` y escribir *PlatformIO* en el cuadro de búsqueda.

Asegurarse de seleccionar la extensión oficial de PlatformIO.org *PlatformIO IDE* y seleccionar Instalar. Aceptar para instalar dependencias.

##### Copiar archivos

Copiar todos los archivos del código fuente de la versión Tasmota en la carpeta de trabajo de VSC.
##### Compilar Tasmota

Iniciar VSC y seleccionar `File` - `Open folder...` para apuntar a la carpeta de trabajo.

Nota: Presionar `Ctrl + Shift + P` y escribir **PlatformIO** para ver todas las opciones.

Seleccionar el firmware deseado editando el archivo `platformio.ini` según sea necesario.

##### Cargar Tasmota

Habilitar las opciones deseadas en `platformio.ini` para la carga serial como:
```Bash
; *** Subir el método de restablecimiento de serie para Wemos y NodeMCU
upload_port = COM5
; upload_speed = 512000
upload_speed = 115200
; upload_resetmethod = nodemcu
```

Para el caso de la WeMos D1 mini y el S.O. Lubuntu 18.04 usado, el `upload_port` en vez de ser **COM5** es **/dev/ttyUSB0**. Además hay que dar permisos para que platformIO pueda acceder al dispositivo.

Luego **Build**(si es necesario) y **Upload**.
	Consejo:

	En caso de que VSC muestre una gran cantidad de errores usando `PlatformIO - Intellisense`, una posible "solución" es cambiar el cpp Intelli Sense a "TAG PARSER"
	
	Esta configuración se puede cambiar en la configuración del área de trabajo mediante: Use `Ctrl` + `Shift` + `P` y escribir `Preferences: Open Workspace Settings` y escribir `intelli Sense` en el cuadro de búsqueda. Ahora cambiar el valor de `Intelli Sense Engine` a `Tag Parser`.

### Configuración inicial desde `my_user_config.h`
#### Modificando el código antes de cargarlo
Modificando el archivo `my_user_config.h` se puede tener una configuración inicial personalizada sin necesidad de operar el dispositivo desde la WebUI.
Se define el modulo para trabajar con WeMos D1 Mini con:

```css
	#define MODULE			GENERIC
```

En cuanto a la información de red:
```css
	#define WIFI_IP_ADDRESS        "0.0.0.0"         // [IpAddress1] Set to 0.0.0.0 for using DHCP or enter a static IP address
	#define WIFI_GATEWAY           "0.0.0.0"     // [IpAddress2] If not using DHCP set Gateway IP address
	#define WIFI_SUBNETMASK        "0.0.0.0"   // [IpAddress3] If not using DHCP set Network mask
	#define WIFI_DNS               "0.0.0.0"     // [IpAddress4] If not using DHCP set DNS IP address (might be equal to WIFI_GATEWAY)

	#define STA_SSID1              "[Ssid1]"                // [Ssid1] Wifi SSID
	#define STA_PASS1              "[Password1]"                // [Password1] Wifi password
	#define STA_SSID2              "[Ssid2]"                // [Ssid2] Optional alternate AP Wifi SSID
	#define STA_PASS2              "[Password2]"                // [Password2] Optional alternate AP Wifi password
	#define WIFI_CONFIG_TOOL       WIFI_RETRY        // [WifiConfig] Default tool if wifi fails to connect

```

Para configurar el broker MQTT:
```css
	#define MQTT_HOST              "direccion-MqttHost"                // [MqttHost]
	[...]
	#define MQTT_PORT              1883              // [MqttPort] MQTT port (10123 on CloudMQTT)
	#define MQTT_USER              "[MqttUser]"       // [MqttUser] MQTT user
	#define MQTT_PASS              "[MqttPassword]"       // [MqttPassword] MQTT password
	[...]
	#define MQTT_TOPIC             "Nombre-topic"           // [Topic] (unique) MQTT device topic, set to 'PROJECT "_%06X"' for unique topic including device MAC address
```
Luego puede haber mas o menos parámetros configurables, pero que es posible hacerlo desde un primer momento antes de compilar y flashear la placa, y de esta forma evitar usar la WebUI.

#### Configuración del dispositivo desde la WebUI
Si no se le da información de red para conectarse a su red WiFi, puede cargar el código y unirse a la red propia de Tasmota, y luego modificar los parámetros. Una vez conectado a la misma red, se accede al dispositivo entrando con un navegador web a http:\\[ip-WeMos] (reemplazar por la IP correspondiente). Obtendremos algo como lo siguiente:

![principal.png](Imagenes_anexos/tasmota/principal.png)

Ingresando a `Configuration` se pueden modificar todos los parámetros necesarios para poder trabajar. En nuestro caso necesitamos configurar el tipo de modulo, información de red y el broker MQTT.

![configuracion.png](Imagenes_anexos/tasmota/configuracion.png)

- Conexión a una red
  Ingresando a `Configure Wifi` se pueden configurar dos redes con sus SSID y contraseñas respectivas. Esto es porque por defecto Tasmota intenta conectarse a la primer red, si falla, prueba con la segunda, para garantizar la conexión Wireless en todo momento.

  ![wifi.png](Imagenes_anexos/tasmota/wifi.png)

Presionar **Save** para guardar configuración.

- Broker MQTT
  Se procede a configurar los parametros para poder interactuar con el dispositivo a traves del protocolo de mensajería MQTT. Como host se pone la direccion IP del equipo o la Raspberry Pi, en este caso. El puerto podemos dejar 1883, y luego el nombre de cliente se puede ingresar el que desee. No sera necesario poner usuario y contraseña. No se indagará demasiado en la seguridad por el momento.

  ![mqtt.png](Imagenes_anexos/tasmota/mqtt-blur.png)

Presionar **Save** para guardar configuración.

### Pines para comunicación serial por software de WeMos D1 mini

El ESP8266 tiene dos UARTS (puertos serie): UART0 en los pines 1 y 3 (TX0 y RX0 respectivamente), Y UART1 en los pines 2 y 8 (TX1 y RX1 respectivamente). Sin embargo, GPIO8 se utiliza para conectar el chip flash . Esto significa que UART1 solo puede transmitir datos.

UART0 también tiene control de flujo de hardware en los pines 15 y 13 (RTS0 y CTS0 respectivamente). Estos dos pines también pueden usarse como pines alternativos TX0 y RX0.

Se realiza una conexión serial por software, y para esto se utilizan pines digitales. En este caso GPIO5(D1) y GPIO4(D2) (SerBr Tx y SerBr Rx respectivamente).

![pines serial.png](Imagenes_anexos/tasmota/serial/pines_serial.png)

## Node-RED
Node-RED (https://nodered.org/) es un motor de flujos con enfoque IoT, que permite definir gráficamente flujos de servicios, a través de protocolos estándares como REST, MQTT, Websocket, AMQP… además de ofrecer integración con apis de terceros, tales como Twitter, Facebook, Yahoo!

Se trata de una herramienta visual muy ligera, programada en NodeJS y que puede ejecutarse desde en dispositivos tan limitados como una Raspberry, hasta en plataformas complejas como IBM Bluemix, Azure IoT o Sofia2 Platform.

El editor de flujos de Node-RED consiste en una sencilla interfaz en HTML, accesible desde cualquier navegador, en la que arrastrando y conectando nodos entre sí, es posible definir un flujo que ofrezca un servicio.

![Node-RED](https://camo.githubusercontent.com/01ed64b01d73046a485ea82b645a3be529c64809/687474703a2f2f6e6f64657265642e6f72672f696d616765732f6e6f64652d7265642d73637265656e73686f742e706e67)

### Instalación de Node-RED
Usando un equipo que corra como sistema operativo con kernel Linux, en una terminal ingresamos el comando:

```Bash
bash <(curl -sL https://raw.githubusercontent.com/node-red/raspbian-deb-package/master/resources/update-nodejs-and-nodered)
```

En instalaciones mínimas de Debian puede que haga falta correr `sudo apt-get install build-essential` antes del comando anterior.

### Corriendo Node-RED

Para iniciar Node-RED, hay dos formas:

- En el escritorio, seleccionar `Menu -> Programming -> Node-RED`
- O por medio de la terminal,  `node-red-start`

Nota: cerrar la ventana (o `ctrl + c`) no frena la ejecución de Node-RED (seguirá corriendo en segundo plano)

Para parar Node-RED, ejecutar el comando `node-red-stop`.

Para ver el log, ejecutar el comando `node-red-log`.

### Inicio automático en el booteo

Usar comando:

```Bash
sudo systemctl enable nodered.service
```

Y de la misma manera, hacer `sudo systemctl disable nodered.service` para deshabilitar el inicio automático de Node-RED en el booteo.

#### Instalación de nuevos nodos

Instalaremos nuevos nodos para trabajar, estos son:

- Dashboard
	- `npm install node-red-dashboard`
	Nos permitirá realizar web dinámicas para mostrar nuestros resultados.
- Mqtt-Broker
	-  `npm install node-red-contrib-mqtt-broker`
	Crea un broker local, que usaremos para vincular la Edukit con la base de datos InfluxDB
- InfluxDB
	-  `npm install node-red-contrib-influxdb`
	Nodos para insertar y realizar consultas sobre una base de datos influxDB.

Hay dos formas de poder instalarlos, que describiremos a continuación.

- **Usando el editor:**

Desde la versión 0.15 se pueden instalar nodos directamente usando el editor. para esto se debe ir a **Manage Palette** desde el **menu**, luego seleccionar la pestaña **install**. Desde aquí se pueden buscar nuevos nodos, instalarlos, actualizarlos, habilitar o deshabilitar nodos existentes.

- Usando nodos con paquetes npm

Se puede instalar de manera local en el directorio del usuario(por defecto, $HOME/.node-red):

```Bash
	cd $HOME/.node-red
	npm install <npm-package-name>
```

Después de esto se debe hacer stop y restart sobre Node-RED para cargar los nuevos nodos.

### Creando flujo

Para empezar a trabajar con Node-RED se debe iniciar el servicio, como se explico antes, y luego acceder por el navegador a http://localhost:1880, o bien la dirección IP del equipo remoto que tiene instalada la aplicación.

![node red flujo sin nada.png](Imagenes_anexos/Definitivo/Node-RED/node_red_flujo_sin_nada.png)

Si nos dirigimos al sector superior derecho de la web, presionamos ahí, vamos **Import**, **Clipboard** y luego copiamos y pegamos este código:
	

```json
[
    {
        "id": "230f45d8.6f7a52",
        "type": "tab",
        "label": "Definitivo",
        "disabled": false,
        "info": ""
    },
    {
        "id": "c4539125.7c9968",
        "type": "function",
        "z": "230f45d8.6f7a52",
        "name": "Formato mensaje",
        "func": "msg.payload = {\n    name: msg.msg433.name,\n    mq9: msg.msg433.mq9,\n    hum: msg.msg433.hum,\n    ldr: msg.msg433.ldr,\n    temp: msg.msg433.temp,\n}\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 590,
        "y": 440,
        "wires": [
            [
                "94c0dd83.2c07d8"
            ]
        ]
    },
    {
        "id": "94c0dd83.2c07d8",
        "type": "influxdb out",
        "z": "230f45d8.6f7a52",
        "influxdb": "be79df3d.f330d",
        "name": "Influxdb",
        "measurement": "invernadero",
        "precision": "s",
        "retentionPolicy": "",
        "x": 800,
        "y": 440,
        "wires": []
    },
    {
        "id": "5de4b796.d1efa8",
        "type": "mqtt in",
        "z": "230f45d8.6f7a52",
        "name": "",
        "topic": "testpps/",
        "qos": "0",
        "broker": "16e1aa9a.dc41f5",
        "x": 130,
        "y": 100,
        "wires": [
            [
                "c6c846c1.05ed5"
            ]
        ]
    },
    {
        "id": "c6c846c1.05ed5",
        "type": "function",
        "z": "230f45d8.6f7a52",
        "name": "Parseo de mensaje MQTT",
        "func": "var msg433 = {};\nmsg.payload = msg.payload.replace(/(\\r\\n|\\n|\\r)/gm,\"\");\n\nvar parts433 = msg.payload.split(\",\");\n\nmsg433.name = parts433[0];\nfor (var i=1; i<parts433.length; i++) {\n    var keyvalue = parts433[i].split(\"=\");\n    if (keyvalue.length===2) {\n        msg433[keyvalue[0]] = keyvalue[1];\n    }\n}\n\nmsg.msg433 = msg433;\n//msg.topic=\"rflink\";\nmsg.topic=\"testpps/\";\nreturn msg;\n\n/*\n\n// So firstly a generic means of getting incoming items into an object\n\nvar the433 = {};\nmsg.payload = msg.payload.replace(/(\\r\\n|\\n|\\r)/gm,\"\");\nnode.warn(msg.payload);\nvar parts433 = msg.payload.split(\";\");\n\nthe433.p1 = parts433[0];\nthe433.p2 = parts433[1];\nthe433.name = parts433[2];\n\nvar a = 3;\nwhile (a < parts433.length) {\n    var bits433 = parts433[a].split(\"=\");\n    switch (bits433[0]) {\n        case \"ID\": the433.id = bits433[1]; break;\n        case \"SWITCH\": the433.switch = bits433[1]; break;\n        case \"CMD\": the433.cmd = bits433[1]; break;\n        case \"SET_LEVEL\": the433.set_level = parseInt(bits433[1], 10); break;\n        case \"TEMP\": the433.temp = parseInt(bits433[1], 16) / 10; break;\n        case \"HUM\": the433.hum = parseInt(bits433[1], 10); break;\n        case \"BARO\": the433.baro = parseInt(bits433[1], 16); break;\n        case \"HSTATUS\": the433.hstatus = parseInt(bits433[1], 10); break;\n        case \"BFORECAST\": the433.bforecast = parseInt(bits433[1], 10); break;\n        case \"UV\": the433.uv = parseInt(bits433[1], 16); break;\n        case \"LUX\": the433.lux = parseInt(bits433[1], 16); break;\n        case \"BAT\": the433.bat = bits433[1]; break;\n        case \"RAIN\": the433.rain = parseInt(bits433[1], 16) / 10; break;\n        case \"RAIN\": the433.rainrate = parseInt(bits433[1], 16) / 10; break;\n        case \"WINSP\": the433.winsp = parseInt(bits433[1], 16) / 10; break;\n        case \"AWINSP\": the433.awinsp = parseInt(bits433[1], 16) / 10; break;\n        case \"WINGS\": the433.wings = parseInt(bits433[1], 16); break;\n        case \"WINDIR\": the433.windir = parseInt(bits433[1], 10); break;\n        case \"WINCHL\": the433.winchl = parseInt(bits433[1], 16); break;\n        case \"WINTMP\": the433.wintmp = parseInt(bits433[1], 16); break;\n        case \"CHIME\": the433.chime = parseInt(bits433[1], 10); break;\n        case \"SMOKEALERT\": the433.smokealert = bits433[1]; break;\n        case \"PIR\": the433.pir = bits433[1]; break;\n        case \"CO2\": the433.co2 = parseInt(bits433[1], 10); break;\n        case \"SOUND\": the433.sound = parseInt(bits433[1], 10); break;\n        case \"KWATT\": the433.kwatt = parseInt(bits433[1], 16); break;\n        case \"WATT\": the433.watt = parseInt(bits433[1], 16); break;\n        case \"CURRENT\": the433.current = parseInt(bits433[1], 10); break;\n        case \"CURRENT2\": the433.current2 = parseInt(bits433[1], 10); break;\n        case \"CURRENT3\": the433.current3 = parseInt(bits433[1], 10); break;\n        case \"DIST\": the433.dist = parseInt(bits433[1], 10); break;\n        case \"METER\": the433.meter = parseInt(bits433[1], 10); break;\n        case \"VOLT\": the433.volt = parseInt(bits433[1], 10); break;\n        case \"RGBW\": the433.rgbc = parseInt(bits433[1].substring(0, 2), 16);\n            the433.rgbw = parseInt(bits433[1].substring(2, 4), 16); break;\n    }\n    a++;\n}\n\n// SO - the above is general... here is my specific setup for temporarily displaying\n// the Acurite info\nif ((the433.p1 == \"20\") && (the433.name == \"Acurite\") && (the433.id == \"c826\")) {\n    if (typeof the433.temp !== 'undefined') temp = the433.temp;\n    if (typeof the433.hum !== 'undefined') hum = the433.hum;\n    if (typeof the433.bat !== 'undefined') bat = the433.bat;\n    if (typeof the433.rain !== 'undefined') rain = the433.rain;\n    if (typeof the433.winsp !== 'undefined') winsp = the433.winsp;\n    if (typeof the433.windir !== 'undefined') windir = the433.windir;\n\n    node.warn(\"Temperature: \" + temp + \"c\");\n    node.warn(\"Humidity: \" + hum + \"%\");\n    node.warn(\"Battery: \" + bat);\n    node.warn(\"Rain: \" + rain + \"mm\");\n    node.warn(\"Wind Speed: \" + winsp + \"km/h\");\n    node.warn(\"Wind Dir: \" + (windir * 22.5) + \" degrees\");\n}\n\n*/",
        "outputs": 1,
        "noerr": 0,
        "x": 340,
        "y": 180,
        "wires": [
            [
                "2b375d5f.5623ca"
            ]
        ]
    },
    {
        "id": "2b375d5f.5623ca",
        "type": "function",
        "z": "230f45d8.6f7a52",
        "name": "MQ-9 conversion",
        "func": "if (msg.msg433.mq9 !== undefined) {\n    msg.msg433.mq9 = parseFloat(msg.msg433.mq9 , 10);\n}\nelse msg.msg433.mq9 =-999.0;\nnode.status({fill:\"blue\",shape:\"ring\",text: msg.msg433.mq9  });\nreturn msg;\n",
        "outputs": 1,
        "noerr": 0,
        "x": 310,
        "y": 260,
        "wires": [
            [
                "8f034c1a.1b7428"
            ]
        ]
    },
    {
        "id": "8f034c1a.1b7428",
        "type": "function",
        "z": "230f45d8.6f7a52",
        "name": "Humedad conversion",
        "func": "if (msg.msg433.hum !==undefined) {\n    msg.msg433.hum = parseFloat(msg.msg433.hum , 10);\n}\nelse msg.msg433.hum =-999.0;\nnode.status({fill:\"blue\",shape:\"ring\",text: msg.msg433.hum  });\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 320,
        "y": 320,
        "wires": [
            [
                "7bb503f4.b5778c"
            ]
        ]
    },
    {
        "id": "cf6930a7.38dd9",
        "type": "comment",
        "z": "230f45d8.6f7a52",
        "name": "Suscripción al broker local",
        "info": "",
        "x": 190,
        "y": 40,
        "wires": []
    },
    {
        "id": "bc978e8d.5cd0b8",
        "type": "comment",
        "z": "230f45d8.6f7a52",
        "name": "Configuracion del broker local",
        "info": "",
        "x": 680,
        "y": 260,
        "wires": []
    },
    {
        "id": "cc0daab2.cf179",
        "type": "ui_template",
        "z": "230f45d8.6f7a52",
        "group": "124a1fa3.a5e4c8",
        "name": "frame mq9",
        "order": 2,
        "width": "0",
        "height": "0",
        "format": "<!--<iframe src=\"http://192.168.20.129:3000/d-solo/TQrnackRk/prueba-valor1?orgId=1&from=1551883611046&to=$msg.payload&refresh=10s&panelId=2\" width=\"450\" height=\"200\" frameborder=\"0\"></iframe>-->\n<!--<iframe src=\"http://192.168.20.129:3000/d-solo/CelyNFmgz/edicion_escenario1-edukit?orgId=1&panelId=18&from=1552863382441&to=1552949782445\" width=\"543\" height=\"330\" frameborder=\"0\"></iframe> --->\n<iframe src=\"http://192.168.20.131:3000/d-solo/CelyNFmgz/edicion_escenario1-edukit_2?orgId=1&refresh=10s&from=1553010978259&to=($msg.payload)&panelId=18\" width=\"543\" height=\"330\" frameborder=\"0\"></iframe>",
        "storeOutMessages": true,
        "fwdInMessages": true,
        "templateScope": "local",
        "x": 1270,
        "y": 260,
        "wires": [
            []
        ]
    },
    {
        "id": "7bb503f4.b5778c",
        "type": "function",
        "z": "230f45d8.6f7a52",
        "name": "LDR conversion",
        "func": "if (msg.msg433.ldr !==undefined) {\n    msg.msg433.ldr = parseInt(msg.msg433.ldr , 10);\n}\nelse msg.msg433.ldr =-999.0;\nnode.status({fill:\"blue\",shape:\"ring\",text: msg.msg433.ldr  });\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 300,
        "y": 380,
        "wires": [
            [
                "55bdfe1c.5ced5"
            ]
        ]
    },
    {
        "id": "55bdfe1c.5ced5",
        "type": "function",
        "z": "230f45d8.6f7a52",
        "name": "Temperatura conversion",
        "func": "if (msg.msg433.temp !==undefined) {\n    msg.msg433.temp = parseFloat(msg.msg433.temp , 10);\n}\nelse msg.msg433.temp =-999.0;\nnode.status({fill:\"blue\",shape:\"ring\",text: msg.msg433.temp  });\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 330,
        "y": 440,
        "wires": [
            [
                "c4539125.7c9968"
            ]
        ]
    },
    {
        "id": "8cf93b28.f10ff8",
        "type": "ui_template",
        "z": "230f45d8.6f7a52",
        "group": "b8c3e6e8.f28eb",
        "name": "Frame Hora",
        "order": 5,
        "width": 0,
        "height": 0,
        "format": "<iframe src=\"http://192.168.20.131:3000/d-solo/CelyNFmgz/escenario1-edukit?orgId=1&from=1552863382441&to=1552949782445&panelId=4\" width=\"1100\" height=\"150\" frameborder=\"0\"></iframe>",
        "storeOutMessages": true,
        "fwdInMessages": true,
        "templateScope": "local",
        "x": 1270,
        "y": 140,
        "wires": [
            []
        ]
    },
    {
        "id": "eabf72f.462a41",
        "type": "ui_template",
        "z": "230f45d8.6f7a52",
        "group": "d579fb4d.09324",
        "name": "Frame Temperatura",
        "order": 1,
        "width": "0",
        "height": "0",
        "format": "<!--<iframe src=\"http://192.168.20.129:3000/d-solo/CelyNFmgz/edicion_escenario1-edukit?orgId=1&panelId=14&from=1552863382441&to=$msg.payload\" width=\"543\" height=\"325\" frameborder=\"0\"></iframe>-->\n<!--<iframe src=\"http://192.168.20.129:3000/d-solo/CelyNFmgz/edicion_escenario1-edukit_2?orgId=1&refresh=10s&from=1553010690649&to=$msg.payload&panelId=14\" width=\"543\" height=\"325\" frameborder=\"0\"></iframe>-->\n<iframe src=\"http://192.168.20.131:3000/d-solo/CelyNFmgz/edicion_escenario1-edukit_2?orgId=1&refresh=10s&from=1553089902485&to=$msg.payload&panelId=14\" width=\"543\" height=\"325\" frameborder=\"0\"></iframe>",
        "storeOutMessages": true,
        "fwdInMessages": true,
        "templateScope": "local",
        "x": 1290,
        "y": 180,
        "wires": [
            []
        ]
    },
    {
        "id": "b2f0f93.9458d88",
        "type": "ui_template",
        "z": "230f45d8.6f7a52",
        "group": "d579fb4d.09324",
        "name": "Frame Humedad",
        "order": 4,
        "width": 0,
        "height": 0,
        "format": "<!---<iframe src=\"http://192.168.20.129:3000/d-solo/CelyNFmgz/edicion_escenario1-edukit?orgId=1&refresh=10s&panelId=10&from=1552863382441&to=$msg.payload\" width=\"543\" height=\"325\" frameborder=\"0\"></iframe>-->\n<!--<iframe src=\"http://192.168.20.129:3000/d-solo/CelyNFmgz/edicion_escenario1-edukit_2?orgId=1&refresh=10s&from=1553010852123&to=$msg.payload&panelId=10\" width=\"543\" height=\"325\" frameborder=\"0\"></iframe>-->\n<iframe src=\"http://192.168.20.131:3000/d-solo/CelyNFmgz/edicion_escenario1-edukit_2?orgId=1&refresh=10s&from=1553089966488&to=$msg.payload&panelId=10\" width=\"543\" height=\"325\" frameborder=\"0\"></iframe>",
        "storeOutMessages": true,
        "fwdInMessages": true,
        "templateScope": "local",
        "x": 1290,
        "y": 220,
        "wires": [
            []
        ]
    },
    {
        "id": "8f78ecbe.d9ef1",
        "type": "ui_template",
        "z": "230f45d8.6f7a52",
        "group": "124a1fa3.a5e4c8",
        "name": "Frame LDR",
        "order": 1,
        "width": "0",
        "height": "0",
        "format": "<!--<iframe src=\"http://192.168.20.129:3000/d-solo/CelyNFmgz/edicion_escenario1-edukit?orgId=1&refresh=10s&panelId=8&from=1552863382441&to=1552949782445\" width=\"543\" height=\"325\" frameborder=\"0\"></iframe> ---->\n<iframe src=\"http://192.168.20.131:3000/d-solo/CelyNFmgz/edicion_escenario1-edukit_2?orgId=1&refresh=10s&from=1553011900229&to=($msg.payload)&panelId=8\" width=\"543\" height=\"325\" frameborder=\"0\"></iframe>",
        "storeOutMessages": true,
        "fwdInMessages": true,
        "templateScope": "local",
        "x": 1270,
        "y": 300,
        "wires": [
            []
        ]
    },
    {
        "id": "ac1e53eb.bd3398",
        "type": "comment",
        "z": "230f45d8.6f7a52",
        "name": "Vista rapida",
        "info": "",
        "x": 1270,
        "y": 100,
        "wires": []
    },
    {
        "id": "728df83a.c5c39",
        "type": "comment",
        "z": "230f45d8.6f7a52",
        "name": "Exportar datos historicos",
        "info": "",
        "x": 2430,
        "y": 100,
        "wires": []
    },
    {
        "id": "c1909fc1.25fe1",
        "type": "ui_template",
        "z": "230f45d8.6f7a52",
        "group": "5d22a336.67eb5c",
        "name": "Historico mq9",
        "order": 9,
        "width": "20",
        "height": "6",
        "format": "<!--<iframe src=\"http://192.168.20.129:3000/d-solo/TQrnackRk/prueba-valor1?orgId=1&from=1551883611046&to=$msg.payload&refresh=10s&panelId=2\" width=\"450\" height=\"200\" frameborder=\"0\"></iframe>-->\n<!--<iframe src=\"http://192.168.20.129:3000/d-solo/CelyNFmgz/edicion_escenario1-edukit?orgId=1&panelId=15&from=1552863382441&to=1552949782445\" width=\"1100\" height=\"300\" frameborder=\"0\"></iframe>-->\n<!--<iframe src=\"http://192.168.20.129:3000/d-solo/CelyNFmgz/edicion_escenario1-edukit_2?orgId=1&refresh=10s&from=1553012112762&to=($msg.payload)&panelId=15\" width=\"1100\" height=\"300\" frameborder=\"0\"></iframe>-->\n<div ng-bind-html=\"msg.payload\"></div>",
        "storeOutMessages": true,
        "fwdInMessages": true,
        "templateScope": "local",
        "x": 2600,
        "y": 1020,
        "wires": [
            []
        ]
    },
    {
        "id": "c6890547.33cfe",
        "type": "ui_template",
        "z": "230f45d8.6f7a52",
        "group": "a7f71eb3.656578",
        "name": "Historico Temperatura",
        "order": 3,
        "width": "20",
        "height": "6",
        "format": "<!--<iframe src=\"http://192.168.20.129:3000/d-solo/CelyNFmgz/escenario1-edukit?orgId=1&from=1552863382441&to=1552949782445&panelId=2\" width=\"1100\" height=\"300\" frameborder=\"0\"></iframe>-->\n<!--<iframe src=\"http://192.168.20.129:3000/d-solo/CelyNFmgz/edicion_escenario1-edukit_2?orgId=1&refresh=10s&from=1553012005696&to=($msg.payload)&panelId=2\" width=\"1100\" height=\"300\" frameborder=\"0\"></iframe>-->\n<div ng-bind-html=\"msg.payload\"></div>",
        "storeOutMessages": true,
        "fwdInMessages": true,
        "templateScope": "local",
        "x": 2620,
        "y": 780,
        "wires": [
            []
        ]
    },
    {
        "id": "e8985261.d749e",
        "type": "ui_template",
        "z": "230f45d8.6f7a52",
        "group": "8f212678.706158",
        "name": "Historico Humedad",
        "order": 5,
        "width": "20",
        "height": "6",
        "format": "<!--<iframe src=\"http://192.168.20.129:3000/d-solo/CelyNFmgz/edicion_escenario1-edukit?orgId=1&panelId=16&from=1552863382441&to=1552949782445\" width=\"1100\" height=\"300\" frameborder=\"0\"></iframe>-->\n<!--<iframe src=\"http://192.168.20.129:3000/d-solo/CelyNFmgz/edicion_escenario1-edukit_2?orgId=1&refresh=10s&from=1553012070609&to=($msg.payload)&panelId=16\" width=\"1100\" height=\"300\" frameborder=\"0\"></iframe>-->\n<div ng-bind-html=\"msg.payload\"></div>",
        "storeOutMessages": true,
        "fwdInMessages": true,
        "templateScope": "local",
        "x": 2610,
        "y": 860,
        "wires": [
            []
        ]
    },
    {
        "id": "4fb3d53c.4351b4",
        "type": "ui_template",
        "z": "230f45d8.6f7a52",
        "group": "afecb5f7.f3fbc8",
        "name": "Historico LDR",
        "order": 7,
        "width": "20",
        "height": "6",
        "format": "<!--<iframe src=\"http://192.168.20.129:3000/d-solo/CelyNFmgz/edicion_escenario1-edukit?orgId=1&panelId=17&from=1552863382441&to=1552949782445\" width=\"1100\" height=\"300\" frameborder=\"0\"></iframe>-->\n<!--<iframe src=\"http://192.168.20.129:3000/d-solo/CelyNFmgz/edicion_escenario1-edukit_2?orgId=1&refresh=10s&from=1553012156317&to=($msg.payload)&panelId=17\" width=\"1100\" height=\"300\" frameborder=\"0\"></iframe>-->\n<div ng-bind-html=\"msg.payload\"></div>",
        "storeOutMessages": true,
        "fwdInMessages": true,
        "templateScope": "local",
        "x": 2600,
        "y": 940,
        "wires": [
            []
        ]
    },
    {
        "id": "684d701c.b69238",
        "type": "mosca in",
        "z": "230f45d8.6f7a52",
        "mqtt_port": 1883,
        "mqtt_ws_port": 8080,
        "name": "",
        "username": "",
        "password": "",
        "dburl": "",
        "x": 650,
        "y": 300,
        "wires": [
            []
        ]
    },
    {
        "id": "e92020cd.8880a8",
        "type": "file",
        "z": "230f45d8.6f7a52",
        "name": "",
        "filename": "/home/pi/temp.csv",
        "appendNewline": true,
        "createDir": false,
        "overwriteFile": "true",
        "x": 2730,
        "y": 180,
        "wires": [
            [
                "1042c388.0e4e5c"
            ]
        ]
    },
    {
        "id": "46f9e84.7996b18",
        "type": "csv",
        "z": "230f45d8.6f7a52",
        "name": "",
        "sep": ",",
        "hdrin": false,
        "hdrout": true,
        "multi": "one",
        "ret": "\\r\\n",
        "temp": "time,name,temp",
        "skip": "0",
        "x": 2550,
        "y": 180,
        "wires": [
            [
                "e92020cd.8880a8"
            ]
        ]
    },
    {
        "id": "77ff6db4.33b624",
        "type": "ui_ui_control",
        "z": "230f45d8.6f7a52",
        "name": "Show/Hide",
        "x": 1750,
        "y": 180,
        "wires": [
            []
        ]
    },
    {
        "id": "9a0d3aea.19a5c",
        "type": "ui_date_picker",
        "z": "230f45d8.6f7a52",
        "name": "Fecha_inicial_temp",
        "label": "Fecha inicial",
        "group": "8ed24b80.43df2",
        "order": 1,
        "width": "9",
        "height": "1",
        "passthru": false,
        "topic": "Fecha_inicial_temp",
        "x": 1950,
        "y": 160,
        "wires": [
            [
                "d294b3a4.3c9a8"
            ]
        ]
    },
    {
        "id": "8c416833.629d78",
        "type": "ui_button",
        "z": "230f45d8.6f7a52",
        "name": "Exportar temp",
        "group": "a7f71eb3.656578",
        "order": 0,
        "width": "2",
        "height": "1",
        "passthru": false,
        "label": "Exportar",
        "tooltip": "Exportar datos historicos de temperatura en formato csv",
        "color": "",
        "bgcolor": "",
        "icon": "fa-file-o ",
        "payload": "{\"group\":{\"show\":[\"Registros_export_temp\",\"Registros_Fecha_2_temp\",\"Registros_boton_cancel_export\"]}}",
        "payloadType": "json",
        "topic": "",
        "x": 1560,
        "y": 160,
        "wires": [
            [
                "77ff6db4.33b624"
            ]
        ]
    },
    {
        "id": "da33d4e0.efa66",
        "type": "ui_date_picker",
        "z": "230f45d8.6f7a52",
        "name": "Fecha_final_temp",
        "label": "Fecha final",
        "group": "8ed24b80.43df2",
        "order": 2,
        "width": "9",
        "height": "1",
        "passthru": false,
        "topic": "Fecha_final_temp",
        "x": 1950,
        "y": 200,
        "wires": [
            [
                "d294b3a4.3c9a8"
            ]
        ]
    },
    {
        "id": "cc5101d9.917468",
        "type": "ui_button",
        "z": "230f45d8.6f7a52",
        "name": "Cancel temp",
        "group": "8ed24b80.43df2",
        "order": 7,
        "width": "2",
        "height": "1",
        "passthru": false,
        "label": "Cancelar",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "icon": "fa-times ",
        "payload": "{\"group\":{\"hide\":[\"Registros_export_temp\",\"Registros_Fecha_2_temp\",\"Registros_boton_cancel_export\"]}}",
        "payloadType": "json",
        "topic": "",
        "x": 1570,
        "y": 200,
        "wires": [
            [
                "77ff6db4.33b624"
            ]
        ]
    },
    {
        "id": "6a10d4c3.802384",
        "type": "comment",
        "z": "230f45d8.6f7a52",
        "name": "Seccion Web",
        "info": "",
        "x": 2070,
        "y": 20,
        "wires": []
    },
    {
        "id": "38869e96.9a44da",
        "type": "ui_ui_control",
        "z": "230f45d8.6f7a52",
        "name": "Show/Hide",
        "x": 1750,
        "y": 300,
        "wires": [
            []
        ]
    },
    {
        "id": "2da9ffd4.15dff",
        "type": "ui_date_picker",
        "z": "230f45d8.6f7a52",
        "name": "Fecha_inicial_hum",
        "label": "Fecha inicial",
        "group": "852840d5.64a668",
        "order": 1,
        "width": "9",
        "height": "1",
        "passthru": false,
        "topic": "Fecha_inicial_hum",
        "x": 1950,
        "y": 280,
        "wires": [
            [
                "2bcf0ab.41ddd76"
            ]
        ]
    },
    {
        "id": "1b1a4e8e.f3e4f9",
        "type": "ui_date_picker",
        "z": "230f45d8.6f7a52",
        "name": "Fecha_final_hum",
        "label": "Fecha final",
        "group": "852840d5.64a668",
        "order": 2,
        "width": "9",
        "height": "1",
        "passthru": false,
        "topic": "Fecha_final_hum",
        "x": 1950,
        "y": 320,
        "wires": [
            [
                "2bcf0ab.41ddd76"
            ]
        ]
    },
    {
        "id": "402d943d.28bb84",
        "type": "ui_ui_control",
        "z": "230f45d8.6f7a52",
        "name": "Show/Hide",
        "x": 1750,
        "y": 440,
        "wires": [
            []
        ]
    },
    {
        "id": "a870d237.03fa78",
        "type": "ui_date_picker",
        "z": "230f45d8.6f7a52",
        "name": "Fecha_inicial_ldr",
        "label": "Fecha inicial",
        "group": "224c07c1.06a2d",
        "order": 3,
        "width": "9",
        "height": "1",
        "passthru": false,
        "topic": "Fecha_inicial_ldr",
        "x": 1950,
        "y": 420,
        "wires": [
            [
                "f29a3e6c.d4a7d8"
            ]
        ]
    },
    {
        "id": "c3948d16.f046b8",
        "type": "ui_date_picker",
        "z": "230f45d8.6f7a52",
        "name": "Fecha_final_ldr",
        "label": "Fecha final",
        "group": "224c07c1.06a2d",
        "order": 4,
        "width": "9",
        "height": "1",
        "passthru": false,
        "topic": "Fecha_final_ldr",
        "x": 1940,
        "y": 460,
        "wires": [
            [
                "f29a3e6c.d4a7d8"
            ]
        ]
    },
    {
        "id": "b20893e3.5eb888",
        "type": "ui_ui_control",
        "z": "230f45d8.6f7a52",
        "name": "Show/Hide",
        "x": 1750,
        "y": 580,
        "wires": [
            []
        ]
    },
    {
        "id": "6481979c.4413d",
        "type": "ui_date_picker",
        "z": "230f45d8.6f7a52",
        "name": "Fecha_inicial_mq9",
        "label": "Fecha inicial",
        "group": "29206391.d60f3c",
        "order": 5,
        "width": "9",
        "height": "1",
        "passthru": false,
        "topic": "Fecha_inicial_mq9",
        "x": 1950,
        "y": 560,
        "wires": [
            [
                "d70a417d.669b08"
            ]
        ]
    },
    {
        "id": "c8d347d8.2fc6e",
        "type": "ui_date_picker",
        "z": "230f45d8.6f7a52",
        "name": "Fecha_final_mq9",
        "label": "Fecha final",
        "group": "29206391.d60f3c",
        "order": 6,
        "width": "9",
        "height": "1",
        "passthru": false,
        "topic": "Fecha_final_mq9",
        "x": 1950,
        "y": 600,
        "wires": [
            [
                "d70a417d.669b08"
            ]
        ]
    },
    {
        "id": "c151b034.ba1148",
        "type": "ui_button",
        "z": "230f45d8.6f7a52",
        "name": "Exportar hum",
        "group": "8f212678.706158",
        "order": 0,
        "width": "2",
        "height": "1",
        "passthru": false,
        "label": "Exportar",
        "tooltip": "Exportar datos historicos de humedad en formato csv",
        "color": "",
        "bgcolor": "",
        "icon": "fa-file-o ",
        "payload": "{\"group\":{\"show\":[\"Registros_export_hum\"]}}",
        "payloadType": "json",
        "topic": "",
        "x": 1560,
        "y": 280,
        "wires": [
            [
                "38869e96.9a44da"
            ]
        ]
    },
    {
        "id": "28d742da.b1139e",
        "type": "ui_button",
        "z": "230f45d8.6f7a52",
        "name": "Cancel hum",
        "group": "852840d5.64a668",
        "order": 8,
        "width": "2",
        "height": "1",
        "passthru": false,
        "label": "Cancelar",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "icon": "fa-times ",
        "payload": "{\"group\":{\"hide\":[\"Registros_export_hum\"]}}",
        "payloadType": "json",
        "topic": "",
        "x": 1570,
        "y": 320,
        "wires": [
            [
                "38869e96.9a44da"
            ]
        ]
    },
    {
        "id": "834b3480.145228",
        "type": "ui_button",
        "z": "230f45d8.6f7a52",
        "name": "Exportar ldr",
        "group": "afecb5f7.f3fbc8",
        "order": 0,
        "width": "2",
        "height": "1",
        "passthru": false,
        "label": "Exportar",
        "tooltip": "Exportar datos historicos de niveles de luz en formato csv",
        "color": "",
        "bgcolor": "",
        "icon": "fa-file-o ",
        "payload": "{\"group\":{\"show\":[\"Registros_export_ldr\"]}}",
        "payloadType": "json",
        "topic": "",
        "x": 1570,
        "y": 420,
        "wires": [
            [
                "402d943d.28bb84"
            ]
        ]
    },
    {
        "id": "e981f4c9.eac4e8",
        "type": "ui_button",
        "z": "230f45d8.6f7a52",
        "name": "Cancel ldr",
        "group": "224c07c1.06a2d",
        "order": 9,
        "width": "2",
        "height": "1",
        "passthru": false,
        "label": "Cancelar",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "icon": "fa-times ",
        "payload": "{\"group\":{\"hide\":[\"Registros_export_ldr\"]}}",
        "payloadType": "json",
        "topic": "",
        "x": 1580,
        "y": 460,
        "wires": [
            [
                "402d943d.28bb84"
            ]
        ]
    },
    {
        "id": "23e2742c.6a351c",
        "type": "ui_button",
        "z": "230f45d8.6f7a52",
        "name": "Exportar mq9",
        "group": "5d22a336.67eb5c",
        "order": 0,
        "width": "2",
        "height": "1",
        "passthru": false,
        "label": "Exportar",
        "tooltip": "Exportar datos historicos de concentración de CO en formato csv",
        "color": "",
        "bgcolor": "",
        "icon": "fa-file-o ",
        "payload": "{\"group\":{\"show\":[\"Registros_export_mq9\"]}}",
        "payloadType": "json",
        "topic": "",
        "x": 1560,
        "y": 560,
        "wires": [
            [
                "b20893e3.5eb888"
            ]
        ]
    },
    {
        "id": "dfd6e162.99691",
        "type": "ui_button",
        "z": "230f45d8.6f7a52",
        "name": "Cancel mq9",
        "group": "29206391.d60f3c",
        "order": 10,
        "width": "2",
        "height": "1",
        "passthru": false,
        "label": "Cancelar",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "icon": "fa-times ",
        "payload": "{\"group\":{\"hide\":[\"Registros_export_mq9\"]}}",
        "payloadType": "json",
        "topic": "",
        "x": 1570,
        "y": 600,
        "wires": [
            [
                "b20893e3.5eb888"
            ]
        ]
    },
    {
        "id": "9c1128fe.cd83a",
        "type": "influxdb in",
        "z": "230f45d8.6f7a52",
        "influxdb": "be79df3d.f330d",
        "name": "",
        "query": "",
        "rawOutput": false,
        "precision": "s",
        "retentionPolicy": "",
        "x": 2430,
        "y": 220,
        "wires": [
            [
                "46f9e84.7996b18"
            ]
        ]
    },
    {
        "id": "d294b3a4.3c9a8",
        "type": "function",
        "z": "230f45d8.6f7a52",
        "name": "Envio de consulta con fechas",
        "func": "context.Fecha_inicial_temp = context.Fecha_inicial_temp || 0;\ncontext.Fecha_final_temp = context.Fecha_final_temp || 0;\n\nif (msg.topic === 'Fecha_inicial_temp') {\n  context.Fecha_inicial_temp = msg.payload;\n} else if (msg.topic === 'Fecha_final_temp') {\n  context.Fecha_final_temp = msg.payload;\n}\n\nif (context.Fecha_inicial_temp !== 0 && context.Fecha_final_temp !== 0){\n    //msg.query='SELECT * FROM \"invernadero\" WHERE \"time\" > ' + context.Fecha_inicial_temp + '000000000' + ' AND (time < ' + context.Fecha_final_temp + '000000000' + ')'\n    msg.query='SELECT \"name\",\"temp\" FROM invernadero WHERE time > ' + context.Fecha_inicial_temp + '000000' + ' AND (time < ' + context.Fecha_final_temp + '000000' + ')'\n    context.Fecha_inicial_temp = 0;\n    context.Fecha_final_temp = 0;\n    return msg;\n}\nelse {\n    return null\n}\n\n\n\n//var msg = {};\n//var a = context.get(fecha1);\n//context.set(a)=fecha1;\n//var b = context.get(fecha2);\n//msg.fecha1=\"1553100903000000000\";\n//msg.fecha2=\"1553101023000000000\";\n//msg.fecha1=a;\n//msg.fecha2=b;\n//.payload = msg;\n//return msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 2220,
        "y": 180,
        "wires": [
            [
                "9c1128fe.cd83a"
            ]
        ]
    },
    {
        "id": "2bcf0ab.41ddd76",
        "type": "function",
        "z": "230f45d8.6f7a52",
        "name": "Envio de consulta con fechas",
        "func": "context.Fecha_inicial_hum = context.Fecha_inicial_hum || 0;\ncontext.Fecha_final_hum = context.Fecha_final_hum || 0;\n\nif (msg.topic === 'Fecha_inicial_hum') {\n  context.Fecha_inicial_hum = msg.payload;\n} else if (msg.topic === 'Fecha_final_hum') {\n  context.Fecha_final_hum = msg.payload;\n}\n\nif (context.Fecha_inicial_hum !== 0 && context.Fecha_final_hum !== 0){\n    msg.query='SELECT \"name\",\"hum\" FROM invernadero WHERE time > ' + context.Fecha_inicial_hum + '000000' + ' AND (time < ' + context.Fecha_final_hum + '000000' + ')'\n    context.Fecha_inicial_hum = 0;\n    context.Fecha_final_hum = 0;\n    return msg;\n}\nelse {\n    return null\n}",
        "outputs": 1,
        "noerr": 0,
        "x": 2220,
        "y": 300,
        "wires": [
            [
                "e5364de.5baa13"
            ]
        ]
    },
    {
        "id": "f29a3e6c.d4a7d8",
        "type": "function",
        "z": "230f45d8.6f7a52",
        "name": "Envio de consulta con fechas",
        "func": "context.Fecha_inicial_ldr = context.Fecha_inicial_ldr || 0;\ncontext.Fecha_final_ldr = context.Fecha_final_ldr || 0;\n\nif (msg.topic === 'Fecha_inicial_ldr') {\n  context.Fecha_inicial_ldr = msg.payload;\n} else if (msg.topic === 'Fecha_final_ldr') {\n  context.Fecha_final_ldr = msg.payload;\n}\n\nif (context.Fecha_inicial_ldr !== 0 && context.Fecha_final_ldr !== 0){\n    msg.query='SELECT \"name\",\"ldr\" FROM invernadero WHERE time > ' + context.Fecha_inicial_ldr + '000000' + ' AND (time < ' + context.Fecha_final_ldr + '000000' + ')'\n    context.Fecha_inicial_ldr = 0;\n    context.Fecha_final_ldr = 0;\n    return msg;\n}\nelse {\n    return null\n}\n\n\n\n//var msg = {};\n//var a = context.get(fecha1);\n//context.set(a)=fecha1;\n//var b = context.get(fecha2);\n//msg.fecha1=\"1553100903000000000\";\n//msg.fecha2=\"1553101023000000000\";\n//msg.fecha1=a;\n//msg.fecha2=b;\n//.payload = msg;\n//return msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 2200,
        "y": 440,
        "wires": [
            [
                "a22e3988.585bf"
            ]
        ]
    },
    {
        "id": "d70a417d.669b08",
        "type": "function",
        "z": "230f45d8.6f7a52",
        "name": "Envio de consulta con fechas",
        "func": "context.Fecha_inicial_mq9 = context.Fecha_inicial_mq9 || 0;\ncontext.Fecha_final_mq9 = context.Fecha_final_mq9 || 0;\n\nif (msg.topic === 'Fecha_inicial_mq9') {\n  context.Fecha_inicial_mq9 = msg.payload;\n} else if (msg.topic === 'Fecha_final_mq9') {\n  context.Fecha_final_mq9 = msg.payload;\n}\n\nif (context.Fecha_inicial_mq9 !== 0 && context.Fecha_final_mq9 !== 0){\n    msg.query='SELECT \"name\",\"mq9\" FROM invernadero WHERE time > ' + context.Fecha_inicial_mq9 + '000000' + ' AND (time < ' + context.Fecha_final_mq9 + '000000' + ')'\n    context.Fecha_inicial_mq9 = 0;\n    context.Fecha_final_mq9 = 0;\n    return msg;\n}\nelse {\n    return null\n}\n\n\n\n//var msg = {};\n//var a = context.get(fecha1);\n//context.set(a)=fecha1;\n//var b = context.get(fecha2);\n//msg.fecha1=\"1553100903000000000\";\n//msg.fecha2=\"1553101023000000000\";\n//msg.fecha1=a;\n//msg.fecha2=b;\n//.payload = msg;\n//return msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 2200,
        "y": 580,
        "wires": [
            [
                "e249a631.816a4"
            ]
        ]
    },
    {
        "id": "e249a631.816a4",
        "type": "influxdb in",
        "z": "230f45d8.6f7a52",
        "influxdb": "be79df3d.f330d",
        "name": "",
        "query": "",
        "rawOutput": false,
        "precision": "s",
        "retentionPolicy": "",
        "x": 2430,
        "y": 620,
        "wires": [
            [
                "749caeef.5c8ac"
            ]
        ]
    },
    {
        "id": "a22e3988.585bf",
        "type": "influxdb in",
        "z": "230f45d8.6f7a52",
        "influxdb": "be79df3d.f330d",
        "name": "",
        "query": "",
        "rawOutput": false,
        "precision": "s",
        "retentionPolicy": "",
        "x": 2430,
        "y": 480,
        "wires": [
            [
                "fe3c9d68.93ec28"
            ]
        ]
    },
    {
        "id": "e5364de.5baa13",
        "type": "influxdb in",
        "z": "230f45d8.6f7a52",
        "influxdb": "be79df3d.f330d",
        "name": "",
        "query": "",
        "rawOutput": false,
        "precision": "s",
        "retentionPolicy": "",
        "x": 2430,
        "y": 340,
        "wires": [
            [
                "76067011.d0d0b8"
            ]
        ]
    },
    {
        "id": "76067011.d0d0b8",
        "type": "csv",
        "z": "230f45d8.6f7a52",
        "name": "",
        "sep": ",",
        "hdrin": false,
        "hdrout": true,
        "multi": "one",
        "ret": "\\n",
        "temp": "time,name,hum",
        "skip": "0",
        "x": 2550,
        "y": 300,
        "wires": [
            [
                "fb694316.9ace48"
            ]
        ]
    },
    {
        "id": "fe3c9d68.93ec28",
        "type": "csv",
        "z": "230f45d8.6f7a52",
        "name": "",
        "sep": ",",
        "hdrin": false,
        "hdrout": true,
        "multi": "one",
        "ret": "\\n",
        "temp": "time,name,ldr",
        "skip": "0",
        "x": 2550,
        "y": 440,
        "wires": [
            [
                "2cbb7a16.a15426"
            ]
        ]
    },
    {
        "id": "749caeef.5c8ac",
        "type": "csv",
        "z": "230f45d8.6f7a52",
        "name": "",
        "sep": ",",
        "hdrin": false,
        "hdrout": true,
        "multi": "one",
        "ret": "\\n",
        "temp": "time,name,mq9",
        "skip": "0",
        "x": 2550,
        "y": 580,
        "wires": [
            [
                "bf55b6bb.77b918"
            ]
        ]
    },
    {
        "id": "fb694316.9ace48",
        "type": "file",
        "z": "230f45d8.6f7a52",
        "name": "",
        "filename": "/home/pi/hum.csv",
        "appendNewline": true,
        "createDir": false,
        "overwriteFile": "true",
        "x": 2730,
        "y": 300,
        "wires": [
            [
                "75e5519e.732cd"
            ]
        ]
    },
    {
        "id": "2cbb7a16.a15426",
        "type": "file",
        "z": "230f45d8.6f7a52",
        "name": "",
        "filename": "/home/pi/ldr.csv",
        "appendNewline": true,
        "createDir": false,
        "overwriteFile": "true",
        "x": 2720,
        "y": 440,
        "wires": [
            [
                "d37e950a.c11f3"
            ]
        ]
    },
    {
        "id": "bf55b6bb.77b918",
        "type": "file",
        "z": "230f45d8.6f7a52",
        "name": "",
        "filename": "/home/pi/mq9.csv",
        "appendNewline": true,
        "createDir": false,
        "overwriteFile": "true",
        "x": 2730,
        "y": 580,
        "wires": [
            [
                "33659eb8.4a5502"
            ]
        ]
    },
    {
        "id": "f5698941.9b4fa8",
        "type": "ui_toast",
        "z": "230f45d8.6f7a52",
        "position": "top right",
        "displayTime": "3",
        "highlight": "",
        "outputs": 0,
        "ok": "OK",
        "cancel": "",
        "topic": "Datos exportados con exito",
        "name": "Tarea completada",
        "x": 2930,
        "y": 220,
        "wires": []
    },
    {
        "id": "1042c388.0e4e5c",
        "type": "function",
        "z": "230f45d8.6f7a52",
        "name": "",
        "func": "msg.payload=\"/home/pi/temp.csv\";\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 2910,
        "y": 180,
        "wires": [
            [
                "f5698941.9b4fa8"
            ]
        ]
    },
    {
        "id": "75e5519e.732cd",
        "type": "function",
        "z": "230f45d8.6f7a52",
        "name": "",
        "func": "msg.payload=\"/home/pi/hum.csv\";\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 2910,
        "y": 300,
        "wires": [
            [
                "b6aabdb6.42b2c"
            ]
        ]
    },
    {
        "id": "b6aabdb6.42b2c",
        "type": "ui_toast",
        "z": "230f45d8.6f7a52",
        "position": "top right",
        "displayTime": "3",
        "highlight": "",
        "outputs": 0,
        "ok": "OK",
        "cancel": "",
        "topic": "Datos exportados con exito",
        "name": "Tarea completada",
        "x": 2930,
        "y": 340,
        "wires": []
    },
    {
        "id": "d37e950a.c11f3",
        "type": "function",
        "z": "230f45d8.6f7a52",
        "name": "",
        "func": "msg.payload=\"/home/pi/ldr.csv\";\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 2910,
        "y": 440,
        "wires": [
            [
                "845ef2b8.6d6da8"
            ]
        ]
    },
    {
        "id": "845ef2b8.6d6da8",
        "type": "ui_toast",
        "z": "230f45d8.6f7a52",
        "position": "top right",
        "displayTime": "3",
        "highlight": "",
        "outputs": 0,
        "ok": "OK",
        "cancel": "",
        "topic": "Datos exportados con exito",
        "name": "Tarea completada",
        "x": 2930,
        "y": 480,
        "wires": []
    },
    {
        "id": "33659eb8.4a5502",
        "type": "function",
        "z": "230f45d8.6f7a52",
        "name": "",
        "func": "msg.payload=\"/home/pi/mq9.csv\";\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 2910,
        "y": 580,
        "wires": [
            [
                "f9a08d12.bf3048"
            ]
        ]
    },
    {
        "id": "f9a08d12.bf3048",
        "type": "ui_toast",
        "z": "230f45d8.6f7a52",
        "position": "top right",
        "displayTime": "3",
        "highlight": "",
        "outputs": 0,
        "ok": "OK",
        "cancel": "",
        "topic": "Datos exportados con exito",
        "name": "Tarea completada",
        "x": 2930,
        "y": 620,
        "wires": []
    },
    {
        "id": "e2df35d7.5139a8",
        "type": "template",
        "z": "230f45d8.6f7a52",
        "name": "Historico Temperatura",
        "field": "template",
        "fieldType": "msg",
        "format": "handlebars",
        "syntax": "mustache",
        "template": "<iframe src=\"http://192.168.20.131:3000/d-solo/CelyNFmgz/edicion_escenario1-edukit_2?orgId=1&{{payload}}&panelId=2\" width=\"1100\" height=\"300\" frameborder=\"0\"></iframe>",
        "output": "str",
        "x": 2400,
        "y": 780,
        "wires": [
            [
                "c6890547.33cfe"
            ]
        ]
    },
    {
        "id": "bb344085.ea39d",
        "type": "ui_button",
        "z": "230f45d8.6f7a52",
        "name": "confirmar",
        "group": "d04e1b2.f8adae8",
        "order": 4,
        "width": "3",
        "height": "1",
        "passthru": false,
        "label": "Confirmar",
        "tooltip": "Confirma los intervalos definidos. Presionar una vez que este seguro de los datos ingresados. ",
        "color": "",
        "bgcolor": "",
        "icon": "fa-check-circle ",
        "payload": "confirmar",
        "payloadType": "str",
        "topic": "confirmar",
        "x": 1560,
        "y": 980,
        "wires": [
            [
                "322bac1e.1b0f44",
                "d454004f.29974",
                "ffd57fe9.3f8d8"
            ]
        ]
    },
    {
        "id": "7f90fbfd.c86404",
        "type": "ui_form",
        "z": "230f45d8.6f7a52",
        "name": "f1",
        "label": "Momento inicial",
        "group": "dc137fb4.d8e79",
        "order": 1,
        "width": "6",
        "height": "1",
        "options": [
            {
                "label": "",
                "value": "f1",
                "type": "date",
                "required": true
            },
            {
                "label": "Hora del dia (entre 0 y 24))",
                "value": "h1",
                "type": "number",
                "required": true
            }
        ],
        "formValue": {
            "f1": "",
            "h1": ""
        },
        "payload": "",
        "submit": "Cargar",
        "cancel": "Cancelar",
        "topic": "f1",
        "x": 1570,
        "y": 800,
        "wires": [
            [
                "322bac1e.1b0f44",
                "d454004f.29974"
            ]
        ]
    },
    {
        "id": "8619e095.f071f",
        "type": "ui_form",
        "z": "230f45d8.6f7a52",
        "name": "f2",
        "label": "Momento final",
        "group": "41fbf2c8.27dd2c",
        "order": 1,
        "width": "6",
        "height": "1",
        "options": [
            {
                "label": "",
                "value": "f2",
                "type": "date",
                "required": true
            },
            {
                "label": "Hora del dia (entre 0 y 24))",
                "value": "h2",
                "type": "number",
                "required": true
            }
        ],
        "formValue": {
            "f2": "",
            "h2": ""
        },
        "payload": "",
        "submit": "Cargar",
        "cancel": "Cancelar",
        "topic": "f2",
        "x": 1570,
        "y": 860,
        "wires": [
            [
                "322bac1e.1b0f44",
                "d454004f.29974"
            ]
        ]
    },
    {
        "id": "ffd57fe9.3f8d8",
        "type": "ui_dropdown",
        "z": "230f45d8.6f7a52",
        "name": "",
        "label": "Refrescar cada ",
        "tooltip": "Seleccionar un intervalo de tiempo para refrescar los graficos y obtener información en tiempo real",
        "place": "Seleccione intervalo",
        "group": "d04e1b2.f8adae8",
        "order": 1,
        "width": "6",
        "height": "1",
        "passthru": false,
        "options": [
            {
                "label": "Off",
                "value": "Off",
                "type": "str"
            },
            {
                "label": "5s",
                "value": "5s",
                "type": "str"
            },
            {
                "label": "10s",
                "value": "10s",
                "type": "str"
            },
            {
                "label": "30s",
                "value": "30s",
                "type": "str"
            },
            {
                "label": "1m",
                "value": "1m",
                "type": "str"
            },
            {
                "label": "5m",
                "value": "5m",
                "type": "str"
            },
            {
                "label": "15m",
                "value": "15m",
                "type": "str"
            },
            {
                "label": "30m",
                "value": "30m",
                "type": "str"
            },
            {
                "label": "1h",
                "value": "1h",
                "type": "str"
            },
            {
                "label": "2h",
                "value": "2h",
                "type": "str"
            },
            {
                "label": "1d",
                "value": "1d",
                "type": "str"
            }
        ],
        "payload": "",
        "topic": "refresh",
        "x": 1540,
        "y": 920,
        "wires": [
            [
                "322bac1e.1b0f44",
                "d454004f.29974"
            ]
        ]
    },
    {
        "id": "322bac1e.1b0f44",
        "type": "function",
        "z": "230f45d8.6f7a52",
        "name": "refresh, from y to - Datos Historicos",
        "func": "context.f1 = context.f1 || 0;\ncontext.f2 = context.f2 || 0;\ncontext.h1 = context.h1 || -1;\ncontext.h2 = context.h2 || -1;\ncontext.ref = context.ref || 'Off';\ncontext.con = context.con || 'Nada';\n\nif (msg.topic === 'f1') {\n  context.f1 = msg.payload.f1;\n  context.h1 = msg.payload.h1;\n} else if (msg.topic === 'f2') {\n  context.f2 = msg.payload.f2;\n  context.h2 = msg.payload.h2;\n} else if (msg.topic === 'refresh') {\n  context.ref = msg.payload;\n} else if (msg.topic === 'confirmar') {\n  context.con = msg.payload;\n} else if (msg.topic === 'reset') {\n  context.reset = msg.payload;\n}\n\nif (context.f1 !== 0 && context.f2 !== 0 && context.ref !== 'Off' && context.con == 'confirmar'){\n    var fecha1 = new Date(context.f1); // Your timezone!\n    var epoch1 = fecha1.getTime();\n    \n    var fecha2 = new Date(context.f2); // Your timezone!\n    var epoch2 = fecha2.getTime();\n    \n    var hora1 = context.h1;\n    var hora2 = context.h2;\n    epoch1 = epoch1 + hora1*3600*1000; //3600 segundos en una hora, 1000 miliseg por cada seg\n    epoch2 = epoch2 + hora2*3600*1000;\n    \n    msg.payload='refresh=' + context.ref + '&from='+ epoch1.toString() + '&to='+ epoch2.toString();\n\n    context.f1 = 0;\n    context.f2 = 0;\n    context.h1 = -1;\n    context.h2 = -1;\n    context.ref = 'Off';\n    context.con = 'NADA';\n    \n    return msg;\n}\nelse {\n    return null\n}",
        "outputs": 1,
        "noerr": 0,
        "x": 1980,
        "y": 840,
        "wires": [
            [
                "e2df35d7.5139a8",
                "f498edee.ab468",
                "39dea26a.fb10d6",
                "4f824516.3c5ddc"
            ]
        ]
    },
    {
        "id": "f498edee.ab468",
        "type": "template",
        "z": "230f45d8.6f7a52",
        "name": "Historico Humedad",
        "field": "template",
        "fieldType": "msg",
        "format": "handlebars",
        "syntax": "mustache",
        "template": "<iframe src=\"http://192.168.20.131:3000/d-solo/CelyNFmgz/edicion_escenario1-edukit_2?orgId=1&{{payload}}&panelId=16\" width=\"1100\" height=\"300\" frameborder=\"0\"></iframe>",
        "output": "str",
        "x": 2390,
        "y": 860,
        "wires": [
            [
                "e8985261.d749e"
            ]
        ]
    },
    {
        "id": "39dea26a.fb10d6",
        "type": "template",
        "z": "230f45d8.6f7a52",
        "name": "Historico LDR",
        "field": "template",
        "fieldType": "msg",
        "format": "handlebars",
        "syntax": "mustache",
        "template": "<iframe src=\"http://192.168.20.131:3000/d-solo/CelyNFmgz/edicion_escenario1-edukit_2?orgId=1&{{payload}}&panelId=17\" width=\"1100\" height=\"300\" frameborder=\"0\"></iframe>",
        "output": "str",
        "x": 2380,
        "y": 940,
        "wires": [
            [
                "4fb3d53c.4351b4"
            ]
        ]
    },
    {
        "id": "4f824516.3c5ddc",
        "type": "template",
        "z": "230f45d8.6f7a52",
        "name": "Historico mq9",
        "field": "template",
        "fieldType": "msg",
        "format": "handlebars",
        "syntax": "mustache",
        "template": "<iframe src=\"http://192.168.20.131:3000/d-solo/CelyNFmgz/edicion_escenario1-edukit_2?orgId=1&{{payload}}&panelId=15\" width=\"1100\" height=\"300\" frameborder=\"0\"></iframe>",
        "output": "str",
        "x": 2380,
        "y": 1020,
        "wires": [
            [
                "c1909fc1.25fe1"
            ]
        ]
    },
    {
        "id": "b44ffe1f.41f14",
        "type": "comment",
        "z": "230f45d8.6f7a52",
        "name": "Datos historicos -  Graficos e intervalo de muestra",
        "info": "",
        "x": 2080,
        "y": 720,
        "wires": []
    },
    {
        "id": "4d4268a3.1ad878",
        "type": "ui_text",
        "z": "230f45d8.6f7a52",
        "group": "d04e1b2.f8adae8",
        "order": 3,
        "width": "6",
        "height": "2",
        "name": "",
        "label": "Tiempo seteado: ",
        "format": "{{msg.payload}}",
        "layout": "row-left",
        "x": 2130,
        "y": 1020,
        "wires": []
    },
    {
        "id": "d454004f.29974",
        "type": "function",
        "z": "230f45d8.6f7a52",
        "name": "Mostrar actual",
        "func": "context.f1 = context.f1 || 0;\ncontext.f2 = context.f2 || 0;\ncontext.h1 = context.h1 || -1;\ncontext.h2 = context.h2 || -1;\ncontext.ref = context.ref || 'Off';\ncontext.con = context.con || 'Nada';\n\nif (msg.topic === 'f1') {\n  context.f1 = msg.payload.f1;\n  context.h1 = msg.payload.h1;\n} else if (msg.topic === 'f2') {\n  context.f2 = msg.payload.f2;\n  context.h2 = msg.payload.h2;\n} else if (msg.topic === 'refresh') {\n  context.ref = msg.payload;\n} else if (msg.topic === 'confirmar') {\n  context.con = msg.payload;\n}\n\nif (context.f1 !== 0 && context.f2 !== 0 && context.ref !== 'Off' && context.con == 'confirmar'){\n    var fecha1 = new Date(context.f1); // Your timezone!\n    var epoch1 = fecha1.getTime();\n    \n    var fecha2 = new Date(context.f2); // Your timezone!\n    var epoch2 = fecha2.getTime();\n    \n    var hora1 = context.h1;\n    var hora2 = context.h2;\n    epoch1 = epoch1 + hora1*3600*1000; //3600 segundos en una hora, 1000 miliseg por cada seg\n    epoch2 = epoch2 + hora2*3600*1000;\n    \n    epoch1_show = new Date(epoch1).toLocaleString();\n    epoch2_show = new Date(epoch2).toLocaleString();\n    \n    epoch1_show = epoch1_show.replace(/-/g,\":\");\n    epoch1_show = epoch1_show.replace(\" \",\":\");\n    var partes1 = epoch1_show.split(\":\");\n    \n    epoch2_show = epoch2_show.replace(/-/g,\":\");\n    epoch2_show = epoch2_show.replace(\" \",\":\");\n    var partes2 = epoch2_show.split(\":\");\n\n    var dia1 = partes1[2];\n    var mes1 = partes1[1];\n    var anio1 = partes1[0];\n    var h1 = partes1[3];\n    var m1 = partes1[4];\n    var s1 = partes1[5];\n    \n    var dia2 = partes2[2];\n    var mes2 = partes2[1];\n    var anio2 = partes2[0];\n    var h2 = partes2[3];\n    var m2 = partes2[4];\n    var s2 = partes2[5];\n    \n    msg.payload = 'Inicio:' + dia1 + '/' + mes1 + '/' + anio1 + '-' + h1 + 'hs' + '<br>Final: ' + \" \" + dia2 + '/' + mes2 + '/' + anio2 + '-' + h2 + 'hs';\n\n    context.f1 = 0;\n    context.f2 = 0;\n    context.h1 = -1;\n    context.h2 = -1;\n    context.ref = 'Off';\n    context.con = 'NADA';\n    \n    return msg;\n}\nelse {\n    return null\n}",
        "outputs": 1,
        "noerr": 0,
        "x": 1920,
        "y": 920,
        "wires": [
            [
                "4d4268a3.1ad878"
            ]
        ]
    },
    {
        "id": "be79df3d.f330d",
        "type": "influxdb",
        "z": "",
        "hostname": "localhost",
        "port": "8086",
        "protocol": "http",
        "database": "invernadero-IT10",
        "name": "",
        "usetls": false,
        "tls": ""
    },
    {
        "id": "16e1aa9a.dc41f5",
        "type": "mqtt-broker",
        "z": "",
        "name": "Broker local",
        "broker": "localhost",
        "port": "1883",
        "clientid": "",
        "usetls": false,
        "compatmode": true,
        "keepalive": "60",
        "cleansession": true,
        "birthTopic": "",
        "birthQos": "0",
        "birthPayload": "",
        "closeTopic": "",
        "closeQos": "0",
        "closePayload": "",
        "willTopic": "",
        "willQos": "0",
        "willPayload": ""
    },
    {
        "id": "124a1fa3.a5e4c8",
        "type": "ui_group",
        "z": "",
        "name": "Datos Instantaneos - mq9/LDR",
        "tab": "efb1f443.02175",
        "order": 3,
        "disp": false,
        "width": "10",
        "collapse": false
    },
    {
        "id": "b8c3e6e8.f28eb",
        "type": "ui_group",
        "z": "",
        "name": "Hora",
        "tab": "efb1f443.02175",
        "order": 1,
        "disp": false,
        "width": "20",
        "collapse": false
    },
    {
        "id": "d579fb4d.09324",
        "type": "ui_group",
        "z": "",
        "name": "Datos Instantaneos - Temp/humedad",
        "tab": "efb1f443.02175",
        "order": 2,
        "disp": false,
        "width": "10",
        "collapse": false
    },
    {
        "id": "5d22a336.67eb5c",
        "type": "ui_group",
        "z": "",
        "name": "Datos historicos de concentración de CO",
        "tab": "c06a1a1e.69ede",
        "order": 10,
        "disp": true,
        "width": "20",
        "collapse": true
    },
    {
        "id": "a7f71eb3.656578",
        "type": "ui_group",
        "z": "",
        "name": "Datos historicos de temperatura ambiente",
        "tab": "c06a1a1e.69ede",
        "order": 4,
        "disp": true,
        "width": "20",
        "collapse": true
    },
    {
        "id": "8f212678.706158",
        "type": "ui_group",
        "z": "",
        "name": "Datos historicos de humedad",
        "tab": "c06a1a1e.69ede",
        "order": 6,
        "disp": true,
        "width": "20",
        "collapse": true
    },
    {
        "id": "afecb5f7.f3fbc8",
        "type": "ui_group",
        "z": "",
        "name": "Datos historicos de niveles de luz",
        "tab": "c06a1a1e.69ede",
        "order": 8,
        "disp": true,
        "width": "20",
        "collapse": true
    },
    {
        "id": "8ed24b80.43df2",
        "type": "ui_group",
        "z": "",
        "name": "export temp",
        "tab": "c06a1a1e.69ede",
        "order": 5,
        "disp": false,
        "width": "20",
        "collapse": false
    },
    {
        "id": "852840d5.64a668",
        "type": "ui_group",
        "z": "",
        "name": "export hum",
        "tab": "c06a1a1e.69ede",
        "order": 7,
        "disp": false,
        "width": "20",
        "collapse": false
    },
    {
        "id": "224c07c1.06a2d",
        "type": "ui_group",
        "z": "",
        "name": "export ldr",
        "tab": "c06a1a1e.69ede",
        "order": 9,
        "disp": false,
        "width": "20",
        "collapse": false
    },
    {
        "id": "29206391.d60f3c",
        "type": "ui_group",
        "z": "",
        "name": "export mq9",
        "tab": "c06a1a1e.69ede",
        "order": 11,
        "disp": false,
        "width": "20",
        "collapse": false
    },
    {
        "id": "d04e1b2.f8adae8",
        "type": "ui_group",
        "z": "",
        "name": "Tiempo de refresco de gráficos",
        "tab": "c06a1a1e.69ede",
        "order": 3,
        "disp": false,
        "width": "6",
        "collapse": false
    },
    {
        "id": "dc137fb4.d8e79",
        "type": "ui_group",
        "z": "",
        "name": "Mom inicial",
        "tab": "c06a1a1e.69ede",
        "order": 1,
        "disp": false,
        "width": "7",
        "collapse": false
    },
    {
        "id": "41fbf2c8.27dd2c",
        "type": "ui_group",
        "z": "",
        "name": "Mom final",
        "tab": "c06a1a1e.69ede",
        "order": 2,
        "disp": false,
        "width": "7",
        "collapse": false
    },
    {
        "id": "efb1f443.02175",
        "type": "ui_tab",
        "z": "",
        "name": "Invernadero",
        "icon": "fa-leaf",
        "order": 1,
        "disabled": false,
        "hidden": false
    },
    {
        "id": "c06a1a1e.69ede",
        "type": "ui_tab",
        "z": "",
        "name": "Registros",
        "icon": "fa-area-chart",
        "order": 2,
        "disabled": false,
        "hidden": false
    }
]
```

Es la información de todos los nodos creados con la configuración necesaria para nuestros propósitos. Si seccionamos un poco todo los nodos:

- **Nodos principales:** Son los que se encargan de tomar los datos que son publicados al broker MQTT, que también es creado aquí, parsear la información, y darle un formato correcto para insertarse en la base de datos.
![nodos principales.png](Imagenes_anexos/Definitivo/Node-RED/nodos_principales.png)

- **Nodos para visualización web:** Gracias a los nodos **Dashboard** se puede crear una web rápidamente, que usaremos para visualizar en tiempo real lo que esta pasando en nuestro invernadero. Podemos verlo en 3 partes:
	
	- **Nodos con datos instantáneos:** Nos muestra lo que sucede actualmente en el invernadero, con el último dato enviado de temperatura, humedad, luz y concentración de CO.
	
  ![nodos vista rapida.png](Imagenes_anexos/Definitivo/Node-RED/nodos_vista_rapida.png)
	
	- **Nodos de datos historicos:** Nos permite visualizar datos tomados hisoticamente por los sensores, seteando el intervalo de tiempo que se quiere ver.![nodos datos historicos.png](Imagenes_anexos/Definitivo/Node-RED/nodos_datos_historicos.png)
	
	- **Nodos para exportar datos historicos:** Los utilizamos para poder extraer los datos en un archivo csv sobre cada sensor en particular.
	![nodos exportar.png](Imagenes_anexos/Definitivo/Node-RED/nodos_exportar.png)

Es necesario modificar la dirección IP en todos los nodos que hagan referencia a gráficos. Al momento de realizar el proyecto cuenta con la dirección IP `192.168.20.131`, que es donde esta alojado Node-RED, Grafana, InfluxDB y el broker local. Se aconseja cambiar a la dirección que corresponda, sin modificar los puertos, para poder ejecutarlo correctamente. Para hacer esto hay que ingresar a cada nodo y verificar si hay direcciones IP que sirvan para configuración o ejecución del flujo de Node-RED. Están asociados a las aplicaciones antes mencionadas. También así si se utilizan usuarios y contraseñas para securizar los procesos.

Una vez realizado lo dicho anteriormente, se puede ir al botón **Deploy**, sobre la esquina superior derecha. Se puede observar que no se están enviando datos a la base de datos porque la misma no esta creada todavía, y es el paso siguiente.

## InfluxDB

![Influxdb_logo.svg22222](Imagenes_anexos/influxdb/Influxdb_logo.svg22222.png)

InfluxDB, desarrollada por [influxdata](https://www.influxdata.com), es una base de datos basada en series de tiempo (*time-series database*). Maneja de forma eficiente estas series de datos, con miles de inserciones por segundo, y de los que necesitamos hacer cálculos, búsquedas, y agregar información, como medias, máximos, etc, y todo ello en tiempo real. Dentro de esta categoría, InfluxDB es una base de datos que supera a los esquemas SQL y NoSQL. Tiene una versión comunitaria de código abierto. 

Se instala con el comando: `sudo apt-get install influxdb`. Las nuevas versiones están listas para entrar como administrador.

Para poder acceder, utilizar y modificar la base de datos se requiere un cliente. Para esto lo instalamos con `sudo apt-get install influxdb-client`.

Para iniciar la base de datos no es necesario hacerlo a través del servicio, es decir `sudo service influxdb start`. Solo hace falta iniciarlo así:

```Bash
sudo influxd run --config /etc/influxdb/influxdb.conf &
```

Lo corremos en segundo plano para poder utilizar el cliente en la misma terminal, sobre todo si pretendemos usar una sola consola con ssh.

También podríamos hacer las consultas o lo que sea necesario a través de un cliente web.

### Configuraciones previas

Para acceder al cliente por el terminal, solo ingresamos `influx` en la misma. Puede que sea necesario aclarar en algún caso al host al que se conecta, esto se hace: `influx -host IP`. Esa IP es la local, o en todo caso la del equipo que maneje la base de datos.

Primero debemos crear un usuario como el de Raspbian (ya que se desarrollo todo sobre una Raspberry Pi) para la base de datos:
```Bash
CREATE USER "pi" WITH PASSWORD 'it10' WITH ALL PRIVILEGES
```

Si se usa otra plataforma o una PC, cambiar **pi** e **it10** por su respectivo usuario y contraseña.

### Crear Base de datos
Con InfluxDB ya instalado solo hace falta crear la base de datos, ya que Node-RED envía los datos, pero si no hay una base de datos donde insertarse, estos nunca se envían y dará un error en la interfaz de Node-RED. Se procede de la siguiente manera:

1. Abrir el cliente de influxDB en la terminal de Raspbian:

	```Bash
	influx
	```

2. Creamos la base de datos `invernadero-IT10`:
	```Bash
	CREATE DATABASE invernadero-IT10
	```
	
	Dependiendo las versiones puede ser que algunos caracteres, como el guion medio ("-"), no se permita utilizar. En todo caso se puede sustituir por un guion bajo. En este caso es posible con guion medio y asi se procede en todo el proyecto. De lo contrario el nombre que se ingrese hay que insertarlo que cada parte que sea necesario, como los nodos de Node-RED donde se haga referencia a la base de datos, o también Grafana.
	
3. Mostramos para ver que se creo correctamente:

  ```Bash
  show databases
  ```
  ![influx databases2.png](Imagenes_anexos/Definitivo/InfluxDB/influx_databases2.png)

4. Una vez hecho lo anterior, Hacemos **Deploy** nuevamente en Node-RED, para que cree el *Measurement* **invernadero**, con los respectivos *Field Keys* comenzar a recibir los primeros valores. Luego lo observaremos con el cliente influx.
  ```Bash
  use invernadero-IT10
  show measurements
  ```
  ![influx measurements2.png](Imagenes_anexos/Definitivo/InfluxDB/influx_measurements2.png)

  ```Bash
  show FIELD KEYS
  ```
  ![influx field keys2.png](Imagenes_anexos/Definitivo/InfluxDB/influx_field_keys2.png)

  ```Bash
  SELECT * FROM invernadero LIMIT 10
  ```

![influx valores.png](Imagenes_anexos/Definitivo/InfluxDB/influx_valores.png)

## Grafana

![Logo Grafana](http://www.freelogovectors.net/wp-content/uploads/2018/07/grafana-logo.png)

Grafana es una herramienta de código abierto para el análisis y visualización de métricas. Está escrita en Lenguaje Go (creado por Google) y Node.js LTS. Además cuenta con una fuerte Interfaz de Programación de Aplicaciones (API); es una aplicación que ha venido escalando posiciones, con una comunidad entusiasta de más de 600 colaboradores bien integrados. Su código fuente está publicado en GitHub.

A partir de una serie de datos recolectados se puede obtener un panorama gráfico presentado en una forma elegante y amigable. Grafana permite consultar, visualizar, alertar y comprender métricas que pueden ser recolectadas y/o procesadas por aplicaciones de terceros. Puede recopilar de forma nativa datos de *Cloudwatch, Graphite, Elasticsearch, OpenTSDB, Prometheus, Hosted Metrics e InfluxDB*.

Entre sus características posee:

- 37 complementos de fuentes de datos.
- 28 complementos para el panel.
- 15 complementos de aplicaciones.
- Más de 600 paneles de control creados para aplicaciones populares.

### Instalación

Para la Raspberry Pi 3 B+ se puede instalar por terminal. Primero se descarga:

```Bash
wget https://dl.grafana.com/oss/release/grafana_5.4.3_armhf.deb
sudo dpkg -i grafana_5.4.3_armhf.deb
```
Y cuando termina la descarga se descomprime e instala:
```Bash
sudo dpkg -i grafana_5.4.3_armhf.deb
```

Luego se indica los siguiente para instalar Grafana:
```Bash
	### NOT starting on installation, please execute the following statements to configure grafana to start automatically using systemd
 sudo /bin/systemctl daemon-reload
 sudo /bin/systemctl enable grafana-server
	### You can start grafana-server by executing
 sudo /bin/systemctl start grafana-server
```
Por ultimo habilitamos el servicio y activamos el demonio para que inicie la herramienta.

```Bash
	sudo /bin/systemctl daemon-reload
	sudo /bin/systemctl enable grafana-server
	sudo /bin/systemctl restart grafana-server
	sudo service grafana-server start
	sudo systemctl enable grafana-server.service
	sudo /lib/systemd/systemd-sysv-install enable grafana-server
```
Ahora sabiendo la dirección IP del equipo, ingresando en un navegador web con `http://<IP>:3000/` se puede acceder a Grafana y desarrollar un dashboard.

### Ingresando por primera vez

Para entrar por primera vez, una vez iniciado el servicio se solicita loguearse. Por defecto el usuario es `admin` y su contraseña también. Posteriormente se pide cambiar la contraseña para nuevos ingresos.

### Agregando un datasource
El datasource es la fuente de datos que se usara para generar las graficas. Grafana toma los datos insertados en la base de datos temporal y los pone a disposición en una grafica de nuestro gusto.

1. Ingresar a la aplicación web con nuestra IP local o a la que estamos accediendo en la misma red, en este caso `http://192.168.20.129:3000` e ingresar con el usuario:contraseña correspondiente.

2. Asignar una fuente de datos. Ir a configuración, sobre el panel izquierdo y seleccionar `Data sources`. Luego presionar sobre `Add data source`. 

   ![agregar nueva fuente de datos.png](Imagenes_anexos/grafana/agregar_nueva_fuente_de_datos.png) En la imagen aparece ya agregada, pero el procedimiento es el mismo para agregar cualquier *data source*.

3. Configurar la fuente de datos. Aquí se pone la información necesaria para acceder a la base de datos InfluxDB. 

  ![datasources1.png](Imagenes_anexos/Definitivo/Grafana/datasources1.png)
  ![datasources2.png](Imagenes_anexos/Definitivo/Grafana/datasources2.png)
  Se le pone un nombre (en este caso *Escenario1_2.0*). En las opciones HTTP se pone la dirección IP local o de la maquina a la que se accede en la red, con el puerto 8086, que es el puerto por defecto en el que la base de datos escucha las peticiones HTTP. Dejamos la opción `Access: Server(Default)`. En cuanto a la autenticación, solo ponemos la básica, que son nuestro *usuario:contraseña* de Raspbian. Por ultimo en los detalles de InfluxDB, se indica la base de datos a la que se quiere acceder. En este caso a `invernadero-IT10`, con el *usuario:contraseña* correspondiente. Esta ultima parte de autenticación no es necesaria ya que se accede de manera local con el mismo usuario pi. Se guarda y se procede a crear un dashboard.

### Creando el dashboard

Aquí se decide que tipo de panel se va a utilizar. 

![nuevo dashboard.png](Imagenes_anexos/grafana/nuevo_dashboard.png) 

![panel nuevo.png](Imagenes_anexos/grafana/panel_nuevo.png) 

Sobre el `Panel Title` se hace click, luego `Edit`. 

![metrics.png](Imagenes_anexos/grafana/metrics.png) 

En la pestaña metrics esta la parte esencial para mostrar los datos. Es donde se encuentra la petición a la base de datos. Por el momento solo realizaremos una sola para este panel.

- En la línea `FROM`: *Data source* *Measurement* `WHERE` *Field* `=` *valor*. Esta línea debemos indicar la fuente como **default** o **invernadero-IT10**, **invernadero** como *Measurement*. `WHERE` nos sirve para identificar datos particulares dentro de la misma base de datos, como por ejemplo todo lo referido a un sensor, entonces se indica **name** como *Field* y **edukit** valor de ese field. En el caso que tuviéramos mas sensores de esta forma se puede discriminar uno de otro.
- En la línea `SELECT`: *field()* *mean()*. En field, indicamos que para **temp** realizaremos la grafica con sus datos, y realizando la media de los datos aportados en el intervalo de tiempo que se ha consultado.![edit temp.png](Imagenes_anexos/Definitivo/Grafana/edit_temp.png)
- Se puede agregar o modificar la característica de como se procesan los datos, no es solamente **mean()**, Para el caso es el mas conveniente para visualizar.
- En la línea `GROUP BY`: `time($_interval)` `fill(null)`. Aquí se indica el tiempo de acceso a los datos y como mostrarlos. Dejamos por defecto `time` y en **fill** podemos dejarlo como **null**, que se visualiza en forma de puntos, o con **none**, en forma de línea continua. Dejaremos  **none** Es importante dejar por defecto `time($_interval)`, ya que esto nos permite cambiar el rango de fechas a visualizar de forma dinámica desde el dashboard, muy útil para nuestros propósitos.
- En la línea `FORMAT AS`: `Time Series` lo dejamos por defecto.

Para el caso de mostrar valores instantáneos la configuración es muy similar, solo cambiaríamos el campo `SELECT`: *field()* *mean()* por *last()*, que sería el último valor de la base de datos.

Si queremos un panel que contenga una imagen será necesario descargar uno nuevo. Los que vienen por defecto no tienen esa opción. Descargaremos el panel **epict Panel**, que tiene sus instrucciones en https://grafana.com/plugins/larona-epict-panel/installation, pero es sencillamente ingresar por la terminal:

```Bash
grafana-cli plugins install larona-epict-panel
```
Luego solo resta reiniciar el servicio de Grafana.

```Bash
sudo service grafana-server restart
```
La configuración es muy similar a los demás en cuanto las métricas. Para insertar una imagen, se debe ir a la pestaña **Options**, y en **Backgroud URL** copiar la URL de la imagen deseada.
![epict panel luz.png](Imagenes_anexos/Definitivo/Grafana/epict_panel_luz.png)


Para ver mas cuestiones títulos, leyendas, formas de grafico, etc, están las demás pestañas. El resultado de toda esta configuración es la siguiente:

![instantaneos.png](Imagenes_anexos/Definitivo/Grafana/instantaneos.png)

![temp y hum.png](Imagenes_anexos/Definitivo/Grafana/temp_y_hum.png)

![co y ldr.png](Imagenes_anexos/Definitivo/Grafana/co_y_ldr.png)

Recordar guardar el dashboard cuando se finalicen los cambios, para luego poder exportarlos.

### Embebiendo gráficos
Hasta acá solo podemos ver los gráficos ingresando a Grafana, pero la idea es poder tenerlos disponibles en nuestra web que desarrollamos con Node-RED. Para eso debemos ir uno por uno de los gráficos que queremos embeber, presionar en su título y luego en **Share**.
![embed1.png](Imagenes_anexos/Definitivo/Grafana/embed1.png)
Después ir hasta la pestaña **Embed** y copiar el código HTML. Tener estos códigos presentes, que mas adelantes los usaremos en Node-RED.

![embed2.png](Imagenes_anexos/Definitivo/Grafana/embed2.png)

### Permisos para usuarios
Nosotros como administradores ya tenemos todos los permisos posibles. Pero para poder embeber gráficos es necesario que ese "usuario que va a consultar el gráfico" que es Node-RED tenga al menos permisos para ver.

En el archivo de configuración de Grafana, que se ubica en `/etc/grafana/grafana.ini` nos dirigimos a la sección **Users** y **Anonymous Auth**. Hay que descomentar las siguientes líneas (borrar el punto y coma), salvo para el caso `org_name` que pondremos true, y en `org_role` que pondremos el nombre del Team que crearemos posteriormente:

```Bash
	# Set to true to automatically assign new users to the default organization (id$
auto_assign_org = true

	# Default role new users will be automatically assigned (if disabled above is s$
auto_assign_org_role = Viewer

[auth.anonymous]
	# enable anonymous access
enabled = true

	# specify organization name that should be used for unauthenticated users
org_name = IT10

	# specify role for unauthenticated users
org_role = Viewer

```
Luego, ya en la interfaz web, vamos a **Configuration**, **Users** y a la pestaña **Teams**.
![configuracion users.png](Imagenes_anexos/Definitivo/Grafana/configuracion_users.png)

Crearemos un nuevo **Team**, que es donde por defecto entraran los nuevos usuarios que quieran ver los gráficos. 

![creando team.png](Imagenes_anexos/Definitivo/Grafana/creando_team.png)

Ponemos un nombre y un mail al que hagan referencia (puede ser cualquiera). Y listo, Team creado.

![team creado.png](Imagenes_anexos/Definitivo/Grafana/team_creado.png)

Por último, debemos darle efectivamente permisos para ver a estos usuarios. Hay que dirigirse a las opciones del dashboard que creamos en un principio, a la sección **Permissions** y otorgar permisos de **Viewer**. Guardar para finalizar.

![add permissions.png](Imagenes_anexos/Definitivo/Grafana/add_permissions.png)

Y con esto ya Node-RED puede mostrar los gráficos que embebimos en sus nodos. Es necesario hacer un *restart* del servicio de `grafana-server` para que se carguen las modificaciones.

## Integración de plataforma y herramientas

Ya tenemos listas todas las plataformas configuradas, y si bien Node-RED es el eslabón más importante que vincula todo, la configuración de algunos nodos son particulares de cada desarrollo. Ahora si podemos meternos dentro de los nodos para entender que sucede en cada parte. Haremos un recorrido desde que llega el dato al broker MQTT.

1. Nos suscribimos al broker MOSCA para escuchar los datos que ingresen al topic **testpps/**.
2. Se parsean los datos , ya que el formato es poco amigable:
``` Bash
edukit,mq9= 0.52,hum= 2.00,ldr=828.00,temp=34.21
```
La función que parsea esto es la siguiente:
![funcion parseo de mensaje mqtt.png](Imagenes_anexos/Definitivo/Node-RED/funcion_parseo_de_mensaje_mqtt.png)

3. Posteriormente se añaden los valores como una propiedad de un objeto **msg** de Javascript y se termina de integrar en un payload que ingresará a la base de datos InfluxDB.
![fun formato mensaje.png](Imagenes_anexos/Definitivo/Node-RED/fun_formato_mensaje.png)

4. Se configura el nodo Influxdb para darle acceso a nuestra base de datos.
![nodo influx in1.png](Imagenes_anexos/Definitivo/Node-RED/nodo_influx_in1.png)
En el servidor se da la información de usuario y contraseña, además del nombre de la base de datos. Sin toda esta información los datos no se guardarían, y grafana no podría generar gráficos.
![nodo influx in.png](Imagenes_anexos/Definitivo/Node-RED/nodo_influx_in.png)

5. Una vez desplegado el flujo de Node-RED ya se puede observar que ya hay datos en la base de datos, tanto por los gráficos, o por el cliente **influx** o también por el cliente web de InfluxDB

![influx valores de temp.png](Imagenes_anexos/Definitivo/InfluxDB/influx_valores_de_temp.png)

### Sección web
#### Vista rápida

1. En el nodo *Frame Hora* hay que ingresar el código HTML que sacamos de grafana del panel que muestra la hora (ya integrado al instalar la aplicación). En cuanto al campo **Group** ya fue definido en el código que brindamos del flujo de Node-RED.

![conf vista rapida hora.png](Imagenes_anexos/Definitivo/Node-RED/conf_vista_rapida_hora.png)

2. Para los valores instantáneos de los sensores los pasos son los mismos.
![conf vista rapida temp.png](Imagenes_anexos/Definitivo/Node-RED/conf_vista_rapida_temp.png)

Como se ve en el código HTML hace referencia a la dirección IP de Grafana. Esto es muy importante de entender, esta dirección puede variar (y de hecho lo hará) de equipo a equipo y red IP (son conocimientos que no se explicarán en el presente proyecto).

Pasamos a la segunda pestaña, de **Registros**, que podemos acceder desde
#### Registros históricos
##### Valores en función del tiempo e intervalo de muestra
Es interesante poder mostrar los gráficos de los datos históricos de los sensores en un intervalo de tiempo que consideremos de estudio. Para esto necesitamos dos formularios, que cada uno tendrá una fecha y una hora, y se corresponderán con el inicio y final de muestra. También es importante determinar el intervalo de refresco.

No profundizaremos en detalles en cuanto los formularios y botones para setear parámetros, pero si es importante entender como se actualizan las ventanas de tiempo en los gráficos.

![nodos datos historicos.png](Imagenes_anexos/Definitivo/Node-RED/nodos_datos_historicos.png)

En el nodo función **refresh, from y to - Datos históricos** se envía como parte de la salida un mensaje con la información de la ventana temporal de muestra. Para ser mas específicos, tomemos de ejemplo el grafico de temperatura ambiental. El código HTML:
```Javascript
<iframe src="http://192.168.20.129:3000/d-solo/CelyNFmgz/edicion_escenario1-edukit_2?refresh=10s&orgId=1&from=1553689931205&to=1553707931000&panelId=2" width="450" height="200" frameborder="0"></iframe>
```
En la parte 
```Javascript
refresh=10s&orgId=1&from=1553689931205&to=1553707931000
```
esta la información de interés. Los formularios y la función antes descripta modifica esto, y luego se actualiza nuestro gráfico.

El código completo se copia en el nodo **template** de color naranja:

![template temp nodo naranja.png](Imagenes_anexos/Definitivo/Node-RED/template_temp_nodo_naranja.png)

En el último nodo **template** de color verde solo toma la carga del mensaje que sale del nodo anterior (msg.payload) y ahora si es mostrado en la web. Su configuración es la siguiente:

![template ui temp.png](Imagenes_anexos/Definitivo/Node-RED/template_ui_temp.png)

##### Exportando datos de los gráficos
Si quisiéramos exportar los datos que obtuvimos de los sensores para un posterior análisis se necesario hacer una consulta a la base de datos y no a Grafana.

Para hacerlo con el nodo **date picker** se setea el día de inicio y de fin como ventana temporal para poder exportar los datos. Esta información no es más que estampas de tiempo, que luego en el nodo siguiente en una función se da el formato que necesita para hacer la consulta. La consulta en crudo a influxDB es:

```SQL
SELECT "name","temp" FROM invernadero WHERE time > [estampa de tiempo de inicio] AND (time < [estampa de tiempo final])
```

La información devuelta por la base de datos se guarda en un archivo que poder defecto en el caso de la Raspberry Pi, en `/home/pi/[nombre-dato.csv]`. No es posible elegir le directorio ni el nombre, entonces si se quiere cambiar esta propiedad se debe acceder a los nodos **csv** de cada medición y en el nodo **file** también de cada medición.
Nodo *csv*:
![csv temp.png](Imagenes_anexos/Definitivo/Node-RED/csv_temp.png)
Nodo *file*:
![file csv temp.png](Imagenes_anexos/Definitivo/Node-RED/file_csv_temp.png)

Un agregado interesante es que cada vez que se exporta una notificación en la web nos indica que el proceso se llevó a cabo con éxito. De lo contrario es probable que no se haya exportado la información.

## Visualización y resultados
Para acceder a la web y ver los resultados se debe ingresar con un navegador a http://localhost:1880/ui/. Cambiar `localhost` por la IP correspondiente si se quiere acceder desde un equipo remoto. La visualización de la web pestaña con datos instantáneos en un display con resolución 1920x1080 es la siguiente:

- Página principal:
![web invernadero datos rapidos.png](Imagenes_anexos/Definitivo/Node-RED/web_invernadero_datos_rapidos.png)

- Despliegue de pestañas:
![web invernadero datos rapidos menu desplegado.png](Imagenes_anexos/Definitivo/Node-RED/web_invernadero_datos_rapidos_menu_desplegado.png)

- Selección de ventana temporal para muestra de gráficos:
![web invernadero registros sin graficos.png](Imagenes_anexos/Definitivo/Node-RED/web_invernadero_registros_sin_graficos.png)

- Gráficos enventanados:

  ![web invernadero registros con temp.png](Imagenes_anexos/Definitivo/Node-RED/web_invernadero_registros_con_temp.png)

![web invernadero registros con humedad.png](Imagenes_anexos/Definitivo/Node-RED/web_invernadero_registros_con_humedad.png)

![web invernadero registros con luz.png](Imagenes_anexos/Definitivo/Node-RED/web_invernadero_registros_con_luz.png)

![web invernadero registros con mq9.png](Imagenes_anexos/Definitivo/Node-RED/web_invernadero_registros_con_mq9.png)

- Menú para exportar gráficos:

![web invernadero registros con luz y exportar.png](Imagenes_anexos/Definitivo/Node-RED/web_invernadero_registros_con_luz_y_exportar.png)

## Configuraciones adicionales
### Seguridad en Node-RED

Puede ser crítico que alguien acceda a nuestros nodos de Node-RED, pero se puede securizar con un usuario y contraseña, de manera que el flujo siga funcionando pero solamente le administrador pueda acceder y modificar cosas.

Para securizar Node-RED necesitamos primero descargar `node-red-admin`, que lo haremos desde el directorio donde están los archivos de Node-RED con la línea siguiente desde la terminal:

```Bash
sudo npm install -g node-red-admin
```

Luego de la instalación debemos generar un hash para las password que nosotros deseemos. En nuestro caso usaremos `it10`.

```Bash
node-red-admin hash-pw
```
La herramienta nos pedirá la password, se ingresa, y nos devolverá el hash. Este mismo lo colocaremos en el archivo de opciones de Node-RED `settings.js`. Accediendo al archivo, se debe editar lo siguiente:

```Java
adminAuth: {
    type: "credentials",
    users: [{
        username: "[NOMBRE_USUARIO]",
        password: "[HASH_GENERADO]",
        permissions: "*"
    }]
}
```

El asterisco en *permissions* indica que tiene permisos de administrador. Se puede generar otro usuario con otro permiso, como de lectura (se pone *read* en ese campo).

Podemos ver que cada vez que accedemos a la web local nos pedirá identificarnos.
![auth nodered.png](Imagenes_anexos/Definitivo/Node-RED/auth_nodered.png)

