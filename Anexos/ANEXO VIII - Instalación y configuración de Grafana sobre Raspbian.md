# Anexo VIII

## Grafana

### Instalación

Para la Raspberry Pi 3 B+ se puede instalar por terminal. Primero se descarga:

```Bash
wget https://dl.grafana.com/oss/release/grafana_5.4.3_armhf.deb
sudo dpkg -i grafana_5.4.3_armhf.deb
```

Luego se indica los siguiente para instalar Grafana:

```Bash
	### NOT starting on installation, please execute the following statements to configure grafana to start automatically using systemd
 sudo /bin/systemctl daemon-reload
 sudo /bin/systemctl enable grafana-server
	### You can start grafana-server by executing
 sudo /bin/systemctl start grafana-server
```

Por ultimo habilitamos el servicio y activamos el demonio para que inicie la herramienta.

```Bash
	sudo /bin/systemctl daemon-reload
	sudo /bin/systemctl enable grafana-server
	sudo /bin/systemctl restart grafana-server
	sudo service grafana-server start
	sudo systemctl enable grafana-server.service
	sudo /lib/systemd/systemd-sysv-install enable grafana-server
```

Ahora sabiendo la dirección IP del equipo, ingresando en un navegador web con `http://<IP>:3000/` se puede acceder a Grafana y desarrollar un dashboard.