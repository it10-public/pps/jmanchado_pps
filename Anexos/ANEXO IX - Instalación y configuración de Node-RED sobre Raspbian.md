# Anexo IX

## Node-RED sobre Raspbian

### Instalación de Node-RED

Se usa el comando siguiente en la terminal:

```Bash
	bash <(curl -sL https://raw.githubusercontent.com/node-red/raspbian-deb-package/master/resources/update-nodejs-and-nodered)
```

En instalaciones mínimas de Debian puede que haga falta correr `sudo apt-get install build-essential` antes del comando anterior. No es necesario en nuestro caso porque ya estaba instalado.

#### Descripción del script

Los nodos se pueden administrar ahora a través del palette manager incorporado. Sin embargo, la instalación predeterminada de Pi precarga algunos nodos globalmente, por lo que no se pueden administrar y actualizar fácilmente. La intención del script es para:

1. Actualizar un usuario existente a LTS 8.x o 10.x Node.js y al último Node-RED.
2. Migrar los nodos instalados globalmente existentes al espacio de los usuarios  `~ / .node-red` para que puedan administrarse a través del palette manager.
3. Opcionalmente (re) instalar los nodos adicionales que están preinstalados en una imagen completa de Raspbian Pi
4. Nota: NO actualiza ningún usuario instalado nodos existentes. Esto debe hacerse manualmente por el usuario

Nota: NO actualiza ningún usuario instalado nodos existentes. Esto debe hacerse manualmente por el usuario

Aunque dirigido al usuario de Pi, el script también se ejecutará en cualquier sistema operativo basado en Debian, como Ubuntu, y por lo tanto se puede usar en otras plataformas de hardware, aunque no se ha probado ampliamente.

El comando anterior ejecuta muchos comandos como `sudo` y elimina los directorios existentes de Node.js y Node-RED. 

El script realizará los siguientes pasos:

1. Pregunta si desea (re) instalar los nodos Pi adicionales
2. Guardar una lista de los nodos Node-RED instalados globalmente que se encuentran en `/ usr / lib / node_modules`
3. apt-get remove nodered
4. Elimina todos los archivos binarios del nodo rojo de `/ usr / bin y / usr / local / bin`
5. Elimina cualquier módulo de Node-RED de `/ usr / lib / node_modules` y  `/ usr / local / lib / node_modules`
6. Detecta si Node.js se instaló desde el paquete Node.js o Debian
7. Si no es v8 o más nuevo, eliminarlo según corresponda e instalar el último v8 o v10 LTS (que no usa apt).
8. Limpia la caché npm y la caché .node-gyp para eliminar cualquier versión anterior del código
9. Instala la última versión de Node-RED.
10. Reinstalar bajo la cuenta de usuario cualquier nodo que se haya instalado previamente globalmente
11. Reinstalar los nodos Pi adicionales si es necesario
12. Reconstruir todos los nodos: para volver a compilar los binarios para que coincidan con la última versión de Node.js
13. Agregue los comandos node-red-start, node-red-stop y node-red-log a / usr / bin
14. Añadir menú de acceso directo y el icono
15. Agregar script systemd y establecer usuario
16. Si en un Pi agrega una temperatura de CPU -> ejemplo de IoT

### Corriendo Node-RED

Para iniciar Node-RED, hay dos formas:

- En el escritorio, seleccionar Menu -> Programming -> Node-RED.
- O por medio de la terminal,  `node-red-start`

Nota: cerrar la ventana (o `ctrl + c`) no frena la ejecución de Node-RED (seguirá corriendo en segundo plano)

Para parar Node-RED, ejecutar el comando `node-red-stop`.

Para ver el log, ejecutar el comando `node-red-log`.

### Inicio automático en el booteo

Usar comando:

```Bash
sudo systemctl enable nodered.service
```

Y de la misma manera, hacer `sudo systemctl disable nodered.service` para deshabilitar el inicio automático de Node-RED en el booteo.

#### Instalación de nuevos nodos

Instalaremos nuevos nodos para trabajar, estos son:

- Dashboard
  - `npm install node-red-dashboard`
- Mqtt-Broker
  - `npm install node-red-contrib-mqtt-broker`
- InfluxDB
  - `npm install node-red-contrib-influxdb`

Hay dos formas de poder instalarlos, que describiremos a continuación.

- **Usando el editor:**

Desde la version 0.15 se pueden instalar nodos directamente usando el editor. para esto se debe ir a **Manage Palette** desde el **menu**, luego seleccionar la pestaña **install**. Desde aquí se pueden buscar nuevos nodos, instalarlos, actualizarlos, habilitar o deshabilitar nodos existentes.

- Usando nodos con paquetes npm

Se puede instalar de manera local en el directorio del usuario(por defecto, $HOME/.node-red):

```Bash
	cd $HOME/.node-red
	npm install <npm-package-name>
```

Después de esto se debe hacer **stop** y **restart** sobre Node-RED para cargar los nuevos nodos.

