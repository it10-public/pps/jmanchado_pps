# ANEXO IV

## ESPurna
## Instalando Espurna

1. Borrar la flash:
Para esto hay que tener esptool, que esta en python. Si lo hacemos desde linux seguramente hay que instalar Pyserial, pero primero instalar pip.
En la terminal:

```Bash
curl -O https://bootstrap.pypa.io/get-pip.py
sudo python get-pip.py
```
Después instalar Pyserial:
```Bash
pip install Pyserial
```
Luego cambiar permisos a /dev/ttyUSB0

Por ultimo:

```Bash
esptool.py --port /dev/ttyUSB0 erase_flash
```

2. Cargar nuevo firmware:
Vamos a usar Visual Studio Code, se descarga, se instala. También hace falta Node.js y npm. Seguir pasos de la web: https://nodejs.org/en/download/

Ya en VSC, abrir terminal, haciendo `crtl+shift+P` y escribir Terminal: Create New Integrated Terminal. Ya en la terminal poner:
```Bash
npm install --global gulp-cli
```

3. Instalar extensiones PlatformIO IDE, opcional python y gitlens.

4. Clone https://github.com/xoseperez/espurna.git
Cargar la carpeta espurna como proyecto, pararse en espurna/code. Así arranca PlatformIO automáticamente

5. ir a platformio.ini, y modificar el env_default por este:
`env_default = wemos-d1mini`
El anterior lo comentamos por las dudas

## Conectarse a la WeMos D1 mini

6. Conectarse al red de ESPURNA-XXXXX. La pass es fibonacci.

7. Ir al explorador e ingresar a 192.168.4.1. Ingresar con admin:fibonacci. Pedirá cambiar la contraseña, le ponemos **It10it10**. La guardamos. Se va a desconectar de la red Wifi. Borramos la información de la red para poder ingresar con la nueva contraseña.

8. Ingresar nuevamente a 192.168.4.1 con la pass nueva.

9. Ir a WIFI, agregar las redes que hagan falta, por ejemplo:
`Sin_Conexion/IT10coop.com.ar/192.168.20.200/192.168.20.1/8.8.8.8`

10. Guardar, reconectar y conectarse a esta red ingresada, ya que el dispositivo va a estar ahí.

11. Ingresar a la IP del dispositivo que ingresamos antes, por ejemplo 192.168.20.200

12. Para modificar mqtt una cosa distinta a TASMOTA es que pide QoS, retain y keep alive. Ademas hay que decir si se usa JSON como payload.

El root topic es: sensar/ppstest/espurna

13. Instalar node-red para interactuar con dispositivo. Seguir el tutorial hasta el paso 4(no incluido)(Despues revisar el resto para securizar la conexión): https://www.digitalocean.com/community/tutorials/how-to-connect-your-internet-of-things-with-node-red-on-ubuntu-16-04

## Modificando los parámetros para una configuración directa
Podemos modificar los parámetros iniciales para no tener que realizarlos luego via web. Para esto hay que ir a `code/espurna/config/general.h`, donde modificaremos los aspectos:

- Contraseña de administrador
Cambiamos la contraseña:
```Bash
#define ADMIN_PASS              "fibonacci"     // Default password (WEB, OTA, WIFI SoftAP)
```
A la siguiente:
```Bash
#define ADMIN_PASS              "It10it10"     // Default password (WEB, OTA, WIFI SoftAP)
```

- Paramtetros de red WiFi
En la seccion de wifi, modificamos donde corresponde con:

```Bash
// Optional hardcoded configuration (up to 2 networks)
#ifndef WIFI1_SSID
#define WIFI1_SSID                  "Sin_Conexion"
#endif

#ifndef WIFI1_PASS
#define WIFI1_PASS                  "IT10coop.com.ar"
#endif

#ifndef WIFI1_IP
#define WIFI1_IP                    "192.168.20.200"
#endif

#ifndef WIFI1_GW
#define WIFI1_GW                    "192.168.20.1"
#endif

#ifndef WIFI1_MASK
#define WIFI1_MASK                  "255.255.255.0"
#endif

#ifndef WIFI1_DNS
#define WIFI1_DNS                   "8.8.8.8"
#endif
```

- Web
Para que no nos solicite cambiar la contraseña de administrador nuevamente, ponemos un cero en la linea:

```Bash
#define WEB_FORCE_PASS_CHANGE       1          // Force the user to change the password if default one
```

- MQTT

Debemos habilitar la posibilidad de comunicarnos por MQTT. Ponemos un 1 en la línea:
```Bash
#define MQTT_ENABLED                0               // Do not enable MQTT connection by default
```
Luego hay que brindar información del broker, usuario, contraseña y root topic. Se hace como sigue:

```Bash
#ifndef MQTT_SERVER
#define MQTT_SERVER                 "emqtt.it10coop.com.ar"              // Default MQTT broker address

#endif

#ifndef MQTT_USER
#define MQTT_USER                   "it10"              // Default MQTT broker usename
#endif

#ifndef MQTT_PASS
#define MQTT_PASS                   "it10"              // Default MQTT broker password
#endif

#ifndef MQTT_PORT
#define MQTT_PORT                   1883            // MQTT broker port
#endif

#ifndef MQTT_TOPIC
#define MQTT_TOPIC                  "sensar/ppstest/espurna"    // Default MQTT base topic
#endif
```

- Hostname
En el archivo `defaults.h` en la misma carpeta modificamos la línea con el nombre que queremos. Si queda vacío por defecto el nombre que tiene es ESPURNA-XXXXXX (con los 3 últimos octetos del chipID). Cambiamos el nombre de esta manera:

```Bash
#define HOSTNAME                "wemos-d1-mini"
```

#### MQTT
Otra información interesante del archivo `general.h`, son las partículas que se pueden concatenar al topic para interactuar con la WeMos d1 mini:

```Bash
// These particles will be concatenated to the MQTT_TOPIC base to form the actual topic
#define MQTT_TOPIC_JSON             "data"
#define MQTT_TOPIC_ACTION           "action"
#define MQTT_TOPIC_RELAY            "relay"
#define MQTT_TOPIC_LED              "led"
#define MQTT_TOPIC_BUTTON           "button"
#define MQTT_TOPIC_IP               "ip"
#define MQTT_TOPIC_SSID             "ssid"
#define MQTT_TOPIC_VERSION          "version"
#define MQTT_TOPIC_UPTIME           "uptime"
#define MQTT_TOPIC_DATETIME         "datetime"
#define MQTT_TOPIC_FREEHEAP         "freeheap"
#define MQTT_TOPIC_VCC              "vcc"
#define MQTT_TOPIC_STATUS           "status"
#define MQTT_TOPIC_MAC              "mac"
#define MQTT_TOPIC_RSSI             "rssi"
#define MQTT_TOPIC_MESSAGE_ID       "id"
#define MQTT_TOPIC_APP              "app"
#define MQTT_TOPIC_INTERVAL         "interval"
#define MQTT_TOPIC_HOSTNAME         "host"
#define MQTT_TOPIC_TIME             "time"
#define MQTT_TOPIC_RFOUT            "rfout"
#define MQTT_TOPIC_RFIN             "rfin"
#define MQTT_TOPIC_RFLEARN          "rflearn"
#define MQTT_TOPIC_RFRAW            "rfraw"
#define MQTT_TOPIC_UARTIN           "uartin"
#define MQTT_TOPIC_UARTOUT          "uartout"
#define MQTT_TOPIC_LOADAVG          "loadavg"
#define MQTT_TOPIC_BOARD            "board"
#define MQTT_TOPIC_PULSE            "pulse"
#define MQTT_TOPIC_SPEED            "speed"
#define MQTT_TOPIC_IRIN             "irin"
#define MQTT_TOPIC_IROUT            "irout"
#define MQTT_TOPIC_OTA              "ota"

// Light module
#define MQTT_TOPIC_CHANNEL          "channel"
#define MQTT_TOPIC_COLOR_RGB        "rgb"
#define MQTT_TOPIC_COLOR_HSV        "hsv"
#define MQTT_TOPIC_ANIM_MODE        "anim_mode"
#define MQTT_TOPIC_ANIM_SPEED       "anim_speed"
#define MQTT_TOPIC_BRIGHTNESS       "brightness"
#define MQTT_TOPIC_MIRED            "mired"
#define MQTT_TOPIC_KELVIN           "kelvin"
#define MQTT_TOPIC_TRANSITION       "transition"

#define MQTT_STATUS_ONLINE          "1"         // Value for the device ON message
#define MQTT_STATUS_OFFLINE         "0"         // Value for the device OFF message (will)

#define MQTT_ACTION_RESET           "reboot"    // RESET MQTT topic particle
```

## Pruebas con ESPurna y WeMos D1 mini
No hay suficiente documentación como para realizar pruebas similares a las que se hacen con Tasmota, esto es, programar temporizadores o triggers para realizar tareas, por ende realizaremos algo similar usando **Node-Red** y aportando estas funcionalidades desde ahí y no desde el firmware. La imposibilidad de usar la opción `Schedule` se puede deber a que el firmware instalado es solo para WeMos d1 mini, y no el relayshield WeMos d1 mini.

### Disposición de pines
La disposición de pines de la WeMos D1 mini es la siguiente:

![Disposicion de pines Wemos D1 Mini](https://escapequotes.net/wp-content/uploads/2016/02/d1-mini-esp8266-board-sh_fixled.jpg)

### Probando conectar un LED

Cambiando el archivo `hardware.h` en la parte de la wemos-d1-mini. Agregamos lineas:
``` Bash
#define LED2_PIN            5
#define LED2_MODE           LED_MODE_MQTT
#define LED2_PIN_INVERSE    0
```
Luego configurar como antes despues de cargado el firmware.
Para tener en cuenta, poner en la parte de `General`, en `LED Mode` seleccionar **"MQTT Managed"**, para poder modificar el estado con mensajes MQTT. Si enviamos un 0, se apaga el led; con 1 se enciende; con 2 alterna el estado y cambia de acuerdo al que tiene actualmente (si esta on, se pone off, y viceversa).

#### Enviando mensajes con Node-RED
Una vez abierta la interfaz se colocan dos bloques:
- input/inject
- output/mqtt

El primero es para enviar un string con el valor que quiero para modificar el estado. En nuestro caso enviaremos un "2", asi el led alterna entre on/off. Esto se envia al topic: **sensar/ppstest/espurna/led/0/set**, y pedimos que se repita con intervalo de 1 segundo.

Luego, con el bloque MQTT configuramos el broker, con el mismo topic y por ultimos hacemos DEPLOY. El led alternara entre on/off a traves de mensajes MQTT.

### Conectando un DHT11

Primero se debe habilitar el soporte para el sensor en `code/espurna/config/sensor.h`. Se debe modificar estas lineas en la seccion `DHTXX temperature/humidity sensor`:

```Bash
#define DHT_SUPPORT                     0
```
por
```Bash
#define DHT_SUPPORT                     1
```
y

```bash
#define DHT_TYPE                        DHT_CHIP_DHT22
```

por

```Bash
#define DHT_TYPE                        DHT_CHIP_DHT11
```
El pin no lo modificamos, es el GPIO 14, que en la WeMos D1 mini la identificamos como D5.

Además en el archivo `platformio.ini` hay que agregar los flags donde se refiere a nuestra placa WeMos D1 mini los siguientes flags

```Bash
build_flags = ${common.build_flags} -DWEMOS_D1_MINI -DDEBUG_FAUXMO=Serial -DNOWSAUTH -DDHT_SUPPORT=1 -DDHT_PIN=14 -DDHT_TYPE="DHT_CHIP_DHT11"
```
Los últimos tres indican que se usará un sensor DHT, el pin 14 y el sensor DHT11.

Luego, se puede modificar el el intervalo de tiempo y la cantidad de mediciones que se desean obtener cambiando los parámetros siguientes de `sensor.h`:

```Bash
// =============================================================================
// SENSORS - General data
// =============================================================================

#define SENSOR_DEBUG                        0               // Debug sensors

#define SENSOR_READ_INTERVAL                6               // Read data from sensors every 6 seconds

#define SENSOR_READ_MIN_INTERVAL            1               // Minimum read interval
#define SENSOR_READ_MAX_INTERVAL            3600            // Maximum read interval
#define SENSOR_INIT_INTERVAL                10000           // Try to re-init non-ready sensors every 10s

#define SENSOR_REPORT_EVERY                 10              // Report every this many readings

#define SENSOR_REPORT_MIN_EVERY             1               // Minimum every value
#define SENSOR_REPORT_MAX_EVERY             60              // Maximum
```

## Uso de memoria

DATA:    [=====     ]  53.5% (used 43800 bytes from 81920 bytes)
PROGRAM: [=         ]  11.4% (used 478092 bytes from 4194304 bytes)