# ANEXO III

## Sonoff-Tasmota

![](https://raw.githubusercontent.com/blakadder/Sonoff-Tasmota/development/tools/logo/TASMOTA_FullLogo_Vector.svg?sanitize=true)

Firmware alternativo para dispositivos basados en ESP8266 con web, temporizadores, actualizaciones de firmware 'Over The Air' (OTA) y compatibilidad con sensores, lo que permite el control bajo Serial, HTTP, MQTT y KNX, para su uso en Smart Home Systems. Escrito para Arduino IDE y PlatformIO.

### Dispositivos soportados
Los siguientes dispositivos son compatibles con el control Serial, Web y MQTT:

- iTead Sonoff Basic (R2)
- iTead Sonoff RF
- iTead Sonoff SV
- iTead Sonoff TH10/TH16 con sensor temperature
- iTead Sonoff Dual (R2)
- iTead Sonoff Pow con Energy Monitoring
- iTead Sonoff Pow R2 con Energy Monitoring
- iTead Sonoff 4CH (R2)
- iTead Sonoff 4CH Pro (R2)
- iTead S20 Smart Socket
- Sonoff S22 Smart Socket
- iTead Sonoff S26 Smart Socket
- iTead Sonoff S31 Smart Socket con Energy Monitoring
- iTead Slampher
- iTead Sonoff Touch
- iTead Sonoff T1
- iTead Sonoff SC
- iTead Sonoff Led
- iTead Sonoff BN-SZ01 Ceiling Led
- iTead Sonoff B1 (R2)
- iTead Sonoff iFan02
- iTead Sonoff RF Bridge 433
- iTead Sonoff Dev
- iTead Sonoff PSA Smart Switch Module
- iTead 1 Channel Switch 5V / 12V
- iTead Motor Clockwise/Anticlockwise
- Electrodragon IoT Relay Board
- AI Light o cualquier my9291 compatible RGBW LED bulb
- H801 PWM LED controller
- MagicHome PWM LED controller (aka Flux-Light LED module)
- AriLux AL-LC01, AL-LC06 y AL-LC11 PWM LED controller
- Supla device - Espablo-inCan mod. for electrical Installation box
- BlitzWolf BW-SHP2 Smart Socket con Energy Monitoring
- BlitzWolf BW-SHP4 Smart Socket con Energy Monitoring (UK version)
- BlitzWolf BW-SHP6 Smart Socket with Energy Monitoring
- Luani HVIO board
- Wemos D1 mini
- HuaFan Smart Socket
- Hyleton-313 Smart Plug
- Allterco Shelly 1
- Allterco Shelly 2 con Energy Monitoring
- NodeMcu y Ledunia
- Switches basados en KS-602, como GresaTek, Jesiya, NewRice, Lyasi, etc
- OBI Wifi Stecker Schuko
- OBI Wifi Stecker Schuko weiß (Rev.2)
- SmartPlug AISIRER, AVATAR con POW
- SmartPlug NEO COOLCAM NAS WR01W

El que usaremos para desarrollar algunas pruebas es el **Wemos D1 mini**.

### Características
Las siguientes características están disponibles:

- MQTT `GroupTopic` puede abordar múltiples dispositivos
- Carga de firmware por OTA o mediante carga de página web
- Mensajes de estado expandidos
- Los mensajes de Syslog UDP se pueden filtrar por nombre de programa que comienza con `ESP-`
- El botón puede enviar un mensaje MQTT diferente definido con `ButtonTopic`
- Configuración inicial de Wifi por `user_config.h`, Serial, Smartconfig, Wifi manager o configuración WPS
- Un servidor web proporciona el control de Sonoff y contiene una facilidad de carga de firmware
- Compatibilidad con los sensores de temperatura DHTxx, AM2301 o DS18B20 que se utilizan en Sonoff TH10 / TH16
- Soporte para sensores I2C BH1750, BME280, BMP280, HTU21, SI70xx, LM75AD y SHT1x
- Los datos de telemetría se pueden enviar usando un prefijo diferente opcional de los mensajes de estado
- Soporte nativo de Domoticz MQTT
- Fácil integración en soluciones de automatización del hogar como openHAB, HomeAssistant, ...
- Emulación de Wemo y Hue para el soporte de Amazon Echo (Alexa)
- Compatibilidad con el protocolo IP KNX para la comunicación a redes KNX y también de dispositivo a dispositivo.

## Primeros pasos
### Pre-requisitos
#### Hardware necesario

- Uno de los módulos ESP8266 soportados
- Un convertidor de USB a serie de muy buena calidad que puede suministrar 3.3V suficientes para alimentar el dispositivo (o una fuente de alimentación de 3.3V por separado).

#### Software necesario

Hay binarios disponibles para descargar e instalar. Instalar un binario es mucho más fácil que configurar **PlatformIO** o **Arduino** para construir el proyecto. Se puede usar el software Esptool (https://github.com/arendst/Sonoff-Tasmota/wiki/Esptool) para actualizar un binario preconfigurado en su dispositivo. Los archivos binarios están en *releases section*(https://github.com/arendst/Sonoff-Tasmota/releases ).

Si se necesita o se desea modificar el código o la configuración predeterminada, se puede utilizar uno de los siguientes:

1. [PlatformIO - https://platformio.org/ ](https://platformio.org/ ) (todas las bibliotecas y configuraciones necesarias están preconfiguradas en [platformio.ini- https://github.com/arendst/Sonoff-Tasmota/blob/master/platformio.ini](https://github.com/arendst/Sonoff-Tasmota/blob/master/platformio.ini))
2. [Arduino IDE- https://www.arduino.cc/en/Main/Software ](https://www.arduino.cc/en/Main/Software )

#### Otros requerimientos

- El código fuente del firmware
- Un broker MQTT
- Un cliente MQTT para interactuar con el módulo

### Preparación del hardware
#### Conexión serial

La siguiente tabla muestra la conexión entre los conectores del programador y los módulos. Preste atención a las líneas cruzadas RX y TX.


| Programador | Módulo Sonoff |
|--------|--------|
|     3V3   | 3V3/VCC       |
|     TX   | RX      |
|     RX   | TX       |
|     GND   | GND      |

Algunos módulos de Sonoff exponen un encabezado de cinco pines. El quinto pin es irrelevante para la conexión serial. Por favor, consulte las páginas del módulo específico para obtener más información.

#### Poniendo el módulo en modo flash

Particularmente en la Wemos D1 mini, no es necesario realizar nada, solo conectarlo a la interfaz serial.

### Upload
#### PlatformIO

1. Descargar y extraer el último "Código fuente (zip) de Sonoff-Tasmota" (o clonar el repositorio).
2. Cargue la carpeta base de Sonoff-Tasmota en PlatformIO
3. Conectar el módulo en modo Flash
4. Seleccionar una variante de firmware para instalar en su módulo sin comentar una de las líneas `env_default` en `platformio.ini` (a continuación los detalles de la variante). Es posible que también deba cambiar el `upload_port` para que coincida con el puerto de comunicaciones de la interfaz serial USB.
5. Abrir `my_user_config.h` y configure sus ajustes de WiFi y, opcionalmente, un MQTT, Syslog, WebServer, NTP, etc. NOTA: Si se carga varias veces es necesario modificar el parametro `CFG_HOLDER`. Cada vez que se intente cargar el firmware se debe cambiar el numero. Este número se almacena en la memoria flash y si el nuevo código tiene el mismo número, se conservan las configuraciones actuales en la memoria flash. Por ende, no sucederan cambios.
6. Seleccionar "Upload" en el menú para actualizar el firmware.
7. Después de la transferencia exitosa del firmware, desconectar el módulo.

Continúe en "Primeros pasos" y asegúrese de revisar las instrucciones para conectar sensores adicionales.

##### Variantes de Firmware

- `sonoff.bin`: el firmware predeterminado para todos los dispositivos
- `sonoff-minimal.bin`: es un firmware provisional que se utilizará cuando las imágenes de firmware anteriores sean demasiado grandes para que quepan como OTA o carga web; la instalación de este primero y luego la carga del sonoff.bin deseado permite un crecimiento futuro del tamaño del firmware por encima del límite OTA del archivo de 1/2 flash.
- `Sonoff-sensores`: es una versión con la mayoría de los sensores usados habilitados.

### Herramientas para la carga del firmware
#### Visual Studio Code
Cómo preparar y configurar Visual Studio Code con PlatformIO para la compilación y carga de Tasmota.

##### Descargar e instalar Visual Studio Code
Descargar Visual Studio Code (VSC) desde su pagina web (https://code.visualstudio.com/)
##### Instalar la extensión PlatformIO

Instalar la extensión *PlatformIO IDE* en VSC.

Seleccionar `Ver` - `Extensiones` y escribir *PlatformIO* en el cuadro de búsqueda.

Asegurarse de seleccionar la extensión oficial de PlatformIO.org *PlatformIO IDE* y seleccionar Instalar. Aceptar para instalar dependencias.
##### Descargar Tasmota

Descargar la última versión de Tasmota de [https://github.com/arendst/Sonoff-Tasmota/releases](https://github.com/arendst/Sonoff-Tasmota/releases) y descomprimir en una carpeta conocida.
##### Copiar archivos

Copiar todos los archivos del código fuente de la versión Tasmota en la carpeta de trabajo de VSC.
##### Compilar tasmota

Iniciar VSC y seleccionar `File` - `Open folder...` para apuntar a la carpeta de trabajo.

Nota: Presionar `Ctrl + Shift + P` y escribir **PlatformIO** para ver todas las opciones.

Seleccionar el firmware deseado editando el archivo `platformio.ini` según sea necesario.

##### Cargar Tasmota

Habilitar las opciones deseadas en `platformio.ini` para la carga serial como:
```Bash
; *** Subir el método de restablecimiento de serie para Wemos y NodeMCU
upload_port = COM5
; upload_speed = 512000
upload_speed = 115200
; upload_resetmethod = nodemcu
```

Para el caso de la Wemos D1 mini y el S.O. Lubuntu 18.04 usado, el `upload_port` en vez de ser **COM5** es **/dev/ttyUSB0**. Además hay que dar permisos para que platformIO pueda acceder al dispositivo.

Habilitar las opciones deseadas en `platformio.ini` para cargarlas en su servidor OTA local como:

```Bash
; *** Subir archivo al servidor OTA utilizando HTTP
upload_port = domus1: 80 / api / upload-arduino.php
extra_scripts = pio / http-uploader.py
```

Luego **Build**(si es necesario) y **Upload**.
##### Consejo:

En caso de que VSC muestre una gran cantidad de errores usando `PlatformIO - Intellisense`, una posible "solución" es cambiar el cpp Intelli Sense a "TAG PARSER"

Esta configuración se puede cambiar en la configuración del área de trabajo mediante: Use `Ctrl` + `Shift` + `P` y escribir `Preferences: Open Workspace Settings` y escribir `intelli Sense` en el cuadro de búsqueda. Ahora cambiar el valor de `Intelli Sense Engine` a `Tag Parser`.

### Configuración del dispositivo

Antes de compilar, considere la modificación de los valores `STA_SSID1` y `STA_PASS1` dentro de `user_config.h` para que coincidan con su **SSID** de WiFi y su **contraseña** de WiFi.

```Bash
	#define STA_SSID1 "indebuurt1" // [Ssid1] Wifi SSID
	#define STA_PASS1 "VnsqrtnrsddbrN" // [Password1] Contraseña de Wifi
```

Esta operación simplifica los primeros pasos, ya que Sonoff reconocerá y se unirá automáticamente a su WiFi.

Establecer las reglas de horario de verano/horario estándar en `user_config.h` también simplificará la configuración futura
```Bash
	#define TIME_DST  North, Last, Sun, Mar, 2, +120  // Northern Hemisphere, Last sunday in march at 02:00 +120 minutes
	#define TIME_STD  North, Last, Sun, Oct, 3, +60  // Northern Hemisphere, Last sunday in october 03:00 +60 minutes
```

Si la nueva configuración no parece aplicarse después de la actualización, puede hacer lo siguiente para solucionarlo:

- Cambie el valor de CFG_HOLDER] en `user_config.h` y volver a flashear
- Mantener presionado el botón del dispositivo durante al menos 4 segundos para iniciar un comando RESET
- Restablecer el dispositivo a través de la consola web.

### Upgrade
### Configuración inicial
Se ha cargado correctamente el firmware de Sonoff-Tasmota en su módulo.

- Conectar a la alimentación (CA) (serial NO conectado). SOLO PARA SONOFF
- Si el WiFi no se configuró antes de compilar como se describe en la sección Upload, configurarlo usando uno de los modos de configuración provistos: Uso del botón o usar WPS si el router lo admite.
- Conectarse a la página web del dispositivo (http://[sonoff-ip]/cn) NOTA: Reemplazar `[sonoff-ip]` con la dirección IP real del dispositivo.
- Seleccionar el tipo de módulo correcto desde la interfaz de configuración
- Configurar la conexión del broker MQTT, elegir un topic único
- Probar la comunicación con el cliente MQTT
- Integrarlo con la interfaz de control (Home Assistant, Domoticz, etc) de su elección.

## Flashando Wemos D1 mini con Tasmota
![Wemos D1 Mini](https://imgix.ttcdn.co/i/product/original/0/457993-1530b9ccd375411c82eb9953a15f0338.jpeg?q=100&auto=format%2Ccompress&w=2000)

Seguir pasos mencionados anteriormente para la carga de Tasmota, pero añadir las siguientes líneas al final de `platformio.ini`:
```Bash
[env:wemos-d1-mini]
platform = espressif8266
framework = arduino
board = esp01_1m
board_flash_mode = dout
build_flags = -Wl,-Tesp8266.flash.1m0.ld -DMQTT_MAX_PACKET_SIZE=1000
lib_deps = PubSubClient, NeoPixelBus, IRremoteESP8266, ArduinoJSON
extra_scripts = pio/strip-floats.py

; *** Serial Monitor options
monitor_baud = 115200

; *** Upload Serial reset method for Wemos and NodeMCU
upload_resetmethod = nodemcu
upload_speed = 115200
;upload_port = COM6

; *** Upload file to OTA server using SCP
;upload_port = user@host:/path
;extra_scripts = pio/strip-floats.py, pio/sftp-uploader.py

; *** Upload file to OTA server using HTTP
;upload_port = domus1:80/api/upload-arduino.php
;extra_scripts = pio/strip-floats.py, pio/http-uploader.py
```

Luego **Build** y **Upload**.

## Configurar Tasmota para Wemos
En primera instancia se puede conectarse a la página web del dispositivo (http://[sonoff-ip]) NOTA: Reemplazar `[sonoff-ip]` con la dirección IP real del dispositivo.

![principal](Imagenes_anexos/tasmota/principal.png)

Ingresando a `Configuration` se pueden modificar todos los parametros necesarios para poder trabajar. En nuestro caso necesitamos configurar el tipo de modulo, información de red y el broker MQTT.

![configuracion.png](Imagenes_anexos/tasmota/configuracion.png)
### Hardware ESP8266
#### Pines disponibles
El ESP8266 tiene 17 pines GPIO (0-16) pero varios están reservados o tienen restricciones. No se debe utilizar ninguno de los pines reservados. Si no, podría bloquear el programa. En el ESP8266, se usan seis pines (GPIO 6 - 11) para interconectar la memoria flash 

GPIO 1 y 3 se utilizan como TX y RX del puerto serial del hardware (UART), por lo que en la mayoría de los casos, no puede usarlos como I / O normales mientras se envían / reciben datos en serie.

#### Boot modes

Algunos pines de I/O tienen una función especial durante el arranque: seleccionan 1 de los 3 modos de arranque:

| GPIO15 | GPIO0 | GPIO2 | Mode                             |
| ------ | ----- | ----- | -------------------------------- |
| 0V     | 0V    | 3.3V  | Uart Bootloader                  |
| 0V     | 3.3V  | 3.3V  | Boot sketch (SPI flash)          |
| 3.3V   | x     | x     | SDIO mode (not used for Arduino) |

Nota: no es necesario agregar una resistencia de extracción externa a GPIO2, la interna está habilitada en el arranque.

Tenemos que asegurarnos de que se cumplan estas condiciones agregando resistencias externas, o el fabricante de la placa de su placa las ha agregado para usted. Esto tiene algunas implicaciones, sin embargo:

GPIO15 siempre está bajo, por lo que no puede usar la resistencia interna de pull-up. Debe tener esto en cuenta cuando utilice GPIO15 como entrada para leer un interruptor o conectarlo a un dispositivo con una salida de colector abierto (o de drenaje abierto), como I²C. GPIO0 se coloca alto durante el funcionamiento normal, por lo que no puede usarlo como una entrada Hi-Z. GPIO2 no puede ser bajo en el arranque, por lo que no se puede conectarle un interruptor. Las resistencias internas pull-up / -down GPIO 0-15 tienen una resistencia pull-up incorporada, como en un Arduino. GPIO16 tiene una resistencia desplegable incorporada.

#### PWM

El ESP8266 no admite hardware PWM, sin embargo, el software PWM es compatible con todos los pines digitales. El rango predeterminado de PWM es de 10 bits a 1 kHz, pero se puede cambiar (hasta > 14-bit@1 kHz).

#### Entrada analógica

El ESP8266 tiene una sola entrada analógica, con un rango de entrada de 0 - 1.0V. Si suministra 3.3V, por ejemplo, dañará el chip. Puede usarse un potenciómetro como divisor de voltaje.

El ADC (convertidor analógico a digital) tiene una resolución de 10 bits.

#### Digital I/O

Al igual que un Arduino normal, el ESP8266 tiene pines de entrada / salida digital (I/O o GPIO, pines de entrada / salida de uso general). Como su nombre lo indica, se pueden usar como entradas digitales para leer un voltaje digital, o como salidas digitales para generar 0V (corriente de sumidero) o 3.3V (corriente de fuente).

### GPIO overview

| [NodeMCU Labelled Pin](https://techtutorialsx.com/2017/04/02/esp8266-nodemcu-pin-mappings/) | GPIO  | Function             | State            | Restrictions                                                 |
| ------------------------------------------------------------ | ----- | -------------------- | ---------------- | ------------------------------------------------------------ |
| D3                                                           | 0     | Boot mode select     | 3.3V             | No Hi-Z                                                      |
| D10                                                          | 1     | TX0                  | -                | Not usable during Serial transmission                        |
| D4                                                           | 2     | Boot mode select TX1 | 3.3V (boot only) | Don’t connect to ground at boot time Sends debug data at boot time |
| D9                                                           | 3     | RX0                  | -                | Not usable during Serial transmission                        |
| D2                                                           | 4     | SDA (I²C)            | -                | -                                                            |
| D1                                                           | 5     | SCL (I²C)            | -                | -                                                            |
| x                                                            | 6 - 8 | Flash connection     | x                | Not usable, and not broken out                               |
| x                                                            | 9, 10 | Flash connection *   |                  | * Only available on the ESP8285                              |
| x                                                            | 11    | Flash connection     | x                | Not usable, and not broken out                               |
| D6                                                           | 12    | MISO (SPI)           | -                | -                                                            |
| D7                                                           | 13    | MOSI (SPI)           | -                | -                                                            |
| D5                                                           | 14    | SCK (SPI)            | -                | -                                                            |
| D8                                                           | 15    | SS (SPI)             | 0V               | Pull-up resistor not usable (extern pull down resistor)      |
| D0                                                           | 16    | Wake up from sleep   | -                | No pull-up resistor, but pull-down instead Should be connected to RST to wake up |

### Disposición de pines

La disposicion de pines de la Wemos D1 mini es la siguiente:

![Disposicion de pines Wemos D1 Mini](https://escapequotes.net/wp-content/uploads/2016/02/d1-mini-esp8266-board-sh_fixled.jpg)

### Generic module

En la página `Configuration -> Configure Module`, seleccionar **Module Type: "18 Generic"** (para versiones anteriores a la 5.12.0, esto se llamó "Wemos D1 Mini"). Después de guardar la configuración, WEMOS se reinicia con la configuración genérica.

![generic module.png](Imagenes_anexos/tasmota/generic_module.png)

Tambien se puede modificar desde el archivo `user_config.h`. Se define el modulo con:
```css
	#define MODULE			GENERIC
```

### Conexión a una red
Ingresando a `Configure Wifi` se pueden configurar dos redes con sus SSID y contraseñas respectivas. Esto es porque por defecto Tasmota intenta conectarse a la primer red, si falla, prueba con la segunda, para garantizar la conexión wireless en todo momento.

![wifi.png](Imagenes_anexos/tasmota/wifi.png)

### Broker MQTT
Se procede a configurar los parametros para poder interactuar con el dispositivo a tras del protocolo de mensajería MQTT.

![mqtt.png](Imagenes_anexos/tasmota/mqtt.png)

### Configuración inicial desde `user_config.h`

Tambien se puede modificar el archivo `my_user_config.h` para tener una configuración inicial personalizada sin necesidad de operar el dispositivo desde la WebUI. 
Se define el modulo para trabajar con Wemos D1 Mini con:
```css
	#define MODULE			GENERIC
```

En cuanto a la información de red:
```css
	#define WIFI_IP_ADDRESS        "0.0.0.0"         // [IpAddress1] Set to 0.0.0.0 for using DHCP or enter a static IP address
	#define WIFI_GATEWAY           "192.168.1.1"     // [IpAddress2] If not using DHCP set Gateway IP address
	#define WIFI_SUBNETMASK        "255.255.255.0"   // [IpAddress3] If not using DHCP set Network mask
	#define WIFI_DNS               "192.168.1.1"     // [IpAddress4] If not using DHCP set DNS IP address (might be equal to WIFI_GATEWAY)

	#define STA_SSID1              "[Ssid1]"                // [Ssid1] Wifi SSID
	#define STA_PASS1              "[Password1]"                // [Password1] Wifi password
	#define STA_SSID2              "[Ssid2]"                // [Ssid2] Optional alternate AP Wifi SSID
	#define STA_PASS2              "[Password2]"                // [Password2] Optional alternate AP Wifi password
	#define WIFI_CONFIG_TOOL       WIFI_RETRY        // [WifiConfig] Default tool if wifi fails to connect

```

Para configurar el broker MQTT:
```css
	#define MQTT_HOST              "direccion-MqttHost"                // [MqttHost]
	[...]
	#define MQTT_PORT              1883              // [MqttPort] MQTT port (10123 on CloudMQTT)
	#define MQTT_USER              "[MqttUser]"       // [MqttUser] MQTT user
	#define MQTT_PASS              "[MqttPassword]"       // [MqttPassword] MQTT password
	[...]
	#define MQTT_TOPIC             "Nombre-topic"           // [Topic] (unique) MQTT device topic, set to 'PROJECT "_%06X"' for unique topic including device MAC address
```
Luego puede haber mas o menos parametros configurables, pero que es posible hacerlo desde un primer momento antes de compilar y flashear la placa, y de esta forma evitar usar la WebUI.

## Pruebas con Tasmota
Para probar el funcionamiento se han desarrollado una serie de escenarios. Estos se eligieron en base a posibles implementaciones a futuro, y capacidad de escalabilidad. Se ven a continuación.

### Probando comando Timer por MQTT
Usando un timer se puede ejecutar acciones cuando estos acaben. Para que sea posible utilizarlos se deben programar reglas. Cada regla debe seguir la siguiente sintaxis:

```
on <trigger> do <command> endon
```

**on** inicia una nueva regla.

**<trigger>** es lo que necesita que ocurra para ejecutar el `<command>`

**do** separador entre `<trigger>` y`<command>`

**endon**  marca el final de la regla. Puede ser seguida por otra regla.

Listado de reglas y comandos en https://github.com/arendst/Sonoff-Tasmota/wiki/Rules.

Es util para generar timers en el que se pueda activar algo y repetirse en todos los dias (con la parte de days, que cada cifra es un dia de la semana, arrancando del domingo) a la misma hora, con mas o menos opciones. 

Usando el cliente `mosquitto`, publicando al broker emqtt de la cooperativa al topic cmnd/.../.../<command> se puede enviar comandos a la WeMos D1 mini. Por ejemplo

```
mosquitto_pub -h emqtt.it10coop.com.ar -p 1883 -t cmnd/sensar/ppstest/tasmota-wemos-d1-mini/timer1 -m '{"Arm":1,"Time":"17:56","Days":"0010000","Output":1,"Action":3}'
```

#### Muestra direccion IP cada 10 segundos, usando un solo timer
1- Hacer mosquitto_pub a:

```
mosquitto_pub -h emqtt.it10coop.com.ar -p 1883 -t cmnd/sensar/ppstest/tasmota-wemos-d1-mini/rule1 -m 'on rules#timer=1 do IPAddress1;ruletimer1 3 endon on rules#timer=1 do backlog ruletimer1 3;IPAddress1 endon'
```

2- Despues activar regla con on:

```
mosquitto_pub -h emqtt.it10coop.com.ar -p 1883 -t cmnd/sensar/ppstest/tasmota-wemos-d1-mini/rule1 -m 'on'
```

3- Luego setear el timer haciendo:

```
mosquitto_pub -h emqtt.it10coop.com.ar -p 1883 -t cmnd/sensar/ppstest/tasmota-wemos-d1-mini/ruletimer1 -m "10"
```

El numero 10 es el tiempo con el que arranca el timer, para luego ejecutar la regla. Despues de esto, el timer se renueva de acuerdo a la regla que generamos, que puede ser el mismo tiempo u otro.

NOTA: Si se quiere usar otro timer, se hace lo mismo, publicando a */rule2* la regla (con un *rules#timer=2*, para indicar que finaliza el timer 2 y no otro), activando la regla con "on", y seteando el nuevo timer.

```
mosquitto_pub -h emqtt.it10coop.com.ar -p 1883 -t cmnd/sensar/ppstest/tasmota-wemos-d1-mini/rule2 -m 'on rules#timer=2 do status 6;ruletimer2 30 endon on rules#timer=2 do backlog ruletimer2 30;status 6 endon'
mosquitto_pub -h emqtt.it10coop.com.ar -p 1883 -t cmnd/sensar/ppstest/tasmota-wemos-d1-mini/rule2 -m "on"
mosquitto_pub -h emqtt.it10coop.com.ar -p 1883 -t cmnd/sensar/ppstest/tasmota-wemos-d1-mini/ruletimer2 -m "30"
```

### Conectando un led a la WeMos d1 mini
Luego de conectar el led, ir a configuracion desde la WebUI, en el **D1 GPIO5** seleccionamos **Led(52)**.

Para enviar el comando por mqtt:

```
mosquitto_pub -h emqtt.it10coop.com.ar -p 1883 -t cmnd/sensar/ppstest/tasmota-wemos-d1-mini/LedPower -m '1'
```

El mensaje es "1" u "ON" para encender el LED, o "0" u "OFF" para apagarlo.

#### Encedido/apagado periodico de LED
Ahora probemos usar reglas para hacer un encedido/apagado periódico del LED:
1- Crear regla

```
mosquitto_pub -h emqtt.it10coop.com.ar -p 1883 -t cmnd/sensar/ppstest/tasmota-wemos-d1-mini/rule3 -m 'on rules#timer=1 do LedPower 1 endon on rules#timer=1 do backlog ruletimer1 3;LedPower 0 endon'
```

2- Activar regla

```
mosquitto_pub -h emqtt.it10coop.com.ar -p 1883 -t cmnd/sensar/ppstest/tasmota-wemos-d1-mini/rule3 -m 'on'
```

3- Lanzar timer

```
mosquitto_pub -h emqtt.it10coop.com.ar -p 1883 -t cmnd/sensar/ppstest/tasmota-wemos-d1-mini/ruletimer1 -m '3'
```

Para parar los performs timer, que sucederan de acuerdo a las veces que ponga en una regla:

```
"on rules#timer1 do [comando]"
```

Para borrar completamente la accion de un ruletimer se debe hacer con ingresando la linea anterior con los comandos que hagan falta.

Por MQTT esto se puede hacer, por ejemplo para desactivar el timer de esta prueba:

```
mosquitto_pub -h emqtt.it10coop.com.ar -p 1883 -t cmnd/sensar/ppstest/tasmota-wemos-d1-mini/ruletimer1 -m "LedPower 0"
```

O sino:

```
mosquitto_pub -h emqtt.it10coop.com.ar -p 1883 -t cmnd/sensar/ppstest/tasmota-wemos-d1-mini/backlog -m "ruletimer1 3;LedPower 0"
```


#### Modulando la intensidad de brillo de un LED con PWM
Teniendo conectado el LED, se debe cambiar el dispositivo que esta esta seteado para el pin **D1 GPIO5**, que actualmente es **Led (52)**. Ahora lo modificamos poniendo **PWM1 (37)**. De no hacer esto, todos los comandos utilizados para modificar parametros de PWM no funcionarán.

Una vez cambiado el dispositivo ya sea via web o por mqtt se procede a habilitar el uso de PWM. Con MQTT se hace:

```
mosquitto_pub -h emqtt.it10coop.com.ar -p 1883 -t cmnd/sensar/ppstest/tasmota-wemos-d1-mini/SetOption15 -m "0"
```

Luego se puede modificar el valor digital que sale por el pin. Este es un valor de 10 bits, entonces el rango va desde "0" a "1023". De todas formas este rango se puede modificar. Con MQTT es:

```
mosquitto_pub -h emqtt.it10coop.com.ar -p 1883 -t cmnd/sensar/ppstest/tasmota-wemos-d1-mini/PwmRange -m "[1-1023]"
```

Los valores posibles van de 255 a 1023. Modificar el rango maximo hace que los valores que vaya tomando se van mapeando al rango total de 0-1023. Esto quiere decir que si pongo como valor maximo 255, la intensidad del LED será máxima, como si fuera 1023, y si el valor que se le da es de 75, estara dentro del rango, y brillará con una intensidad media, como si estuviera seteado en un valor de 512 para el rango de 0-1023.

### Conectando sensores a Wemos D1 Mini
Es tan simple como ir a la configuración del modulo, y seleccionar **DHT11 (1)** en el pin **D4 GPIO2**.

![seleccionando pin dht11.png](Imagenes_anexos/tasmota/seleccionando_pin_dht11.png)

Guardamos y ya podemos verlo funcionando.

![principal_sensor_dht11.png](Imagenes_anexos/tasmota/principal_sensor_dht11.png)

## Conexion Serial con Edukit10
Edukit es un proyecto de hardware libre basado en Arduino, desarrollado por la Cooperativa IT10, para la enseñanza de robótica. Es una tecnología que permite acercar de una manera sencilla los fundamentos de la robótica y aquellos conceptos técnicos que se vuelven complejos para trabajarlos de la manera tradicional. Se compone de una serie de elementos electrónicos que trabajan en conjunto y permiten investigar y diseñar pequeños robots pedagógicos de forma sencilla, reciclando componentes. El software utilizado para la programación es libre y multiplataforma.

Se pretende lograr una conexión serial entre la Wemos D1 mini y la Edukit. Las configuraciones correspondientes se detallan a continuación.

### Configurando Wemos D1 mini

El ESP8266 tiene dos UARTS (puertos serie): UART0 en los pines 1 y 3 (TX0 y RX0 respectivamente), Y UART1 en los pines 2 y 8 (TX1 y RX1 respectivamente). Sin embargo, GPIO8 se utiliza para conectar el chip flash . Esto significa que UART1 solo puede transmitir datos.

UART0 también tiene control de flujo de hardware en los pines 15 y 13 (RTS0 y CTS0 respectivamente). Estos dos pines también pueden usarse como pines alternativos TX0 y RX0.

Se realiza una conexión serial por software, y para esto se utilizan pines digitales. En este caso GPIO5(D1) y GPIO4(D2) (SerBr Tx y SerBr Rx respectivamente).

![pines serial.png](Imagenes_anexos/tasmota/serial/pines_serial.png)

Solo es posible hacerlo a traves de web y luego guardando la configuracion como back up. https://github.com/arendst/Sonoff-Tasmota/issues/5068

### Configurando Edukit

De acuerdo al esquematico de Edukit, se usan los pines digitales 4 y 5, que se corresponden con los pines D10 y D9 respectivamente. Con estos pines, y usando la librería `SoftwareSerial.h` creamos un enlace serial por software entre la Wemos D1 mini y Edukit.

### Envío de comandos y mensajes por conexión serial
Tasmota no tiene integrada una funcionalidad que permita la recepción de comandos por serial. Esto es algo que nos es muy util, ya que al conectar sensores a la Edukit, esta podría enviar datos obtenidos, estado de los mismos, etc. Además que se sería provechoso poder modificar ciertos parámetros de Tasmota desde Edukit.

Dicho esto y viendo que el desarrollador del firmware no ha tenido en mente agregar esta característa se optó por modificar el codigo fuente agregándola. A continuación se especifica el proceso.

#### SSerialReceived
Tasmota muestra por consola los mensajes por software serial de la siguiente forma:

	MQT: tele/sensar/ppstest/tasmota-wemos-d1-/RESULT = {"SSerialReceived":"MENSAJE"}

Solamente se puede mostrar pero resulta imposible generar alguna accion por medio de la conexión serial.

#### MqttPublishSerial
De acuerdo a la librería **PubSubClient**, que es la utilizada por Tasmota para publicar y suscribirse, existe método llamado `publish`:
```Bash
boolean PubSubClient::publish(const char* topic, const char* payload) {
    return publish(topic,(const uint8_t*)payload,strlen(payload),false);
	}
```
Basicamente lo que hace es recibir como argumento un **topic** y el **payload** correspondiente para publicar. Valiendonos de esto, podriamos crear una funcion que de acuerdo a un mensaje serial se pueda parsear en dos partes, **topic;payload**, y publicar al broker.

En el archivo `sonoff/xdrv_02_mqtt.ino` estan todas las funciones que se utilizan para ejecutar el protocolo MQTT. Allí se especifica que utiliza la libreria `PubSubClient.h`. Se incluye en el codigo la siguiente funcion:

```Bash
//################ FUNCION PARA ENVIAR MENSAJES DEL SERIAL POR MQTT ##################
void MqttPublishSerial(const char* carga_serial)
{
  #define STRSIZE 80
  const char * serial = carga_serial;
  char topic[STRSIZE];
  char payload[STRSIZE];
  int i=0;
  int j=0;
  while (serial[i] != ';')
  {
      topic[i]=serial[i];
      i++;
  }
  topic[i]='\0';
  i++;
  while (serial[i] != '\0')
  {
      payload[j]=serial[i];
      i++;
      j++;
  }
  payload[j]='\0';

  MqttClient.publish(topic, payload);
  //MqttClient.publish("tele/sensar/ppstest/tasmota-wemos-d1-",serial);
  Serial.printf("Mensaje enviado!, %s , %s",topic, payload);
}
//#######################################################################################
```

Esa una función que no devuelve ningun valor. Recibe como argumento el mensaje serial que llega desde Edukit a traves del Bridge Serial de Tasmota. La idea aqui es parsear el String que llega y asignarlo a 2 nuevos strings como topic y payload.

El string `carga_serial` tiene un delimitador que es un punto y coma (;). Este marca el fin del topic y el comienzo del payload correspondiente. Se recorre el string `serial` con un *while* hasta que encuentra ese delimitador, a medida que va guardando caracter por caracter en **topic**. Luego se agrega un **NULL** al final de la cadena. Luego se procede a hacer lo mismo con **payload**. Finalmente se utiliza el metodo `publish` antes descripto sobre el objeto **MqttClient**, inicializado anteriormente en el codigo. Además se envia un mensaje por el Hardware Serial de Tasmota, indicando el mensaje que se ha enviado.

#### MqttPublishTeleSerial
Esta función se crea con el objeto de que cuando se recibe un comando o un mensaje para publicarse con MQTT que proviene del Software Serial este pueda ser visualizado en la consola de Tasmota. Tambien se declara en `sonoff/xdrv_02_mqtt.ino`. A continuación describimos su codigo:

```bash
//################ FUNCION PARA ENVIAR MENSAJES A TELE POR MQTT CUANDO RECIBO SERIAL ##################
void MqttPublishTeleSerial(uint8_t prefix, const char* subtopic, boolean retained)
{
/* prefix 0 = cmnd using subtopic
 * prefix 1 = stat using subtopic
 * prefix 2 = tele using subtopic
 * prefix 4 = cmnd using subtopic or RESULT
 * prefix 5 = stat using subtopic or RESULT
 * prefix 6 = tele using subtopic or RESULT
 */
  char romram[33];
  char stopic[TOPSZ];

  snprintf_P(romram, sizeof(romram), ((prefix > 3) && !Settings.flag.mqtt_response) ? S_RSLT_RESULT : subtopic);
  for (byte i = 0; i < strlen(romram); i++) {
    romram[i] = toupper(romram[i]);
  }
  prefix &= 3;
  GetTopic_P(stopic, prefix, mqtt_topic, romram);

  char mensaje[] = "Mensaje enviado!: ";

  char combined[100] = {0};

  strcat(combined, mensaje);
  strcat(combined, stopic);

  MqttPublish(combined, false);
}
//######################################################################################
```
Es muy parecida a la función `void MqttPublishPrefixTopic_P(uint8_t prefix, const char* subtopic, boolean retained)`. Lo que se agrega efectivamente es:

```bash
  char mensaje[] = "Mensaje enviado!: ";

  char combined[100] = {0};

  strcat(combined, mensaje);
  strcat(combined, stopic);

  MqttPublish(combined, false);
```

Una leyenda que aparezca inicialmente indicando el mensaje enviado. Se combina con la cadena que determina el topic a donde se publica para visualizar mensajes por consola. Se basa en uno de los 3 prefijos utilizados en Tasmota, **tele**, **cmnd** y **stat**. En este caso se usa tele, generalmente utilizado para enviar mensaje periodicos de estado de sensores, información y estado de la placa, características activadas, etc.

### Proceso de ejecución

En el archivo `sonoff/xdrv_08_serial_bridge.ino`, bajo la función `void SerialBridgeInput(void)`, que es la que declara que hacer con las entradas por serial, es donde modificamos para utilizar las funciones creadas anteriormente. A continuación se describe una parte del codigo:

```Bash
[...]
  if (serial_bridge_in_byte_counter && (millis() > (serial_bridge_polling_window + SERIAL_POLLING))) {
    serial_bridge_buffer[serial_bridge_in_byte_counter] = 0;  // serial data completed
    snprintf_P(mqtt_data, sizeof(mqtt_data), PSTR("{\"" D_JSON_SSERIALRECEIVED "\":\"%s\"}"), serial_bridge_buffer);
    MqttPublishPrefixTopic_P(RESULT_OR_TELE, PSTR(D_JSON_SSERIALRECEIVED));
//    XdrvRulesProcess();

//####################### RECEPCION Y PUBLICACION MQTT DE MENSAJES POR SERIAL ###########################################

    if (serial_bridge_buffer != 0){
      MqttPublishSerial(serial_bridge_buffer);
      snprintf_P(mqtt_data, sizeof(mqtt_data), PSTR("%s"), serial_bridge_buffer);
      MqttPublishTeleSerial(RESULT_OR_TELE, PSTR(D_JSON_SSERIALRECEIVED), false);
    }

//########################################################################################################################
    serial_bridge_in_byte_counter = 0;
  }
```
Cuando ya esta disponible el Serial Bridge, si hay datos que estan llenando un buffer de serial se ejecutan las funciones correspondientes para indicar por consola que es lo que se envio. Debajo de eso, de manera secuencial cargamos nuestras funciones.

```Bash
//####################### RECEPCION Y PUBLICACION MQTT DE MENSAJES POR SERIAL ###########################################

    if (serial_bridge_buffer != 0){
      MqttPublishSerial(serial_bridge_buffer);
      snprintf_P(mqtt_data, sizeof(mqtt_data), PSTR("%s"), serial_bridge_buffer);
      MqttPublishTeleSerial(RESULT_OR_TELE, PSTR(D_JSON_SSERIALRECEIVED), false);
    }

//########################################################################################################################
```
Si el buffer se esta llenando, ejecutamos `MqttPublishSerial` con lo que haya en ese buffer. Luego utilizamos `MqttPublishTeleSerial` al igual que `MqttPublishPrefixTopic_P`, pero en este caso para indicar por consola que ha sido enviando un mensaje serial al broker MQTT.

### Probando las nuevas funciones
#### Enviar dato de temperatura
Se intentará enviar datos de temperatura periodicamente por serial y publicando al broker en un topic distinto al que usa Tasmota. El dato es tomado por la Edukit, que dispone de un sensor **LM35CZ**.

##### Sketch Arduino de Edukit

```Bash
	#include <SoftwareSerial.h>

	SoftwareSerial mySerial(10, 9); // RX, TX

	int lecturaADC = 0;
	double voltajeLM35 = 0.0;
	double TemperaturaLM35 = 0.0;
	unsigned long tiempo1 = 0;
	unsigned long tiempo2 = 0;

	void setup() {
	  // Open serial communications and wait for port to open:
	  Serial.begin(9600);
	  while (!Serial) {
		; // wait for serial port to connect. Needed for native USB port only
	  }
	  Serial.println("Inicio!");
	  // set the data rate for the SoftwareSerial port
	  mySerial.begin(9600);

	  tiempo1=millis();

	}

	void loop() {

	  //#1 Lectura del canal 0 del ADC, recordando que el ADC de Arduino es de 10 Bits, el resultado se guardara en una variable int (16 bits).
	  lecturaADC = analogRead(A6);

	  /*
	   * Convertir el valor binario a un voltaje que se guardara en una variable del programa, dado que la variable lecturaADC es entera, la operación lecturaADC/1023 regresará un valor entero.

	   tiempo2=millis();
	   if(tiempo2 > (tiempo1+(10000))){
		tiempo1 = millis(); //Actualiza el tiempo actual

		//Opcion (b)
		voltajeLM35 = ((double)lecturaADC/1023)*5;

		TemperaturaLM35 = voltajeLM35/0.01;
		Serial.print("El valor en temperatura de salida en el sensor es: ");
		Serial.print(TemperaturaLM35,2); //Imprime la variable double con 2 decimales
		Serial.println(" °C");

		char temp[5];
		dtostrf(TemperaturaLM35, 5, 2, temp);
		//(variable float, ancho, cantidad de decimales, variable char)

		String leyenda= "El valor de temperatura es: ";
		String grados= " °C";

		String mensaje = leyenda + temp + grados;
		String topic = "sensar/ppstest/;";

		pub_mqtt(topic, mensaje);
	   }

	  if (mySerial.available()) {
		Serial.write(mySerial.read());
	  }
	  if (Serial.available()) {
		;
	  }
	}
	//#########################----------------FUNCIONES--------------------########################

	void pub_mqtt(String topic, String payload) //que sea recurrente
	{
	  String a;
	  a = topic + payload;
	  Serial.println(a);
	  mySerial.println(a);
	}
```

# Uso de memoria de Tasmota
Segun Visual Studio Code:

	DATA:    [======    ]  58.9% (used 48268 bytes from 81920 bytes)
	PROGRAM: [=====     ]  51.7% (used 529464 bytes from 1023984 bytes)

Segun Tasmota:

	ESP Chip Id	7934606
	Flash Chip Id	0x164020
	Flash Size	4096kB
	Program Flash Size	1024kB
	Program Size	521kB
	Free Program Space	480kB
	Free Memory	27kB

