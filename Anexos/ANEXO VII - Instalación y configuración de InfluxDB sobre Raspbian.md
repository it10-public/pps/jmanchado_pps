# Anexo VII

## InfluxDB

Se instala con el comando: `sudo apt-get install influxdb`. No es necesario configurar nada previamente, ya que las nuevas versiones están listas para entrar como administrador.

Para poder acceder, utilizar y modificar la base de datos se requiere un cliente. Para esto lo instalamos con `sudo apt-get install influxdb-client`.

Para iniciar la base de datos no es necesario hacerlo a través del servicio, es decir `sudo service influxdb start`. Solo hace falta iniciarlo así:

```Bash
sudo influxd run --config /etc/influxdb/influxdb.conf &
```

Lo corremos en segundo plano para poder utilizar el cliente en la misma terminal, sobre todo si pretendemos usar una sola consola con ssh.

También podríamos hacer las consultas o lo que sea necesario a través de un cliente web

![cliente web](Imagenes_anexos/influxdb/cliente_web.png)

### Configuraciones previas

Para acceder al cliente por el terminal, solo ingresamos `influx` en la misma. Puede que sea necesario aclarar en algún caso al host al que se conecta, esto se hace: `influx -host IP`. Esa IP es la local, o en todo caso la del equipo que maneje la base de datos.

Primero debemos crear un usuario como el de Raspbian para la base de datos:

```Bash
CREATE USER "pi" WITH PASSWORD 'it10' WITH ALL PRIVILEGES
```
