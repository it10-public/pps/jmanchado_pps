# Despliegue IOT para para plataforma Educativa Edukit10

## Indice

1. [Anexo I - Pinout Multiconector Edukit10 y Esquema](/docs/1_ANEXO_I-Pinout_ Multiconector_Edukit10_Esquematico.md)
2. [Anexo II - Pinout Wemos ](/docs/2_ANEXO_ II-pinout_wemos.md)
3. [Anexo III - Tasmota ](/docs/3_ANEXO_III-Tasmota.md)
4. [Anexo IV - ESPurna](/docs/4_ANEXO_ IV-ESPurna.md)
5. [Anexo V - Raspbian](/docs/5_ANEXO_V-Raspbian.md)
6. (ver)
7. [Anexo VII - InfluxDB](/docs/7_ANEXO_VII-InfluxDB_sobre_raspbian.md)
8. [Anexo VIII - Grafana](/docs/8_ANEXO_VIII-Grafana_sobre_raspbian.md)
9. [Anexo IX- Node RED](/docs/9_ANEXO_IX-Node-RED_sobre_Raspbian.md)
10. [Anexo X - Integración NodeRED, InfluxDB y Grafana](/docs/10_ANEXO_ X-Integracion_Nodered_influx_ y_ grafana.md)
11. [Anexo XI](/docs/11_ANEXO_XI-Documentacion_proyecto_IoT_Invernadero_Inteligente)
