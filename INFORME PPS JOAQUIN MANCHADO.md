# INFORME PPS JOAQUIN MANCHADO

- [Resumen](#Resumen)

- [Introducción](#Introducción)

- [Descripción de la empresa](#descripción-de-la-empresa)

- [Desarrollo](#Desarrollo)
  - [Búsqueda, revisión y selección de Tecnologías](#búsqueda-revisión-y-selección-de-tecnologías)
    - [Edukit](#Edukit)
    - [Interfaz de comunicación inalámbrica 802.11](#interfaz-de-comunicación-inalámbrica-80211)
      - [Firmware](#firmware)
        - [Tasmota](#tasmota)
        - [ESPurna](#espurna)
        - [ESPEasy](#espeasy)
        - [Funcionalidades en común](#funcionalidades-en-común)
        - [Áreas comunes de mejora](#áreas-comunes-de-mejora)
        - [Comparación y decisión](#comparación-y-decisión)
    - [Protocolo de mensajería](#protocolo-de-mensajería)
      - [MQTT](#mqtt)
      - [Comparativa](#comparativa)
    - [Gateway / server](#gateway--server)
      - [Raspberry Pi B+](#raspberry-pi-b)
  - [Selección y prueba de sensores](#selección-y-prueba-de-sensores)
    - [Sensor de intensidad lumínica LDR](#sensor-de-intensidad-lumínica-ldr)
    - [Sensor de temperatura LM35](#sensor-de-temperatura-lm35)
    - [Sensor de gases MQ-9](#sensor-de-gases-mq-9)
    - [Sensor de humedad de suelos YL-69](#sensor-de-humedad-de-suelos-yl-69)
    - [PCB Shield](#pcb-shield)
  - [Servidor con base de datos y visualización](#servidor-con-base-de-datos-y-visualización)
    - [Base de datos](#base-de-datos)
      - [InfluxDB](#influxdb)
      - [Criterio de selección](#criterio-de-selección)
    - [Visualización](#visualización)
      - [Grafana](#grafana)
      - [Criterio de selección](#criterio-de-selección-1)
    - [Plataforma](#plataforma)
      - [Criterio de selección](#criterio-de-selección-2)
  - [Lenguajes de programación utilizados](#lenguajes-de-programación-utilizados)
    - [C++](#c)
    - [JavaScript](#javascript)
    - [HTML](#html)
  - [Pruebas de integración](#pruebas-de-integración)
    - [Esquema general y funcionamiento del prototipo](#esquema-general-y-funcionamiento-del-prototipo)
      - [Edukit y módulo IoT](#edukit-y-módulo-iot)
      - [Gateway / Server](#gateway--server-1)
      - [Visualización](#visualización-1)
      - [Resultado Final](#resultado-final)
        - [Pestaña valores instantáneos - *Invernadero*](#pestaña-valores-instantáneos---invernadero)
        - [Pestaña valores históricos - *Registros*](#pestaña-valores-históricos---registros)
          - [Intensidad de luz](#intensidad-de-luz)
          - [Humedad en suelo](#humedad-en-suelo)
          - [Concentración de monóxido de carbono](#concentración-de-monóxido-de-carbono)
          - [Temperatura ambiente](#temperatura-ambiente)
- [Conclusiones](#conclusiones)
- [Referencias y bibliografía](#referencias-y-bibliografía )
- [Anexos](#anexos)







# Resumen


El presente informe corresponde a la Práctica Profesional Supervisada (PPS) realizada en la *Cooperativa de Trabajo Informática y Telecomunicaciones 10 Ltda.* ubicada en calle Belgrano 45 de nuestra localidad. Partiendo de dos conceptos como lo es IoT (Internet of Things) y la educación en programación, surge la idea que moviliza esta PPS. El objetivo general de la misma se enfoca en diseñar e implementar una arquitectura de software en el marco de Internet de las Cosas, utilizando la plataforma educativa Edukit10, la cual está orientada a la enseñanza de la programación de sistemas embebidos facilitando el acercamiento de niños y jóvenes a las nuevas disciplinas técnicas. El hardware está desarrollado por la cooperativa, basada en Arduino. Se busca integrar una interfaz de comunicación inalámbrica que permita enviar datos de sensores a un servidor utilizando protocolos orientados a mensajes. Dichos datos se  almacenarán en una base de datos para luego recuperarlos, procesarlos y visualizarlos a través de una aplicación web.

**Palabras claves:** Internet de las Cosas, Educación, WiFi, servidor, Raspberry Pi, Node-RED, InfluxDB, Grafana, invernadero, aplicación web


# Introducción


El **internet de las cosas** (en inglés, Internet of Things, abreviado IoT) es un concepto que se refiere a una interconexión digital de objetos cotidianos con internet. Este concepto fue usado por primera vez por Kevin Ashton (creador de estándares de tecnologías como RFID).

Dar una definición de IoT no es algo sencillo. Desde un punto de vista tecnológico, The Global Standards Initiative on Internet of Things (IoT-GSI), de ITU, define IoT como:
*“Una infraestructura global en el marco de la sociedad de la información que provee servicios a través de la conexión de elementos físicos o virtuales, basada en tecnologías de la información y la comunicación, tanto existentes como en desarrollo”.*


Hoy por hoy, el término IoT se usa con una denotación de conexión avanzada de dispositivos, sistemas y servicios que va más allá del tradicional M2M (máquina a máquina) y abarca una amplia variedad de protocolos, dominios y aplicaciones. Es un tema emergente de importancia técnica, social y económica, y es uno de los pilares del “Internet del Futuro” junto a los conceptos involucrados a Internet de las Personas, Internet de los Contenidos y el Conocimiento e Internet de los Servicios.


En este momento se están combinando productos de consumo, bienes duraderos, automóviles, componentes industriales, componentes de servicios públicos, sensores y otros objetos de uso cotidiano con conectividad a Internet con potentes capacidades de análisis de datos que prometen transformar el modo en que trabajamos, vivimos, jugamos, enseñamos y aprendemos.


Dentro de los objetivos propuestos y alcanzados se encuentran la implementación de una arquitectura de software en la nube que permita almacenar, procesar y visualizar datos. Estos datos serán enviados agregando una interfaz de comunicación WiFi 802.11, con una placa WeMos D1 mini basada en el SoC ESP2866 a la placa Edukit10 desarrollada por la cooperativa, la cual tiene sensores integrados y la posibilidad de agregar otros externos. Para esto se seleccionó y utilizó el protocolo de comunicación orientados a mensajes MQTT. También se analizó y seleccionó el software necesario para instalar en el servidor en una Raspberry Pi, como ser base de datos de serie de tiempo, con InfluxDB; una herramienta que sirva para análisis y visualización de datos en gráficas, con Grafana. Por ultimo se desarrolló una aplicación web que se encargue de procesar y presentar la información, todo gestionado a través de la plataforma Node-RED. Los lenguajes utilizados para la programación de cada herramienta y servicio son JavaScript, C++, HTML . Se usaron metodologías ágiles (scrum) para el desarrollo y se documentó todo el proceso para respaldo de la cooperativa.  


Se realizó una integración y puesta en marcha de lo antes mencionado en un prototipo de invernadero inteligente a modo de ejemplo, liberando la documentación para la implementación y posterior uso de la comunidad. <!--Poner algo de la licencia del proyecto -->


# Descripción de la Empresa


- Nombre: Cooperativa de Trabajo Informática y Telecomunicaciones 10 Ltda.
- Dirección:  Belgrano 45
- Tel./Fax: 3584823284
- E-mail: contacto@it10coop.com.ar
- Área de la empresa donde se desarrolla la práctica: IoT
- Permanencia: 2 de enero de 2019 al 29 de marzo de 2019. 
- Jornada laboral: 5hs diarias de lunes a viernes. 6hs diarias solo los dias 2 al 8 de enero.
- Descripción: IT10 es una empresa cooperativa de Informática y Telecomunicaciones integrada por jóvenes profesionales de diversas áreas (ingeniería, informática y la comunicación) que brindan soluciones tecnológicas innovadoras de manera flexible y eficaz. Trabajan con tecnologías adaptables a cada demanda para el desarrollo de productos y servicios (software y hardware), eventos tecnológicos, capacitaciones y consultoría a profesionales, empresas e instituciones locales y regionales. En lo últimos años, la organización se ha perfilado hacia la investigación y el desarrollo de productos y servicios principalmente en el campo de la tecnología educativa y el Internet de las Cosas (IOT), siempre apostando por “la innovación con sentido social”, desde la autogestión y el trabajo en red con otros actores para potenciar juntos un ecosistema productivo en los nuevos escenarios de complejidades y entramados.
- Productos y Servicios:

  - Aplicaciones Android

    - Link: http://play.google.com/store/apps/developer?id=Cooperativa+de+Trabajo+IT10+Ltda
  - Sistemas web
  - Páginas Web
  - IoT – Internet de las cosas
  - Impresión 3d
  - Robótica Educativa

    - Edukit10
- Organigrama: Al ser una cooperativa de trabajo no dispone precisamente de un organigrama. La jerarquía dentro de la empresa es horizontal, siendo todos socios los trabajadores de la misma.


# Desarrollo


# Búsqueda, revisión y selección de Tecnologías


Se determinó incorporar una interface de comunicación inalámbrica 802.11 para integrar a la Edukit, de manera que el módulo provea facilidades en cuanto a portabilidad. Este es el punto de partida y primer condicionante para la selección del resto del hardware, software y herramientas a utilizar.


Para determinar los protocolos y selección de hardware se describe el proceso en las siguientes etapas:


- Edukit
  - Selección interfaz de comunicación 
    - Hardware 
    - Firmware
- Protocolo de mensajería
- Gateway / server 
  - Hardware
  - Software


Se detalla a continuación cada etapa.


## Edukit


Es un proyecto de hardware libre basado en Arduino, desarrollado por la Cooperativa IT10, para la enseñanza de robótica. Es una tecnología que permite acercar de una manera sencilla los fundamentos de la robótica y aquellos conceptos técnicos que se vuelven complejos para trabajarlos de la manera tradicional. Se compone de una serie de elementos electrónicos que trabajan en conjunto y permiten investigar y diseñar pequeños robots pedagógicos de forma sencilla, reciclando componentes. El software utilizado para la programación es libre y multiplataforma.

![Imagen Edukit10](Imagenes/edukit10.jpg)


El objetivo es adicionar un módulo con comunicación inalámbrica que se vincula´ra con la Edukit a través de una conexión serial por software. No se toma en cuenta la conexión serial por hardware porque se pretende utilizarla para monitorear el funcionamiento del dispositivo. Además se requiere que el nuevo módulo sea de bajo costo y funcione con gran estabilidad por muchas horas.


En función a todo esto se elije la interfaz de comunicación. En el Anexo I se incluye el diagrama pinout y esquemático de la Edukit10.

## Interfaz de comunicación inalámbrica 802.11

El nodo es el vínculo entre la Edukit y el servidor que procesa los datos. De un gran abanico de posibilidades, la cooperativa tiene a disposición alternativas que en común poseen el microchip **ESP8266**. La razón de ello es que son económicos, estables, de bajo consumo, poseen varios buses de comunicación, su procesador es sumamente rápido (en términos relativos), entre otras cosas. El elegido es el **WeMos D1 mini**, que cumple con los criterios antes mencionados.

![WeMos D1 mini](Imagenes/wemos.png)

En el Anexo II se encuentra el esquemático y características de la WeMos D1 mini. En el Anexo XII está la hoja de datos del microchip ESP8266.

Sin embargo presenta algunos problemas a la hora de su uso, más precisamente por el firmware que trae de fábrica (un firmware no es más que el software de bajo nivel, la lógica que controla los circuitos electrónicos). Los inconvenientes que se presentan son:


- Firmware AT (Ai-Thinker) muy limitado. Malas referencias de la comunidad.
- Genera fallos en la comunicación con **SoftwareSerial**. Indispensable para el proyecto. No garantiza estabilidad.
- Comandos AT complicados, que muchas veces fallan y se desperdicia tiempo en la depuración de las fallas.
- Se centra totalmente en los aspectos de conexión y desconexión, desaprovechando el potencial de la placa, como por ejemplo el uso de los pines GPIO, buses de comunicación, etc. 

Es por todo esto, y gracias a experiencias previas de los integrantes de la cooperativa que se indica como primera instancia investigar sobre alternativas a este firmware. 


### Firmware


Indagando en la web, se da con tres firmware interesantes para analizar, gracias al trabajo de **Lazar Obradovic**, usuario de GitHub, aficionado al hacking y al mundo IoT. Estos firmware son:


- Tasmota
- ESPurna
- ESPEasy


Se detalla a continuación cada uno.


#### Tasmota


Mas bien llamado Sonoff-Tasmota, en representación de Theo-Arends-Sonoff-MQTT-OTA, se produjo como una evolución de proyectos de retoques anteriores de Theo Arends.


En un principio se creó para reemplazar el firmware original de los primeros dispositivos Iteads Sonoff, pero creció hasta convertirse en un proyecto que admite muchos dispositivos basados en ESP8266.


El proyecto se actualiza casi a diario, exclusivamente por el propio Theo, revisando algunas de las contribuciones de la comunidad de desarrolladores.

La comunidad es muy activa y tiene una combinación saludable de desarrolladores y usuarios "regulares" que aseguran que el firmware sea generalmente utilizable. 

Se adjunta información, datos de configuración y pruebas realizadas con Tasmota en WeMos D1 mini en el Anexo III.


#### ESPurna


Creado por Xose Pérez en 2016, como un proyecto para proporcionar un firmware independiente del dispositivo para las placas ESP8266. Comenzó con la placa WeMos D1 Mini (siendo muy genérico) y agregó soporte para muchas otras placas fabricadas, incluyendo Sonoff.


Falta una mayor adopción social, a pesar del blog muy informativo que dirige Xose. El proyecto se actualiza pocas veces, exclusivamente por su autor, en base a las discusiones o pequeñas contribuciones de la comunidad.

La comunidad es más pequeña que la de Tasmota y está principalmente orientada a desarrolladores con el ingenio suficiente para que coincida con el conocimiento y las habilidades de Xose. 

Se adjunta información, datos de configuración y pruebas realizadas con ESPurna en WeMos D1 mini en el Anexo IV.


#### ESPEasy


ESPEASY es el firmware alternativo más antiguo que existe. Se creó en diciembre de 2015 por un equipo holandés llamado “Lets Control It”. Muy flexible y capaz de soportar cualquier combinación de sensores / actuadores, pero requiere un poco más de configuración para comenzar.


El proyecto se actualiza muchas veces, en su mayoría por pocos desarrolladores centrales de Lets Control It, pero también combinan el código de otros colaboradores.


El tamaño de la comunidad es comparable al de Tasmota, con un balance un poco mejor entre los desarrolladores y los usuarios "regulares".


#### Funcionalidades en común


- Son de código abierto.
- Compatibilidad con casi todas las tarjetas y módulos de Itead Sonoff, además de las placas genéricas como NodeMCU o WeMos D1.
- Amplio soporte para muchos sensores y actuadores adicionales.
- Todos admiten varios modos de conexión WiFi, incluido el modo AP (hotspot) o STA (cliente), con múltiples configuraciones de SSID y escaneo de red Wifi.
- Compatibilidad con MQTT, tanto para sensores / actuadores, como para la configuración propia. Algunos incluso ofrecen un diseño de *topic* específico.
- Soportan actualización por aire -Over-The-Air- (OTA), copia de seguridad y restauración de la configuración.
- Todos soportan sincronización de tiempo vía NTP.


#### Áreas comunes de mejora


Todas las opciones de firmware tienen el mismo conjunto común de problemas y áreas potenciales de mejora:

**Comunicación segura / TLS:** Ninguno lo soporta. Tasmota y ESPurna empezaron a admitir TLS para MQTT, pero eso aborda solo una parte del problema y deja la interfaz de usuario web y otras interfaces sin cifrar y/o autenticar.


**Documentación:** Ninguno tiene una documentación bien estructurada. Tasmota es mejor que los otros, pero podría ser mucho mejor y resolver cuestiones más simples con una buena documentación.

**IPv6:** Todas se basan en IPv4, ninguna soporta IPv6. A futuro será un inconveniente. IPv6 es más un problema de la estructura Arduino / ESP8266 que un problema de firmware individual, pero sigue siendo una cuestión a resolver.


#### Comparación y decisión


Tasmota es "lo suficientemente buena" para el uso diario. Más fácil de utilizar por programadores no expertos. Permite un inicio fácil y resultados rápidos, tiene un poderoso conjunto de características y buen soporte para diferentes sensores, pero también viene con algunas cosas extrañas a las que solo hay que acostumbrarse (como sintaxis de comandos, ejecuciones de reglas, entre otros). 


ESPurna es muy estructurada y ofrece una interfaz limpia y precisa, pero aún no se popularizó demasiado. Aún así, parece que tiene una mejor implementación interna del cliente MQTT y servidor HTTP. ESPurna podría carecer de algunas de las características de lujo que tiene Tasmota.


ESPEasy es demasiado desestructurado para la producción en masa (o uso diario). Si uno toma como algo positivo la libertad y capacidades de configuración, es una buena opción. Tiene opciones de configuración de run-time (tiempo de ejecución) muy abiertas y potentes. Está más avanzado en lo que respecta a la toma de decisiones locales, lo que reduce potencialmente la necesidad de un agente MQTT y una lógica de automatización externa.


Se descarta de entrada ESPEasy, a pesar de sus bondades, ya que su configuración es en tiempo de ejecución, algo inviable para el proyecto. En detalle la comparación entre Tasmota y Espurna es la siguiente:


|                        | Tasmota                                                      | ESPurna                                                      |
| ---------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Comunidad              | Bastante grande. Aportes de muchos usuarios y colaboradores. | Chica, va creciendo poco a poco, pero está muy lejos de Tasmota. |
| Actualizaciones        | Mucho más frecuentes, debido a su extendida comunidad.       | Pocas, depende casi exclusivamente de su desarrollador, el cual no dedica su tiempo 100% al mejoramiento del firmware. |
| Protocolos             | MQTT, HTTP                                                   | MQTT, HTTP                                                   |
| Base de datos          | No hay integración                                           | Integración directa con InfluxDB                             |
| Encriptación/Seguridad | No                                                           | No                                                           |
| Arduino Nano           | La vinculación se puede hacer por medio de comunicación serial por software | No hay indicios de poder usar comunicación serial aparte de la de hardware |

Para tomar una decisión se realizaron diversas pruebas, detalladas en anexos. 

Se concluye en que **Tasmota** es el firmware mas competente para suplantar al de fabrica de la placa WeMos D1 mini., sobre todo por su gran comunidad y posibilidad de comunicación serial por software. 


## Protocolo de mensajería


Si bien existen muchos protocolos de comunicación orientados a mensajes, como XMPP. CoAP, AMQP, etc, de acuerdo a la elección del firmware no queda mas opción que optar por MQTT. 


### MQTT


Message Queue Telemetry Transport es un protocolo de mensajería basado en publicación-suscripción. Se fundamenta en el principio cliente/servidor. Destaca por ser ligero y sencillo de implementar.  Funciona sobre el modelo TCP/IP. 


Andy Stanford-Clark de IBM y Arlen Nipper de Cirrus Link fueron los autores de la primera versión del protocolo en 1999. 


Los principios de diseño son minimizar el ancho de banda de la red y los requisitos de recursos del dispositivo, al mismo tiempo que intentan garantizar la confiabilidad y cierto grado de seguridad de la entrega.


La arquitectura de MQTT sigue una topología de estrella, con un nodo central como "broker", que es el que recopila los datos que los publishers (los objetos comunicantes) le transmiten. Un broker tiene una capacidad de hasta 10000 clientes.

![mqtt-diagram@2x](Imagenes/mqtt-diagram@2x.png)


La comunicación se basa en canales llamados cada uno como *topic* (tema) y puede ser de uno a uno, o de uno a muchos. Los **publishers** envían mensajes al topic y los **subscribers** pueden leerlos. Los mensajes enviados por los objetos comunicantes pueden ser de todo tipo pero no pueden superar los 256 Mb.


Un "topic" se representa mediante una cadena y tiene una estructura jerárquica. Cada jerarquía se separa con '/'. Por ejemplo, `edificio1/planta5/sala1/raspberry2/temperatura` o `/edificio3/planta0/sala3/arduino4/ruido`.


De esta forma un nodo puede subscribirse a un "topic" concreto ("edificio1/planta2/sala0/arduino0/temperatura")  o  a varios ("edificio1/planta2/#").


### Comparativa


Haciendo una comparación con otros protocolos de mensajería existentes, se puede ver que tiene muy buenas prestaciones a fines de esta practica.


![Comparacion con otros protocolos](https://www.14core.com/wp-content/uploads/2017/01/rowebots140313.png)


## Gateway / server


Teniendo en cuenta el criterio de portabilidad se decide un servidor que funcione de Gateway también usando una computadora de tamaño reducido. 


La opción mas interesante es utilizar una Raspberry Pi, que además de ser de bajo costo y tener potencia adecuada para los propósitos de esta practica, ya la tenían a disposición en la cooperativa.


### Raspberry Pi B+

**Raspberry Pi** es un ordenador de placa reducida, u ordenador de placa simple (SBC) de bajo coste desarrollado en el Reino Unido por la Fundación Raspberry Pi, con el objetivo de estimular la enseñanza de informática en las escuelas.

![Raspberry Pi b+](https://images-na.ssl-images-amazon.com/images/I/91zSu44%2B34L._SX425_.jpg)


Aunque no se indica expresamente si es hardware libre es un producto con propiedad registrada, manteniendo el control de la plataforma, pero permitiendo su uso libre tanto a nivel educativo como particular. 


El software sí es de código abierto, siendo su sistema operativo oficial una versión adaptada de Debian, denominada *Raspbian*, y es el utilizado en esta práctica, aunque permite usar otros. En todas sus versiones incluye un procesador Broadcom, una memoria RAM, una GPU, puertos USB, HDMI, Ethernet, 40  pines GPIO y un conector para cámara. Ninguna de sus ediciones incluye memoria.


En el Anexo XII se encuentra la hoja de datos de la Raspberry Pi 3B+. En el Anexo V se encuentra el proceso de instalación y configuración del sistema operativo Raspbian.


En esta mini PC se debe alojar una plataforma que pueda vincular una base de datos y un servicio que permita la visualización e interpretación de información de sensores de la Edukit.


# Selección y prueba de sensores


Los sensores se seleccionaron con la intención de poder crear un escenario de prueba de un invernadero inteligente. Además los mismos ya los tenían a disposición en la cooperativa. 


Son cuatro sensores en total. Estos son:


- Sensor de intensidad lumínica
- Sensor de temperatura
- Sensor de gases
- Sensor de humedad de suelos

![fotos sensores](Imagenes/fotos_sensores.png)


Los dos primeros están incluidos en la Edukit. Los últimos se escogieron de una variedad disponible en inventario de la cooperativa. No se repara demasiado en la calibración de cada sensor (será tarea posterior de la cooperativa), sino más bien en la correcta conversión de sus valores analógicos a digitales. 


Se detallan brevemente a continuación. En el Anexo XII se encuentran las hojas de datos de cada uno.

## Sensor de intensidad lumínica LDR

Un fotorresistor o fotorresistencia es un componente electrónico cuya resistencia disminuye con el aumento de intensidad de luz incidente. Sus siglas, **LDR**, se originan de su nombre en inglés *light-dependent resistor*. Su cuerpo está formado por una célula fotorreceptora y dos patillas. Este sensor viene integrado a la Edukit.


## Sensor de temperatura LM35 

El LM35 es un sensor de temperatura con una precisión calibrada de 1 °C. Su rango de medición abarca desde -55 °C hasta 150 °C. La salida es lineal y cada grado Celsius equivale a 10 mV, por lo tanto:

- 150 °C = 1500 mV
- -55 °C = -550 mV


Opera de 4v a 30v. Viene integrado en la Edukit. 

## Sensor de gases MQ-9

Este sensor de gas puede detectar monóxido de carbono (CO) y también gases inflamables, como Gas Natural y Butano. Tiene alta sensibilidad (ajustable mediante potenciómetro) y un tiempo de respuesta rápido. El monóxido de carbono se detecta a temperatura de calentamiento baja (1.4V) mediante ciclos de temperatura elevada y baja. La conductividad eléctrica incrementa con la concentración de monóxido de carbono en el aire. El sensor tiene 6 pines, 4 para la medición de la señal y 2 para el calentador, sin embargo el módulo dispone directamente de un pin analógico, otro digital, además de alimentación y masa.


## Sensor de humedad de suelos YL-69

Este sensor puede medir la cantidad de humedad presente en el suelo que lo rodea empleando dos electrodos que pasan corriente a través del suelo, y lee la resistencia. Mayor presencia de agua hace que la tierra conduzca electricidad más fácil (Menor resistencia), mientras que un suelo seco es un conductor pobre de la electricidad (Mayor resistencia).


## PCB Shield


Una **“placa de** **circuito impreso”** (del inglés: *Printed Circuit Board*, PCB), es una superficie constituida por caminos, pistas o buses de material conductor laminadas sobre una base no conductora. El circuito impreso se utiliza para conectar eléctricamente a través de las pistas conductoras, y sostener mecánicamente, por medio de la base, un conjunto de componentes electrónicos. Las pistas son generalmente de cobre, mientras que la base se fabrica generalmente de resinas de fibra de vidrio reforzada, cerámica, plástico, teflón o polímeros como la baquelita.   


La producción de la PCB se realizo sobre una placa de simple capa de cobre, y se diseñó usando el Software online EasyEDA.

**EasyEDA** es un software alojado en la nube, disponible a través de una página web, totalmente gratis. Permite diseñar tanto diagramas electrónicos como PCB y simulación de circuitos. Cuenta con una enorme librería de componentes, incluyendo las placas de Arduino, sensores y Raspberry. Está disponible a través de la página web <https://easyeda.com/>.

El diseño del PCB es el siguiente:

![diseño PCB 2](Imagenes/diseño_PCB_2.png)

El resultado final es:

![pcb real 1](Imagenes/pcb_real_11.png)

De este lado va montada la WeMos D1 mini. Del lado reverso:

![pcb real 2](Imagenes/pcb_real_22.png)

Se puede ver un slot para vincular con la Edukit y borneras para conectar sensores analógicos.

El esquemático de la placa PCB se adjunta en el Anexo VI.


# Servidor con base de datos y visualización


Luego de elegir el Gateway / server se seleccionó el siguiente software:


- Base de datos
- Visualización
- Plataforma


La base de datos almacena la información proveniente de los sensores y se hace de forma local. La alternativa mas interesante es usar una base de datos de serie de tiempo, muy útil para aplicaciones de IoT.


La visualización debe tomar los datos de la base de datos, interpretarlos, realizar operaciones con ellos de acuerdo al detalle de información necesario y ponerlos a disposición en un grafico que se actualice en tiempo real.


Por ultimo, la plataforma se elige de manera que pueda vincularse con una base de datos y una aplicación que interprete dichos datos para mostrarlos de forma gráfica y en tiempo real. Se pretende que ésta tenga la cualidad de poder programarse para concentrar todas las tareas de vinculación antes descriptas (recepción, inserción en base de datos y visualización) en la misma herramienta y no tener que derivarlas a otras aplicaciones.


A continuación se detalla cada parte con su criterio de selección.


## Base de datos


Como se dijo anteriormente, de los diferentes tipos de base de datos se opta por usar una del tipo de series de tiempos. Una base de datos de series de tiempo (TSDB, por sus siglas en inglés) es una base de datos optimizada para datos con sello de tiempo (*timestamps*) o series temporales. Estos podrían ser métricas de un servidor, monitoreo del rendimiento de una aplicación, datos de red, datos de sensores, eventos, clics, intercambios en un mercado y muchos otros tipos de datos analíticos. 


Una TSDB está optimizada para medir el cambio en el tiempo. Las propiedades que hacen que los datos de series de tiempo sean muy diferentes de otras cargas de trabajo de datos son la administración del ciclo de vida de los datos, el resumen y los análisis de gran alcance de muchos registros. 


Entre las opciones disponibles en la web, se selecciona **InfluxDB**.


### InfluxDB

![Influxdb_logo.svg22222](Imagenes/Influxdb_logo.svg22222.png)

InfluxDB, desarrollada por *influxdata*, es una base de datos basada en series de tiempo (time-series database). Maneja de forma eficiente estas series de datos, con miles de inserciones por segundo, haciendo cálculos, búsquedas, e incorporando información, como medias, máximos, etc, todo ello en tiempo real. Dentro de esta categoría, InfluxDB es una base de datos que supera a los esquemas SQL y NoSQL. Tiene una versión comunitaria de código abierto.


InfluxDB tiene un protocolo de línea para enviar datos de series de tiempo que adopta la siguiente forma: `measurement-name` `tag-set` `field-set` `timestamp`. El `measurement-name` y `tag-set` se mantienen en un índice invertido que hace que las búsquedas de series específicas sean muy rápidas.

Los timestamps en InfluxDB pueden tener una precisión de segundo, milisegundo, microsegundo o nanosegundo. En el disco, los datos se organizan en un formato de estilo encolumnado donde se establecen bloques de tiempo contiguos para la medición, el `tag-set` y el `field`. Por lo tanto, cada campo se organiza secuencialmente en el disco por bloques de tiempo, lo que hace que el cálculo de agregados en un solo campo sea una operación muy rápida. No hay límite en el número de etiquetas y campos que se pueden utilizar.

En el Anexo VII se encuentra el proceso de instalación y configuración de InfluxDB sobre Raspbian.	


#### Criterio de selección


El sitio web DB-Engines, califica las bases de datos según la popularidad del motor de búsqueda, las menciones en las redes sociales, las ofertas de trabajo y el volumen de discusión técnica. Aquí están los resultados actuales:


![Valoración base de datos de series temporales](https://www.influxdata.com/wp-content/uploads/JUN-2019-Matrix2.png)


Se puede leer toda la metodología para elaborar el ranking en https://db-engines.com/en/ranking_definition.


Además, según el mismo sitio, las bases de datos de series temporales son el segmento de más rápido crecimiento de la industria de bases de datos en el último año.


![Crecimiento base de datos en los ultimos años](https://www.influxdata.com/wp-content/uploads/JUN-2019-Categories-1.png)


Es por todo esto, velocidad, eficiencia, popularidad y gran comunidad que se toma a **InfluxDB** como base de datos para almacenar las mediciones extraídas de los sensores en tiempo real.


## Visualización 


Es importante poder interpretar los datos de un proceso de manera rápida y concisa. Es por esto que se necesita de una aplicación que pueda producir graficas acordes a los datos extraídos de los sensores y en tiempo real. La herramienta elegida para dicha tarea es **Grafana**.


## Grafana

![Logo Grafana](http://www.freelogovectors.net/wp-content/uploads/2018/07/grafana-logo.png)

Grafana es una herramienta de código abierto para el análisis y visualización de métricas. Está escrita en Lenguaje Go (creado por Google) y Node.js LTS. Además cuenta con una fuerte Interfaz de Programación de Aplicaciones (API); es una aplicación que ha venido escalando posiciones, con una comunidad entusiasta de más de 600 colaboradores bien integrados. Su código fuente está publicado en GitHub.


A partir de una serie de datos recolectados se puede obtener un panorama gráfico presentado en una forma elegante y amigable. Grafana permite consultar, visualizar, alertar y comprender métricas que pueden ser recolectadas y/o procesadas por aplicaciones de terceros. Puede recopilar de forma nativa datos de *Cloudwatch, Graphite, Elasticsearch, OpenTSDB, Prometheus, Hosted Metrics e InfluxDB*.

En el Anexo VIII se encuentra el proceso de instalación y configuración de Grafana sobre Raspbian.


#### Criterio de selección


Es una plataforma que permite consultar, visualizar, analizar y comprender todo tipo de métricas sin importar donde estén almacenados los datos. Todo esto desde una interfaz Web elegante y sencilla. Está respaldada por grandes organizaciones como Energy Weather, eBay o Stack Overflow, que la utilizan y es de código abierto. Tiene una gran cantidad de entradas y tiene buenos plugins de componentes para visualizar, además de que la edición de los gráficos es muy intuitiva.


Resulta interesante poder exportar las graficas generadas, y administrar los rangos de tiempos a visualizar. Esto Grafana lo hace muy bien, con la posibilidad de que sea en tiempo real. Además se puede configurar y restringir acceso de usuarios en función de permisos, por Organizaciones. 


## Plataforma


Se hace una breve investigación sobre las plataformas en la nube, pero se termina optando por una local, ya que la intención es alojar el servidor en una mini PC Raspberry Pi. Se toma esta decisión en conjunto con la cooperativa ya que para ciertas funcionalidades las plataformas en la nube suelen tener algún costo y hubiera sido bastante desafortunado toparse con esta situación a mitad del proyecto, teniendo que migrar todo a otro servicio. Al mantener el servidor de manera local se puede programar a gusto. De todas formas, a futuro y con un buen análisis no se descarta por parte de la cooperativa hacer uso de una plataforma cloud.


En cuanto a las plataformas locales nos encontramos con Node-RED y Home Assitant.

**Node-RED** es un motor de flujos con enfoque IoT, que permite definir gráficamente flujos de servicios, a través de protocolos estándares como REST, MQTT, Websocket, AMQ, etc,  además de ofrecer integración con API’s de terceros, tales como Twitter, Facebook, Yahoo!, etc. 

![node-red-icon-2](Imagenes/logo_nodered.png)

Se trata de una herramienta visual muy ligera, programada en NodeJS y que puede ejecutarse tanto en dispositivos tan limitados como una Raspberry, como en plataformas complejas como IBM Bluemix, Azure IoT o Sofia2 Platform.

El editor de flujos de Node-RED consiste en una sencilla interfaz en HTML, accesible desde cualquier navegador, en la que arrastrando y conectando nodos entre sí, permite definir un flujo que ofrezca un servicio.

![flujo-node-red](Imagenes/flujo-node-red.png)


Viene con funcionalidades básicas pero es posible adicionarse otras descargando los paquetes necesarios. Entre ellos se encuentran:


- Dashboard: Incluye nodos que permiten realizar web dinámicas.
- Mqtt-Broker: Crea un broker local, necesario para vincular la Edukit con la base de datos. EL broker es llamado MOSCA.
- InfluxDB: Nodos para insertar, realizar consultas y eliminar datos sobre una base de datos InfluxDB.

En el Anexo IX se encuentra el proceso de instalación y configuración de Node-RED sobre Raspbian.


### Criterio de selección


Ambas son atractivas para el fin del proyecto, con una buena documentación y experiencias en la red, pero por cuestiones de tiempo solo se llega a indagar más sobre Node-RED y no tanto sobre Home Assistant, otra plataforma similar a Node-RED. La decisión final es basar todo el proyecto en Node-RED.


# Lenguajes de programación utilizados


Para la configuración de cada herramienta fue necesario utilizar los siguientes lenguajes de programación.


### C++


Es un lenguaje de programación diseñado en 1979 por Bjarne Stroustrup. La intención de su creación fue extender al lenguaje de programación C mecanismos que permiten la manipulación de objetos. Además se añadieron facilidades de programación genérica, que se sumaron a los paradigmas de programación estructurada y programación orientada a objetos. Por esto se suele decir que el C++ es un lenguaje de programación multiparadigma. 


Usado para programar la **Edukit** y el firmware **Tasmota.**


### JavaScript

**JavaScript** (abreviado comúnmente **JS**) es un lenguaje de programación interpretado, dialecto del estándar ECMAScript. Se define como orientado a objetos, basado en prototipos, imperativo, débilmente tipado y dinámico. Se utiliza principalmente en su forma del lado del cliente (*client-side*), implementado como parte de un navegador web permitiendo mejoras en la interfaz de usuario y paginas web dinámicas. 


Usado para programar los nodos y flujos de la plataforma **Node-RED**.


### HTML


HTML es un lenguaje de marcado que se utiliza para el desarrollo de páginas de Internet. Se trata de la siglas que corresponden a HyperText Markup Language, es decir, Lenguaje de Marcas de Hipertexto. Es el elemento de construcción más básico de una página web y se usa para crear y representar visualmente una página web. Determina el contenido de la página web, pero no su funcionalidad (como si lo haría JavaScript, por ejemplo).


Usado para programar los nodos Dashboard para la generación de la web dinámica de la plataforma **Node-RED**.


# Pruebas de integración


Como prueba de integración se montó un escenario de invernadero inteligente. La idea general es tomar datos del ambiente a través de sensores con el módulo IoT y la Edukit; vía WiFi con una placa WeMos D1 mini se envían los datos usando protocolo MQTT a un broker MOSCA instalado en una Raspberry Pi. Los datos son leídos y guardados en una base de datos de serie temporal, InfluxDB. Por último, se busca obtener graficas en tiempo real, con Grafana, de lo que esta sucediendo en nuestro invernadero, presentando la información en una web local con Node-RED. Esto se hizo basado en un proyecto de Andreas Spiess, aficionado al mundo IoT.


La integración y vinculación de herramientas se hace con la plataforma Node-RED, que además de crear el broker MQTT e insertar y consultar la base de datos InfluxDB provee una web dinámica donde se mostraran los gráficas generados con Grafana.

El proceso de instalación, configuración de las herramientas y resultados de pruebas con estas tecnologías se puede encontrar en el Anexo X.

La documentación para realizar la implementación del invernadero inteligente se puede encontrar en el Anexo XI, Además está en un repositorio en Github, liberado a la comunidad.

El invernadero para pruebas se ve montado de esta manera:

![prototipo](Imagenes/prototipo.jpg)


Se pueden observar que el YL-69 y el MQ-9 esta conectados a dos borneras de la placa PCB, por encima de esta se encuentra la WeMos D1 mini.


## Esquema general y funcionamiento del prototipo


![Esquema general prototipo](Imagenes/esquema_general_2.png)


### Edukit y módulo IoT


Dentro del entorno del invernadero se encuentra la planta y la Edukit con su módulo IoT en una PCB. La Edukit concentra los sensores de intensidad lumínica y de temperatura (LDR y LM35 respectivamente). La PCB dispone de un sensor de humedad de suelos, otro de monóxido de carbono (YL-69 y MQ-9 respectivamente) y la placa WeMos D1 mini. Si bien el conexionado de los sensores adicionales y la WeMos D1 mini se hace sobre la PCB, eléctricamente están unidos a la Edukit. Viene a cumplir la función de un shield para Arduino.


La comunicación entre la Edukit y la interfaz inalámbrica es serial por software. El vinculo serial por hardware se reserva para monitorizar el proceso desde el Arduino.


Los datos de los sensores se envían en forma de un string del estilo:


```Bash
edukit,mq9= 0.52,hum= 2.00,ldr=828.00,temp=34.21
```


Este es un mensaje y el mismo se publica a dos broker MQTT. Uno es un broker EMQTT, que lo dispone la cooperativa para sus propósitos. En este caso se destinó un topic para que los miembros puedan hacer un seguimiento de los mensajes que publican los módulos IoT (que tendrán usarán a futuro). El otro broker se llama MOSCA y es creado como un nodo dentro de la plataforma Node-RED. 


Los datos se envían con cierta periodicidad que puede ser cambiada en el código de la Edukit. Al ser strings relativamente largos, y como se hacen dos publicaciones a brokers distintos, estas se deben hacer con intervalos de al menos 3 segundos. Esto sucede porque los mensajes primero viajan por serial a la WeMos, y luego se publican. La comunicación serial es muy lenta, y esos tiempos deben respetarse, de lo contrario los strings llegarían incompletos. Por fortuna, a fines de esta práctica y para uso en un invernadero, las muestras se toman en períodos de como mínimo 30 minutos, por lo que ese lapso entre publicaciones a distintos brokers pasa a ser despreciable. Los datos del entorno varían muy lentamente con el tiempo.


En cuanto al firmware de la WeMos D1 mini, pese a seleccionarse Tasmota, este no cumplía con las necesidades para el proyecto. No interpretaba los mensajes que llegaban por serial. Era una funcionalidad en la que los desarrolladores estaban trabajando pero en la última versión (hasta ese momento) no estaba disponible aún. Por lo tanto se modificó el código fuente (sin alterar el normal funcionamiento) para que fuera posible recibir un string por software serial, interpretarlo como un comando y luego publicarlo a un topic como mensaje MQTT a un broker.


### Gateway / Server


Dentro de la Raspberry Pi se aloja la plataforma Node-RED, la base de datos InfluxDB y la aplicación Grafana. Todas las herramientas se relacionan entre si gracias a Node-RED.


La plataforma tiene 3 funciones bien marcadas:


- Uso del protocolo de mensajería, haciendo las veces de Broker por un lado, y cliente por otro
- Insertar datos y realizar consultas a la base de datos
- Generar una web presentando gráficas en función a los datos de los sensores


El funcionamiento de Node-RED es en base a flujos y nodos. Los nodos se conectan entre sí generando un flujo. 


En cuanto al protocolo de mensajería hay dos nodos importantes, uno que genera el broker MOSCA y otro que se suscribe al topic donde la Edukit publica los datos de los sensores.


![nodos principales](Imagenes/nodos_principales.png)


Los datos que llegan se parsean y se insertan en la base de datos. Existen nodos que provienen del mismo paquete instalado para vincularse con InfluxDB que sirven para otras operaciones, como gestionar consultas y devolver resultados sobre una base.


La herramienta de visualización Grafana realiza consultas a la base de datos, y una vez obtenidos puede operar con ellos, como calcular máximos, mínimos, medias, etc. Se generan 5 graficas, las cuales son una para cada sensor y la última para mostrar la fecha y la hora. Dichas graficas son embebidas en la web que se desarrolla con Node-RED.


Se crea un sitio web usando los nodos *Dashboard* y combinando los antes mencionados, permitiendo tener en un mismo lugar toda la información necesaria del invernadero en tiempo real y con posibilidad de exportar para posteriores análisis. Los archivos exportados se guardan en un directorio de la Raspberry Pi en formato csv.


### Visualización


La aplicación web es la síntesis de todo el trabajo hecho. Primeramente esta Node-RED, que con distintos nodos levantan el servidor localmente y administran los sectores de la web por medio de bloques modificables en muchos aspectos.


La información esta divida en dos pestañas, una con valores instantáneos y la otra con valores históricos. El flujo de Node-RED para generar la pestaña de valores instantáneos es la siguiente:


![nodos vista rapida](Imagenes/nodos_vista_rapida.png)


No hay conexiones entre nodos porque son solo bloques aislados dentro de la pagina principal que presentan la información.


En cuanto al flujo que genera la pagina para los datos históricos es el siguiente:


![datos historicos](Imagenes/nodos_datos_historicos.png)


Y para poder lograr exportar la información de estos valores históricos se necesita otro flujo de Node-RED, y es este:


![exportar datos historicos](Imagenes/nodos_exportar.png)

Para hacer esto es necesario determinar los intervalos de tiempo en los que se quiere extraer información, el formato, el tipo de consulta que se debe hacer a la base de datos e indicar en donde se guardará el archivo. 

Todo el despliegue, configuraciones y proceso de creación de flujos y nodos se encuentra la documentación liberada en Github y también en el Anexo XI.

### Resultado final

Todo se muestra en un sitio web creado por Node-RED. Se puede acceder al mismo de forma local en un explorador cualquiera con la URL http://192.168.20.129/1880/Invernadero. A continuación se muestran partes del sitio web creado y sus menues.


#### Pestaña valores instantáneos - *Invernadero*


![web invernadero datos rapidos](Imagenes/web_invernadero_datos_rapidos.png)


#### Pestaña valores históricos - *Registros*


##### Intensidad de luz


![web invernadero registros con luz](Imagenes/web_invernadero_registros_con_luz.png)


##### Humedad en suelo


![web invernadero registros con humedad](Imagenes/web_invernadero_registros_con_humedad.png)


##### Concentración de monóxido de carbono


![web invernadero registros con mq9](Imagenes/web_invernadero_registros_con_mq9.png)


##### Temperatura ambiente


![web invernadero registros con temp](Imagenes/web_invernadero_registros_con_temp.png)


Se encuentra desplegado el menú para exportar.


# Conclusiones

El principal objetivo de la práctica profesional es insertar al estudiante al ámbito laboral para que pueda desarrollar los conocimientos adquiridos durante el cursado de la carrera. En este aspecto debo decir que al haber tomado contacto con un entorno distinto al académico fue muy productivo ya que me dejó muchas enseñanzas en lo que se refiere a la carrera que hoy estoy culminando. Me tocó experimentar distintos tipos de actividades, algunas mas conocidas por haberlas desarrollado en alguna asignatura del plan de estudios, como programar un Arduino, hasta otras no tan habituales, como tomar decisiones para la selección de un software o hardware. 

Luego de haber investigado las mejores opciones para desarrollar el objetivo de la practica era necesario tener que comunicarlo al equipo de trabajo y ponerlos al tanto de cada decisión. No era algo a lo que estuviera acostumbrado pero todos los aportes que me dieron fueron para mejor, y me respaldaron en cada decisión tomada. Me sentí muy a gusto en cada paso que daba.

El entorno laboral fue de lo mejor, son personas que ya conocía de otros ámbitos, por lo que la relación con ellos es muy buena. Muchos de mis compañeros son programadores, entonces ante cualquier duda podía consultarles ya que están mucho mas involucrados en la temática. Si bien la programación es una herramienta importante de la carrera, los ingenieros en telecomunicaciones no somos programadores.

En contraparte a lo antes dicho, al haber estado en un ámbito de una cooperativa, donde su lógica de organización es distinta a la de una empresa y a su vez los miembros ya me eran conocidos, no sentí haber tenido grandes responsabilidades, más de las propias de la practica, como respetar los tiempos, comunicar avances, etc. No se si me ha preparado para en un futuro trabajar en un empresa en la que la lógica de trabajo y de organización sea mas vertical, que haya que responder por otras personas o rendir cuentas a un superior, entre otras cosas.

En cuanto a lo profesional y técnico, tomé herramientas que en un futuro podría implementar para un emprendimiento propio. La temática de IoT es muy amplia, pero creo haber allanado el camino lo suficiente como para poder seguir aprendiendo por mi cuenta. Mas allá de eso, entiendo que no estoy preparado para lanzarme con un emprendimiento. Me gustaría poder aprender más, ganar experiencia, y luego si hacer algo propio. 

Respecto a las tareas realizadas, quedaron algunas cosas pendientes que fueron surgiendo en el desarrollo de la practica, y otras que se descartaron. Entre las pendientes tenemos:

- Bridge entre brokers MQTT
- Aprovisionamiento - Instalación y variables de entorno
- Web responsive

En el caso del bridge entre brokers surgía como opción para no tener que hacer publicaciones distintas con la misma información. La idea era que un broker reenvíe lo publicado a otro automáticamente cuando llega un mensaje a determinado topic. Esto no fue posible ya que no hubo muchas opciones de modificación al broker propio de Node-RED. Una solución seria configurar otro broker por fuera de la plataforma en la que se pueda programar para desempeñar esta tarea.

Una cosa que parecía interesante era la opción de poder configurar el modulo y la Edukit a través de la conexión inalámbrica, lo que se conoce como configuración OTA (Over-The-Air). Por falta de tiempo no se pudo indagar mucho en el aprovisionamiento y la modificación de las variables de entorno.

Por ultimo, no fue posible hacer un sitio web responsive, la programación en Node-RED solo permite hacer webs que puedan visualizarse en el explorador de una PC, por cuestiones de resolución.

# Referencias y bibliografía

Obradovic L. (Enero 2018). Tasmota vs ESPurna vs ESPEasy - overview. Recuperador el 2 de enero de 2019. [https://lobradov.github.io/FOSS-Firmware-comparison-overview/](https://lobradov.github.io/FOSS-Firmware-comparison-overview/)	

Sonoff/Tasmota - [https://github.com/arendst/Sonoff-Tasmota](https://github.com/arendst/Sonoff-Tasmota)

Espurna - [https://github.com/xoseperez/espurna](https://github.com/xoseperez/espurna)

ESPEasy - [https://www.letscontrolit.com/wiki/index.php/ESPEasy]( https://www.letscontrolit.com/wiki/index.php/ESPEasy)

MQTT - [https://mqtt.org/documentation](https://mqtt.org/documentation)

Raspberry Pi - [https://www.raspberrypi.org/documentation/](https://www.raspberrypi.org/documentation/)

EasyEDA [- https://docs.easyeda.com/en/FAQ/Editor/index.html](https://docs.easyeda.com/en/FAQ/Editor/index.html)

InfluxDB - [https://github.com/influxdata/influxdb](https://github.com/influxdata/influxdb)

Influxdata - Time series database (TSDB) explained. [https://www.influxdata.com/time-series-database/](https://www.influxdata.com/time-series-database/)

Grafana - [https://grafana.com/docs/](https://grafana.com/docs/)

MQTT Mosca broker - [https://github.com/mcollina/mosca/wiki](https://github.com/mcollina/mosca/wiki)

Mueller J. (Dec 2017). Understanding the Less Popular Push/Streaming Protocols (XMPP, CoAP, MQTT, etc.). Recuperado 8 de enero de 2019. [https://www.programmableweb.com/news/understanding-less-popular-pushstreaming-protocols-xmpp-coap-mqtt-etc/analysis/2017/12/11](https://www.programmableweb.com/news/understanding-less-popular-pushstreaming-protocols-xmpp-coap-mqtt-etc/analysis/2017/12/11)

14core - The IOT Protocols The Base of Internet of Things Ecosystem - [https://www.14core.com/the-iot-protocols-the-base-of-internet-of-things-ecosystem/](https://www.14core.com/the-iot-protocols-the-base-of-internet-of-things-ecosystem/)

DB-Engines - [https://db-engines.com/en/](https://db-engines.com/en/)

DB-Engines - DB-Engines Ranking of Time Series DBMS - [https://db-engines.com/en/ranking/time+series+dbms](https://db-engines.com/en/ranking/time+series+dbms)

Spiess A. - #255 Node-Red, InfluxDB, and Grafana Tutorial on a Raspberry Pi - [https://www.youtube.com/watch?v=JdV4x925au0](https://www.youtube.com/watch?v=JdV4x925au0)

# Anexos

[ANEXO I](https://github.com/danunziata/pps-joaquin-manchado/blob/master/Anexos/ANEXO%20I%20-%20Pinout%20y%20Esquem%C3%A1tico%20Edukit/ANEXO%20I%20-%20Pinout%20y%20Esquem%C3%A1tico%20Edukit.md):  Pinout y Esquemático Edukit10

[ANEXO II](https://github.com/danunziata/pps-joaquin-manchado/blob/master/Anexos/ANEXO%20II%20-%20Pinout%2C%20especificaciones%20y%20Esquem%C3%A1tico%20WeMos%20D1%20Mini/ANEXO%20II%20-%20Pinout%2C%20especificaciones%20y%20Esquem%C3%A1tico%20WeMos%20D1%20Mini.md): Pinout, especificaciones y Esquemático WeMos D1 Mini

[ANEXO III](https://github.com/danunziata/pps-joaquin-manchado/blob/master/Anexos/ANEXO%20III%20-%20Instalaci%C3%B3n%2C%20configuraci%C3%B3n%20y%20pruebas%20realizadas%20con%20Tasmota.md):  Instalación, configuración y pruebas realizadas con Tasmota

[ANEXO IV](https://github.com/danunziata/pps-joaquin-manchado/blob/master/Anexos/ANEXO%20IV%20-%20Instalaci%C3%B3n%2C%20configuraci%C3%B3n%20y%20pruebas%20realizadas%20con%20ESPurna.md): Instalación, configuración y pruebas realizadas con ESPurna

[ANEXO V](https://github.com/danunziata/pps-joaquin-manchado/blob/master/Anexos/ANEXO%20V%20-%20Instalaci%C3%B3n%20y%20configuraci%C3%B3n%20de%20Raspbian%20sobre%20Raspberry%20Pi%203%20B%2B.md): Instalación y configuración de Raspbian sobre Raspberry Pi 3 B+

[ANEXO VI](https://github.com/danunziata/pps-joaquin-manchado/blob/master/Anexos/Anexo%20VI%20-%20Esquematico%20PCB.pdf): Esquemático PCB

[ANEXO VII](https://github.com/danunziata/pps-joaquin-manchado/blob/master/Anexos/ANEXO%20VII%20-%20Instalaci%C3%B3n%20y%20configuraci%C3%B3n%20de%20InfluxDB%20sobre%20Raspbian.md): Instalación y configuración de InfluxDB sobre Raspbian

[ANEXO VIII](https://github.com/danunziata/pps-joaquin-manchado/blob/master/Anexos/ANEXO%20VIII%20-%20Instalaci%C3%B3n%20y%20configuraci%C3%B3n%20de%20Grafana%20sobre%20Raspbian.md): Instalación y configuración de Grafana sobre Raspbian

[ANEXO IX](https://github.com/danunziata/pps-joaquin-manchado/blob/master/Anexos/ANEXO%20IX%20-%20Instalaci%C3%B3n%20y%20configuraci%C3%B3n%20de%20Node-RED%20sobre%20Raspbian.md): Instalación y configuración de Node-RED sobre Raspbian

[ANEXO X](https://github.com/danunziata/pps-joaquin-manchado/blob/master/Anexos/ANEXO%20X%20-%20Integraci%C3%B3n%20de%20tecnolog%C3%ADas%20Node-Red%2C%20InfluxDB%20y%20Grafana.md): Integración de tecnologías Node-Red, InfluxDB y Grafana

[ANEXO XI](https://github.com/danunziata/pps-joaquin-manchado/blob/master/Anexos/ANEXO%20XI%20-%20Documentaci%C3%B3n%20Proyecto%20prototipo%20IoT%20Invernadero%20Inteligente.md): Documentación Proyecto prototipo IoT Invernadero Inteligente

[ANEXO XII](https://github.com/danunziata/pps-joaquin-manchado/tree/master/Anexos/ANEXO%20XII%20-%20Hojas%20de%20datos): Hojas de datos de todos los componentes de Hardware

- ESP8266
- Sensores:
  - LM35
  - LDR
  - MQ-9
  - YL-69
- Raspberry Pi 3 B+
